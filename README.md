# Description
This project is part of [my PhD thesis](http://etheses.whiterose.ac.uk/28366/) entitled: *"The role of emotions in autonomous social agents"*. The main hypothesis, this thesis explores, is that *"emotions inform the brain as to the nature of a given situation, and guide the decision-making process to increase the chances of survival for a virtual agent"*. However, before tackling the role emotions play within the decision-making process, it is important to find which brain areas (or systems) are involved in emotions and what their respective roles are. Using the [**ProtoEmo** architecture](http://aisb2017.cs.bath.ac.uk/conference-edition-proceedings.pdf) on a [**resource foraging** task](https://gitlab.com/davinellulinvega/epuckvsscheutz) it was shown that the survival circuit (see [LeDoux's paper](http://www.cell.com/article/S0896627312001298/fulltext) for more details on this theory) made of the amygdala, the hypothalamus and the thalamus, is indeed able to influence the actions of virtual agents to increase their survival potential. Additionally, the amygdala was also identified as the system responsible for detecting stimuli relevant to an agent's survival. Having explored the mechanism responsible for triggering emotional episodes, we can finally investigate the role of emotions in the decision-making process.

It should be noted that for archival purposes, this project is also available on [the University of Sheffield's Open Research Data Archive](https://figshare.shef.ac.uk/articles/software/The_Role_of_Emotions_in_Autonomous_Social_Agents_Python_Code_experiment_2_/14518086). Since this repository might evolve in the future, for reproducing the results presented in my thesis it is advised to use the source code hosted by the University of Sheffield.

## PrimEmo
**PrimEmo** is the name given to the architecture resulting from the aggregation of [**ProtoEmo**](http://aisb2017.cs.bath.ac.uk/conference-edition-proceedings.pdf), the [Primary Value Learned Value](http://doi.apa.org/getdoi.cfm?doi=10.1037/0735-7044.121.1.31)(PVLV) and the [Pre-frontal cortex Basal ganglia Working Memory](http://psych.colorado.edu/~oreilly/papers/HazyFrankOReilly06.pdf)(PBWM). Briefly, the PVLV model describes the brain mechanisms by which an animal is able to associate a previously neutral stimulus (also called a conditioned stimulus, CS) with an innately rewarding or punishing stimulus (usually referred to as unconditioned stimulus, US). It should be noted that the amygdala is included in this model, and is attributed the role of triggering a burst of dopamine activity at the onset of a CS. The PBWM architecture explores the gating mechanism implemented by the basal ganglia. This mechanism is used in the animal's brain to decide on the next action to perform, as well as when to update the content of working memory. Consequently, at an abstract level the **PrimEmo** architecture can be construed as implementing a complete Actor-Critic to borrow concepts from the reinforcement learning paradigm.

Although the [previous project](https://gitlab.com/davinellulinvega/epuckvsscheutz) set out to prove that **ProtoEmo** is quite a capable architecture in itself, it still lacks the ability to take any definitive decision. Indeed, both the amygdala and hypothalamus are limited to only influencing other parts of the brain. **PrimEmo**, on the other hand, takes full advantage of the basal ganglia's gating mechanism to control the *'species-specific'* reaction to an innately triggering stimulus. Furthermore, through the activity of the dopamine and serotonin systems, **PrimEmo** should be able to build on **ProtoEmo**'s salience output to support primitive emotions. Consequently, the two hypotheses that **PrimEmo** and this experiment seek to verify are:
* **H1**: The use of a system capable of computing the salience of a given situation will enhance the survival capabilities of virtual agents in complex and dynamic environments.
* **H2**: Furthermore, the mechanisms responsible for computing the salience of a situation will trigger an emotional episode, where emotions are characterized by a level of *'arousal'* and *'valence'*.

## Viking Doom
Given that, compared to **ProtoEmo**, **PrimEmo** has access to a reward and punishment system, as well as a complete action selection mechanism the resource foraging task on which **ProtoEmo** was tested did not provide enough of a challenge for **PrimEmo** to be able to make full use of its emotional capabilities. Consequently, it was decided that a more complex and dynamic environment was required to demonstrate **PrimEmo**'s full potential. Enters *'Viking Doom'*.

Viking Doom is a viking themed roguelike game to be played by virtual agents only. Roguelike are usually characterized by a party of mortal heroes having to explore and fight their way out of a procedurally generated dungeon. On the contrary to most other game genre, in a roguelike, when the main character dies it is not brought back to life. Instead, the player has to start again from the beginning, losing all of his progress. This is a concept usually referred to as *'permadeath'*. In Viking Doom, each hero is controlled by either a deep Q-learning network (DQN), an [Advantage Actor-Critic](http://arxiv.org/abs/1602.01783) (A2C) or the **PrimEmo** architecture. The goal is to gather as much gold item as possible while surviving. The environment is a tiled-based map populated heroes, monsters and items. The actions of both heroes and monsters are considered sequentially, with the heroes having precedence over the monsters. Each hero is initialized with 100 units of health, one level of strength and no gold. To build a strategy and collect as much gold as possible, heroes have at their disposal five actions: move up, move down, move left, move right or stay in place. Heroes can collect gold by stepping on the same tile as the item. Similarly, if two heroes or a hero and a monster are on the same tile they will automatically fight, dealing damage equal to their respective level of strength. To replenish its health a hero has to collect health potions.

# Installation
The Viking Doom project has been implemented and tested to run with [Python 3.7.6](https://www.python.org/downloads/release/python-376/).
## Requirements
Running the game engine and plotting the results from the testing phase require the following packages:
```
boto3 (1.11.1)
deap (1.3.0)
h5py (2.10.0)
joblib (0.14.1)
Keras (2.3.1)
lz4 (3.0.2)
matplotlib (3.1.2)
numpy (1.18.1)
pandas (0.25.3)
python-engineio (3.11.2)
python-socketio (4.4.0)
requests (2.22.0)
scipy (1.4.1)
seaborn (0.10.0)
tables (3.6.1)
tensorflow or tensorflow-gpu (1.15.0), depending on available hardware
urllib3 (1.25.7)
```
A more detailed list of all the required packages and their dependencies can be found in the [requirements.txt](requirements.txt) file.

## Installation with PIP
All the packages necessary to run Viking Doom, as well as extract and plot the results can easily be installed using the [Python Package Installer](https://pip.pypa.io/en/stable/). After cloning the repository execute the following command:
```bash
> pip install -U -r requirements.txt
```
or
```bash
> pip install --user -U -r requirements.txt
```
Depending on whether you want to install the packages system-wide or only for the current user, respectively.

# Running the experiment
To run the full experiment, two files are of interest: [Simulation.py](Simulation.py) and [Genetic.py](Genetic.py). While the `Simulation.py` script is in charge of executing the training and testing phase for the Q-learning and A2C controller, as well as the testing phase for the **PrimEmo** based controller, learning for the **PrimEmo** architecture uses a simple genetic algorithm.
## Training
To train the **PrimEmo** controller, prior to the testing phase, use the following command:
```bash
> python Genetic.py --nb_gens number_of_generations
```
Where `--nb_gens` specifies for how many generations to run the genetic algorithm. For more details on the available parameters for the `Genetic.py` script, see: `python Genetic.py --help`.

For any of the other controllers (DQN and A2C), use the `Simulation.py` script as follow:
```bash
> python Simulation.py --ctrl {DEEP_Q|ACTOR_CRITIC} --nb_games number_of_games
```
Where `--nb_games` should be provided the number of games to train the controllers on. Additional parameters are available for monitoring the controller's performances or displaying the game's state. Please have a look at `python Simulation.py --help` for more details on these.

## Testing
Regardless of the type of controller, the testing phase is handled by the `Simulation.py` script. Similar to the training phase, the testing phase for a given controller is executed using the following command line:
```bash
> python Simulation.py --test --ctrl {DEEP_Q|ACTOR_CRITIC|PRIM_EMO|RANDOM}
```
Where the `--test` flag indicate that this is the testing phase.

## Monitoring
There are two ways to monitor the progress for both the game and the different heroes in it. Depending on the amount of details required, you can either use the [VikingDoomDisplay project](https://gitlab.com/davinellulinvega/vikingdoomdisplay) to render the game's state in a window. Alternatively, providing the `--monitor` flag to the `Simulation.py` script will create a [HDF5](https://hdfgroup.org/) formatted data file corresponding to each controller type. After each simulation cycle both the game's and the hero's states are written to this file, to later be processed for extracting the relevant results. The monitored variables can be changed by altering the implementation of the `_monitor()` method, either in the [Controller.py](Controller.py) file or for each controller type (i.e.: [DQN](DeepQCtrl.py), [A2C](ActorCriticCtrl.py), [**PrimEmo**](PrimEmoCtrl.py) and [random](RandomCtrl.py)).

## Plotting
The [extract_results.py](extract_results.py) file serves two purposes:
* It first looks for a global data file to load. If one is present it simply loads the observations in memory. Otherwise, the script will load the individual files associated with each controller type and consolidate the observed states into a single *tidy* formatted array.
* The second part is only interested with processing the data to extract results relevant to the hypotheses mentioned in the first section. Rather than displaying the results in an interactive graph, the script uses the [seaborn](https://seaborn.pydata.org/) library to generate PDF and EPS formatted images usable in publications.
If you modify the type and number of observed variable in the previous step, you will have to make the corresponding changes in the `extract_results.py` script. Additionally, you can also customize what results are computed and displayed in the different images generated.

# Bugs and feedback
For any bug or feedback concerning the source code, please open a new issue. Contributions and push requests to improve the game engine and the overall robustness of the code are welcome.
