#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'davinellulinvega'
from collections import defaultdict
from Vec2D import Vec2D
from settings import HeroCfg


class Hero(object):
    """
    A simple container holding the characteristics describing a given hero.
    It is capable of providing a description of itself in dict form.
    """

    def __init__(self, idx, position, health=HeroCfg.MAX_HEALTH, strength=1,
                 gold=0):
        """
        Initialize the different attributes describing a specific hero,
        and those that are simply required by the game and its environment.
        :param idx: int. An unique integer representing the hero's identifier.
        :param position: Vec2D. A vector representing the hero's position
        along the X and Y axis.
        :param health: int. An integer representing the hero's level of health.
        :param strength: int. An integer representing the hero's level of
        strength.
        :param gold: int. An integer representing the hero's level of
        gathered gold.
        """

        # Initialize the parent class
        super(Hero, self).__init__()

        # Declare the basic features describing a hero
        self._id = idx
        self._position = position
        self._health = health
        self._strength = strength
        self._gold = gold

        # Declare some more attributes required by the Game and its environment
        self.action_is_valid = True
        self.nb_items_gathered = defaultdict(int)
        self.nb_fights = defaultdict(int)

    def to_dict(self):
        """

        :return: Dict. A dictionary regrouping the Hero's main characteristics.
        """
        return dict(position=self._position.tolist(), health=self._health,
                    strength=self._strength, gold=self._gold, id=self._id,
                    is_dead=self.is_dead)

    @property
    def id(self):
        return self._id

    @property
    def position(self):
        return self._position

    @position.setter
    def position(self, new_pos):
        assert isinstance(new_pos, tuple) or isinstance(new_pos, Vec2D), \
            "Only tuple and Vec2D objects can be used as position for the hero."
        assert len(new_pos) == 2, "A position is defined by a location along " \
                                  "the X and Y axis."
        if isinstance(new_pos, tuple):
            # Unpack the tuple and transform it into a 2D Vector object
            self._position = Vec2D(*new_pos)
        else:
            self._position = new_pos

    @property
    def is_dead(self):
        return self._health <= 0

    @property
    def gold(self):
        return self._gold

    @gold.setter
    def gold(self, new_gold):
        assert self._gold <= new_gold, "A Hero cannot lose gold."
        self._gold = new_gold

    @property
    def health(self):
        return self._health

    @health.setter
    def health(self, new_hp):
        self._health = max(0, min(new_hp, HeroCfg.MAX_HEALTH))

    @property
    def strength(self):
        return self._strength

    @strength.setter
    def strength(self, new_strg):
        assert self._strength <= new_strg, "A Hero cannot lose strength."
        self._strength = min(new_strg, HeroCfg.MAX_STRENGTH)
