#!/usr/bin/env python3
from os.path import isfile
from argparse import ArgumentParser
import numpy as np
from h5py import File
from matplotlib import pyplot as plt

if __name__ == "__main__":

    # Define an argument parser and some arguments to parse
    parser = ArgumentParser()
    parser.add_argument('-f', '--file', dest='files', action='append',
                        help='The path to the file in which the monitoring is stored.')

    # Parse the arguments
    args = parser.parse_args()

    # Initialize the plot of the results
    fig, ax = plt.subplots()

    # For each of the file provided on the command line
    for file in args.files:
        try:
            # The file is opened in read mode to avoid any accidental overwrite
            store = File(file, 'r', track_order=True)
        except:
            print('An error occurred, while trying to open the storage file: {}'.format(file))
            continue

        try:
            # For each game in the monitoring file, aggregate the values along the 0th axis
            tot_rewards = []
            for g_id, ds in store.items():
                agg = np.sum(ds[:,2])
                tot_rewards.append(agg)

            # Display the average
            print("For file: {}, average reward was: {}".format(file, sum(tot_rewards) / len(tot_rewards)))

            # Plot the evolution of the total reward across the games
            ax.plot(tot_rewards)
        finally:
            # Close the storage file to avoid data corruption
            store.close()

    # Some minor customization of the plot
    ax.set(xlabel='Game', ylabel='Total Reward')
    ax.grid()

    # Show the plot
    plt.show()
