#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'davinellulinvega'
from os.path import isfile
import numpy as np
from h5py import File
from Controller import Controller
from utils import CtrlType
from settings import CtrlCfg, ACCtrlCfg


class ActorCriticCtrl(Controller):
    """
    Define a controller based on the Actor-Critic model. This specific model
    is implemented in two deep neural networks, each corresponding to one of
    the role (actor and critic). Similar to the Deep Q-learning controller,
    this one also makes use of a memory queue
    """

    def __init__(self, action_space, game_id, hero_id,
                 training=True, monitor=False, misc_config=None):
        """
        Initialize the parent class and declare additional attributes
        required for the selection and training process, specific to the
        actor-critic model.
        :param action_space: List. A list of tuples, representing the
        different actions available to the hero (and therefore the controller).
        :param game_id: An integer representing the ID of the current game.
        :param hero_id: An integer representing the ID of the hero associated
        with the current controller.
        :param training: Bool. A boolean value indicating whether the
        specific controller should be in training or testing mode.
        :param monitor: Bool. A boolean value indicating whether the agent's
        behavior should be monitored or not.
        :param misc_config: Dict. A dictionary containing some more parameters
        specific to each controller.
        """

        # Initialize the parent class
        super(ActorCriticCtrl, self).__init__(action_space,
                                              game_id, hero_id,
                                              training=training,
                                              monitor=monitor,
                                              misc_config=misc_config)

        # Declare two networks, an actor and a critic
        # They are not initialized here, since Keras do not play well with
        # multi-processing. Therefore, they are initialized to None, so that
        # if anything goes wrong later on an exception will be raised early on
        self._actor = None
        self._actor_opt = None
        self._critic = None

        # Declare some placeholders for computing the Actor's loss
        self._action_ph = None
        self._advantages_ph = None

        # Declare some lists to store the actor's experience during the n-steps
        self._states = []
        self._actions = []
        self._rewards = []
        self._dones = []

        # Overwrite the logging facility
        self._log = ACCtrlCfg.LOG

        # If none of the important files exist
        required_files = [ACCtrlCfg.STORE_FILE_PATH,
                          ACCtrlCfg.ACTOR_MODEL_FILE_PATH,
                          ACCtrlCfg.CRITIC_MODEL_FILE_PATH]
        # Try to download the files
        self._download([file for file in required_files if not isfile(file)])

    def _set_session(self):
        """
        Configure Keras' session so that it does not use all of the GPU
        memory for a single controller.
        :return: Nothing.
        """

        # Import the required tensorflow/keras modules
        from tensorflow import ConfigProto, Session
        try:
            from tensorflow.keras import backend as K
        except (ModuleNotFoundError, ImportError):
            from keras import backend as K

        # Configure keras' default session
        config = ConfigProto()
        config.gpu_options.allow_growth = CtrlCfg.ALLOW_GROWTH
        config.gpu_options.per_process_gpu_memory_fraction = CtrlCfg.GPU_MEMORY_FRACTION

        # Set Keras default session
        K.set_session(Session(config=config))

    def init(self, env):
        """
        For any sub-class, this method should contain the logic related to
        the initialization of the different components. This method is only
        present because Keras, as it stands, does not play nice with
        multi-processing and requires its library to be loaded in particular
        ways. Therefore, this method should be called after start() has been
        triggered and the controller is running in its own process.
        :param env: Environment. An instance of the Environment class,
        containing the initial game's state.
        :return: Nothing
        """

        # Import all required tensorflow modules

        # Set the session and import all tensorflow related modules
        self._set_session()

        # Let the parent class do its initialization phase
        super(ActorCriticCtrl, self).init(env)

    def to_dict(self):
        """
        Build a representation of the current controller in the form of a dict.
        :return: Dict. A dictionary containing the main characteristics of the
        current controller.
        """

        # Have the parent class provide the initialized dictionary
        desc = super(ActorCriticCtrl, self).to_dict()

        # Return the description augmented with the type of controller
        desc['type'] = CtrlType.ACTOR_CRITIC.value
        return desc

    @classmethod
    def upload(cls, files=None):
        """
        Upload the different monitoring data and saved model to the cloud,
        for safety and backup purposes.
        :param files: List. A list of paths to the files to upload to the
        VikingDoom bucket. The paths should be specified as string.
        :return: Nothing.
        """

        # Make sure the files provided as argument are compatible
        if files is None:
            files = [ACCtrlCfg.CRITIC_MODEL_FILE_PATH,
                     ACCtrlCfg.ACTOR_MODEL_FILE_PATH,
                     ACCtrlCfg.STORE_FILE_PATH]

        # Provide the correct list of files to the parent's function
        super(ActorCriticCtrl, cls).upload(files)

    def save(self):
        """
        Save the prediction network and the monitoring data using the default
        paths specified within the settings module.
        :return: Nothing.
        """

        # Only save the monitoring data when required
        if self._monitoring:
            # Save the monitoring data in a new data set, whose key is the game ID
            storage_file = File(ACCtrlCfg.STORE_FILE_PATH, mode='a',
                                track_order=True)
            storage_file.create_dataset(str(self._game_id), data=self._store,
                                        chunks=True, track_order=True)

            # Close the storage file
            storage_file.close()

        # Save both the actor and critic networks, each in its dedicated file
        if self._training:
            self._actor.save(ACCtrlCfg.ACTOR_MODEL_FILE_PATH)
            self._critic.save(ACCtrlCfg.CRITIC_MODEL_FILE_PATH)

    def _build(self):
        """
        Build both the actor and critic networks. Densely connected layers
        are used throughout, with relu activation for the hidden layers, and
        linear and softmax, for the critic and actor, respectively.
        :return: Nothing.
        """

        # Import the rest of keras' required modules and classes
        # This is done here, because keras does not play well with
        # multi-processing
        try:
            from tensorflow.keras.optimizers import RMSprop
        except (ModuleNotFoundError, ImportError):
            from keras.optimizers import RMSprop

        # Build the critic
        self._build_critic()

        # Compile the critic
        self._critic.compile(RMSprop(lr=ACCtrlCfg.CRITIC_LEARNING_RATE,
                                     decay=ACCtrlCfg.LR_DECAY_RATE,
                                     epsilon=0.1, rho=0.99),
                             loss='mse')
        # Build the actor
        self._build_actor()

        # Build the optimizer for the Actor's weights
        self._build_actor_optimizer()

    def _build_critic(self):
        """
        Build the network corresponding to the Critic, as a sequence of
        hidden layers, sandwiched between the input and output layers.
        :return: Nothing.
        """

        # Import the rest of keras' required modules and classes
        # This is done here, because keras does not play well with
        # multi-processing
        try:
            from tensorflow.keras import Sequential
            from tensorflow.keras.layers import Dense
            from tensorflow.keras.initializers import Constant
        except (ModuleNotFoundError, ImportError):
            from keras import Sequential
            from keras.layers import Dense
            from keras.initializers import Constant

        # Declare the sequential model
        self._critic = Sequential()

        # Add all the hidden layers
        self._critic.add(Dense(ACCtrlCfg.CRITIC_LAYER_SIZES[0], use_bias=True,
                               input_shape=(CtrlCfg.INPUT_SIZE, ),
                               activation='relu',
                               kernel_initializer='he_normal',
                               bias_initializer=Constant(value=0.1)))

        for layer_size in ACCtrlCfg.CRITIC_LAYER_SIZES[1:]:
            self._critic.add(Dense(layer_size, activation='relu', use_bias=True,
                                   kernel_initializer='he_normal',
                                   bias_initializer=Constant(value=0.1)))

        # Declare the output layer
        self._critic.add(Dense(1, use_bias=True, kernel_initializer='he_normal',
                               bias_initializer=Constant(value=0.1)))

    def _build_actor(self):
        """
        Build the network corresponding to the Actor, as a sequence of hidden
        layers, sandwiched between the input and output layers.
        :return: Nothing.
        """

        # Import the rest of keras' required modules and classes
        # This is done here, because keras does not play well with
        # multi-processing
        try:
            from tensorflow.keras import Sequential
            from tensorflow.keras.layers import Dense
            from tensorflow.keras.initializers import Constant
        except (ModuleNotFoundError, ImportError):
            from keras import Sequential
            from keras.layers import Dense
            from keras.initializers import Constant

        # Declare a sequential model
        self._actor = Sequential()

        # Add all the hidden layers
        self._actor.add(Dense(ACCtrlCfg.ACTOR_LAYER_SIZES[0],
                              input_shape=(CtrlCfg.INPUT_SIZE, ),
                              activation='relu', use_bias=True,
                              kernel_initializer='he_normal',
                              bias_initializer=Constant(value=0.1)))

        for layer_size in ACCtrlCfg.ACTOR_LAYER_SIZES[1:]:
            self._actor.add(Dense(layer_size, activation='relu',
                                  use_bias=True,
                                  kernel_initializer='he_normal',
                                  bias_initializer=Constant(value=0.1)))

        # Add the output layer
        self._actor.add(Dense(CtrlCfg.OUTPUT_SIZE,
                              activation='softmax',
                              kernel_initializer='he_normal'))

    def _build_actor_optimizer(self):
        """
        Build the optimizer for the Actor's network, based on the standard
        RMSprop optimizer.
        :return: Nothing.
        """

        # Import the rest of keras' required modules and classes
        # This is done here, because keras does not play well with
        # multi-processing
        try:
            from tensorflow.keras import backend as K
            from tensorflow.keras.optimizers import RMSprop
        except (ModuleNotFoundError, ImportError):
            from keras import backend as K
            from keras.optimizers import RMSprop

        # Actually set the different placeholders
        self._action_ph = K.placeholder(shape=(None, CtrlCfg.OUTPUT_SIZE),
                                        name='categorical_action')
        self._advantages_ph = K.placeholder(shape=(None,),
                                            name='advantages')

        # Compute the vector of weighted actions, using the memory of
        # categorical actions
        weighted_actions = K.sum(self._action_ph * self._actor.output, axis=1)
        # Since we are implementing a N-steps algorithm, computing the
        # advantage, using the N-steps rewards, is like building an
        # eligibility trace.
        # 1e-10 is used to avoid the log() going to -inf if some of the
        # weighed_actions are equal to zero
        eligibility = (K.log(weighted_actions + 1e-10) *
                       K.stop_gradient(self._advantages_ph))

        # The entropy is used to reduce variability. Furthermore, it helps
        # with tasks requiring hierarchical behaviors
        entropy = K.sum(self._actor.output * K.log(self._actor.output + 1e-10), axis=1)

        # Finally define the actor's loss
        loss = ACCtrlCfg.ENTROPY_RATIO * entropy - K.sum(eligibility)

        # Declare the actor's optimizer
        rms = RMSprop(lr=ACCtrlCfg.ACTOR_LEARNING_RATE, rho=0.99,
                      epsilon=0.1, decay=ACCtrlCfg.LR_DECAY_RATE)

        updates = rms.get_updates(loss, self._actor.trainable_weights)
        self._actor_opt = K.function([self._actor.input, self._action_ph,
                                      self._advantages_ph],
                                     [loss],
                                     updates=updates)

    def _load(self):
        """
        Load both network from their respective location.
        :return: Bool. True if loading was successful, False otherwise.
        """

        # Import the rest of keras' required modules and classes
        # This is done here, because keras does not play well with
        # multi-processing
        try:
            from tensorflow.keras.models import load_model
        except (ModuleNotFoundError, ImportError):
            from keras.models import load_model

        try:
            # Try to load the models from file
            self._actor = load_model(ACCtrlCfg.ACTOR_MODEL_FILE_PATH)
            self._critic = load_model(ACCtrlCfg.CRITIC_MODEL_FILE_PATH)

            # Build the Actor's optimizer
            self._build_actor_optimizer()
        except OSError as e:
            # Log the error for ease of debug
            self._log.exception('An error occurred while trying to load '
                                'the models: {}'.format(e.strerror))
            # Something went wrong, so we return False
            return False
        except BaseException as e:
            # Log the error
            self._log.exception('An error occurred while trying to load '
                                'the models: {}'.format(e))
            # Something went wrong, so we return False
            return False
        else:
            return True

    def _learn(self, old_state, action, curr_state, reward, done):
        """
        Following the Actor-Critic algorithm, update the value of the previous
        state, based on the value of the current one, the reward and the
        action performed to transition in-between. Based on the TD-error
        computed by the critic, update the policy as well.
        :param old_state: List. A list of extracted features describing the
        previous state of the environment.
        :param action: Tuple. A tuple representing the action performed to go
        from the old to the current state.
        :param curr_state: List. A list of extracted features describing the
        current state of the environment.
        :param reward: Float. A float indicating the how good/bad the action
        undertaken was.
        :param done: Bool. Signal the end of an episode/task.
        :return: Nothing.
        """

        # Import the required tensorflow modules
        try:
            from tensorflow.keras.utils import to_categorical
        except (ModuleNotFoundError, ImportError):
            from keras.utils import to_categorical

        # After n-steps, learn from the gathered experience
        if (self._cycle % ACCtrlCfg.N_STEPS == 0 or done) and self._cycle != 0:
            # Reshape the stored states and actions
            self._states = np.reshape(self._states, (-1, CtrlCfg.INPUT_SIZE))
            self._actions = np.reshape(self._actions, (-1, CtrlCfg.OUTPUT_SIZE))

            # Compute the current estimated state values
            state_values = self._critic.predict(self._states).flatten()

            # Discount the rewards
            if self._dones[-1]:
                # If the episode is not done yet, the reward corresponding to
                # the last state hasn't been gathered yet. Instead,
                # we substitute the estimated value provided by the critic
                self._dones += [0]
                self._rewards += [state_values[-1]]
                discounted_rewards = self._discount_with_dones()[:-1]
            else:
                discounted_rewards = self._discount_with_dones()

            # Compute the advantage
            advantages = discounted_rewards - np.reshape(state_values,
                                                         len(state_values))

            # Have the Actor and Critic learn
            hist_c = self._critic.fit(self._states, discounted_rewards,
                                      verbose=0)
            self._log.debug("Critic loss: {}".format(hist_c.history['loss']))

            a_loss = self._actor_opt([self._states, self._actions, advantages])
            self._log.debug("Actor loss: {}".format(a_loss))

            # Reset the content of the different experiences
            self._states = []
            self._actions = []
            self._rewards = []
            self._dones = []

        # Store the old state, the action, the reward and whether the episode
        # is done or not
        self._states.append(old_state)
        self._actions.append(to_categorical(action, CtrlCfg.OUTPUT_SIZE))
        self._rewards.append(reward)
        self._dones.append(done)

    def _discount_with_dones(self):
        """
        Discount the rewards gathered during the n-steps experience. This
        implement a sort of eligibility trace.
        :return: list. A list of the discounted rewards.
        """

        # Declare the list of discounted cumulative reward as well as the
        # variable that will hold the cumulative rewards
        discounted = []
        cum_r = 0

        # Compute the discounted rewards
        # Note that since we are going back in time, this is done in reverse
        # order
        for reward, done in zip(self._rewards[::-1], self._dones[::-1]):
            cum_r = reward + ACCtrlCfg.DISCOUNT_FACTOR * cum_r * (1. - done)
            discounted.append(cum_r)
        return np.asarray(discounted[::-1])

    def action(self, reward=0):
        """
        Based on the given environment's current state, have the actor
        compute the probabilities associated with each action. Use those
        probabilities to then choose the action to perform next.
        :param reward: Float. A float value representing the reward
        corresponding to the previous action. This is only required for the
        PrimEmoCtrl controller. Hence, it is initialized to 0, by default.
        :return: Int. An integer representing the action to perform next,
        given the environment's current state.
        """

        # Based on the current state of the environment, compute the
        # probabilities of taking each action
        act_probas = self._actor.predict(np.reshape(self._old_state, (1, -1)),
                                         batch_size=1).flatten()

        self._log.debug('Current state: {}, action probabilities: '
                        '{}'.format(self._old_state, act_probas))

        # Return the action to perform next, using the computed probabilities
        self._action = np.random.choice(CtrlCfg.OUTPUT_SIZE, p=act_probas)
        return self._action_space[self._action]
