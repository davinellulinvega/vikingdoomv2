#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'davinellulinvega'
from os.path import isfile
from math import sqrt
import numpy as np
from itertools import chain
from h5py import File
from Controller import Controller
from utils import CtrlType, ItemType, EnemyType
from settings import CtrlCfg, PECtrlCfg


def lat_inhib(e):
    """
    Compute the lateral inhibition exerted by each neuron on its neighbours
    in the given layer/tensor.
    :param e: Tensor. A tensor representing the output of a layer.
    :return: Tensor.
    """

    # Import the required keras module
    try:
        import tensorflow.keras.backend as K
    except (ModuleNotFoundError, ImportError):
        import keras.backend as K

    # Get the size of the layer's output
    lay_size = K.int_shape(e)[-1]

    # Build square matrix whose elements equal -1/sqrt(lay_size), save for those
    # on the diagonal which is set to 1
    diag = K.eye(lay_size)
    weights = -(K.ones((lay_size, lay_size)) - diag) / sqrt(lay_size) + diag

    # Return the laterally inhibited values of the layer's output
    return K.dot(e, weights)


class PrimEmoCtrl(Controller):
    """
    A class that controls its corresponding hero using the PrimEmo
    architecture. PrimEmo is a custom architecture mimicking the neural
    circuits at the base of the animal's brain. It involves such brain areas
    as the thalamus, amygdala, hypothalamus, ventral and dorsal striatum,
    the basal ganglia and the dopamine system. It is believed that primitive
    emotions emerge from the activity of these "low-level" circuits.
    """

    def __init__(self, action_space, game_id, hero_id,
                 training=True, monitor=False, misc_config=None):
        """
        Initialize the parent class and declare some additional parameters
        required for the training and decision-making process.
        :param action_space: List. A list of tuples, representing the
        different actions available to the hero (and therefore the controller).
        :param game_id: An integer representing the ID of the current game.
        :param hero_id: An integer representing the ID of the hero associated
        with the current controller.
        :param training: Bool. A boolean value indicating whether the
        specific controller should be in training or testing mode.
        :param monitor: Bool. A boolean value indicating whether the agent's
        behavior should be monitored or not.
        :param misc_config: Dict. A dictionary containing some more parameters
        specific to each controller.
        """

        # Initialize the parent class
        super(PrimEmoCtrl, self).__init__(action_space, game_id,
                                          hero_id, training=training,
                                          monitor=monitor,
                                          misc_config=misc_config)

        # Redefine the array used for storing the monitoring data. Adding
        # some space to record the activities of both the CA and VTA output
        # layers
        self._store = np.empty((0, CtrlCfg.NB_MONITOR_FEATURES +
                                PECtrlCfg.LAYER_SIZES['ca2'] +
                                PECtrlCfg.LAYER_SIZES['vta2']),
                               dtype=np.float32)

        # Declare an empty attribute for holding the architecture
        # It is initialize to None, so that if anything goes wrong during the
        # loading/building process, an exception is sure to be raised quickly
        # after
        self._model = None

        # Get the weights from the configuration, if any
        if misc_config is not None:
            assert isinstance(misc_config, dict), "The configuration given " \
                                                  "in parameter should be a " \
                                                  "dictionary of values."
            self._weights = misc_config.get('weights', None)
        else:
            self._weights = None

        # Overwrite the logging facility
        self._log = PECtrlCfg.LOG

        # Declare a dictionary containing the result of the computation,
        # which in turn will serve as input to the network
        self._net_outputs = {'ba': np.zeros((1, PECtrlCfg.LAYER_SIZES['ba2'])),
                             'ca': np.zeros((1, PECtrlCfg.LAYER_SIZES['ca2'])),
                             'vta': np.zeros((1, PECtrlCfg.LAYER_SIZES[
                                 'vta2']))}

        # Initialize the groups of layers separately
        self._net_outputs.update({'stn_{}'.format(idx): np.zeros((1, lay_size))
                                  for idx, lay_size in enumerate([PECtrlCfg.LAYER_SIZES['stn']] * len(self._action_space))})
        self._net_outputs.update({'pfc_{}'.format(idx): np.zeros((1, lay_size))
                                  for idx, lay_size in enumerate([PECtrlCfg.LAYER_SIZES['pfc']] * len(self._action_space))})
        self._net_outputs.update({'mc_{}'.format(idx): np.zeros((1, lay_size))
                                  for idx, lay_size in enumerate([PECtrlCfg.LAYER_SIZES['mc']] * len(self._action_space))})

        # If none of the important files exist
        required_files = [PECtrlCfg.MODEL_FILE_PATH, PECtrlCfg.STORE_FILE_PATH]
        # Try to download the files
        self._download([file for file in required_files if not isfile(file)])

    def init(self, env):
        """
        Contains the logic related to the initialization of the different
        components. This method is only present because Keras, as it stands,
        does not play nice with multi-processing and requires its library to be
        loaded in particular ways. Therefore, this method should be called
        after start() has been triggered and the controller is running in its
        own process.
        :param env: Environment. An instance of the Environment class,
        containing the initial game's state.
        :return: Nothing
        """

        # Let the parent class do its initialization first
        super(PrimEmoCtrl, self).init(env)

        # Check if the weights were specified
        if self._weights is not None:
            # Let the user know that we are loading the custom weights
            self._log.debug('Loading Weights from custom dictionary ...')
            # Set the model's weights
            self._model.set_weights(self._weights)
            # Set the weights attribute back to None, to allow the garbage
            # collector to remove the array of weights from memory
            self._weights = None
            # Another silly message
            self._log.debug('Done.')

    @classmethod
    def upload(cls, files=None):
        """
        Upload the different monitoring data and saved model to the cloud,
        for safety and backup purposes.
        :param files: List. A list of paths to the files to upload to the
        VikingDoom bucket. The paths should be specified as string.
        :return: Nothing.
        """

        # Make sure the files provided as argument are compatible
        if files is None:
            files = [PECtrlCfg.STORE_FILE_PATH, PECtrlCfg.MODEL_FILE_PATH]

        # Provide the correct list of files to the parent's function
        super(PrimEmoCtrl, cls).upload(files)

    def save(self):
        """
        Save the PrimEmo architecture and the monitoring data using the default
        paths specified within the settings module.
        :return: Nothing.
        """

        # Only save the model and the monitoring data when required
        if self._monitoring:
            # Save the monitoring data for the current game into the dedicated
            # monitoring file
            storage_file = File(PECtrlCfg.STORE_FILE_PATH, mode='a',
                                track_order=True)
            storage_file.create_dataset(str(self._game_id), data=self._store,
                                        chunks=True, track_order=True)

            # Close the storage file
            storage_file.close()

            # Save the updated PrimEmo architecture
            self._model.save(PECtrlCfg.MODEL_FILE_PATH)

    def to_dict(self):
        """
        Build a representation of the current controller in the form of a dict.
        :return: Dict. A dictionary containing the main characteristics of the
        current controller.
        """

        # Get an initialized description from the parent class
        desc = super(PrimEmoCtrl, self).to_dict()

        # Return the description augmented with the type of controller
        # implemented by this class
        desc['type'] = CtrlType.PRIM_EMO.value
        return desc

    def _set_session(self):
        """
        Configure and set Keras' default session, so has to limit the amount
        of GPU memory used per process.
        :return: Nothing.
        """

        # Import the required modules and classes from within the method,
        # to avoid any problems linked to the multi-processing environment
        from tensorflow import ConfigProto, Session
        try:
            from tensorflow.keras import backend as K
        except (ModuleNotFoundError, ImportError):
            from keras import backend as K

        # Configure keras' default session
        config = ConfigProto()
        config.gpu_options.allow_growth = CtrlCfg.ALLOW_GROWTH
        config.gpu_options.per_process_gpu_memory_fraction = CtrlCfg.GPU_MEMORY_FRACTION

        # Set Keras default session
        K.set_session(Session(config=config))

    def _build(self):
        """
        Build the PrimEmo architecture from scratch. The different layers,
        standing in for the brain areas, as well as the connections in
        between them are part of the PrimEmo blueprint, introduced by this
        project.
        :return: Nothing.
        """

        # Before any thing else set the session
        self._set_session()

        # Import some required module from the keras library
        try:
            from tensorflow.keras.models import Model
            from tensorflow.keras.layers import Input, Softmax, Concatenate
            import tensorflow.keras.backend as K
        except (ModuleNotFoundError, ImportError):
            from keras.models import Model
            from keras.layers import Input, Softmax
            import keras.backend as K

        # Declare two variables to store the inputs and outputs that are
        # important to the model
        ins = {}
        outs = {}

        # Declare the different input layers
        ins['int_in'] = Input(batch_shape=(1, PECtrlCfg.LAYER_SIZES['int_in']),
                              name='internal')
        ins['ext_in'] = Input(batch_shape=(1, PECtrlCfg.LAYER_SIZES['ext_in']),
                              name='external')
        ins['rew_in'] = Input(batch_shape=(1, PECtrlCfg.LAYER_SIZES['rew_in']),
                              name='reward')

        ins['ba'] = Input(batch_shape=(1, PECtrlCfg.LAYER_SIZES['ba2']),
                          name='ba_in')
        ins['ca'] = Input(batch_shape=(1, PECtrlCfg.LAYER_SIZES['ca2']),
                          name='ca_in')
        ins['vta'] = Input(batch_shape=(1, PECtrlCfg.LAYER_SIZES['vta2']),
                           name='vta_in')

        # Declare groups of input for the PFC and MC layers
        ins['stn'] = [Input(batch_shape=(1, lay_size),
                            name='stn_{}_in'.format(idx))
                      for idx, lay_size in enumerate([PECtrlCfg.LAYER_SIZES['stn']] * len(self._action_space))]
        ins['pfc'] = [Input(batch_shape=(1, lay_size),
                            name='pfc_{}_in'.format(idx))
                      for idx, lay_size in enumerate([PECtrlCfg.LAYER_SIZES['pfc']] * len(self._action_space))]
        ins['mc'] = [Input(batch_shape=(1, lay_size),
                           name='mc_{}_in'.format(idx))
                     for idx, lay_size in enumerate([PECtrlCfg.LAYER_SIZES['mc']] * len(self._action_space))]

        # Declare the LHA layer
        lha_out = self._build_layer('lha', [ins['ca'], ins['int_in']],
                                    modulation=ins['vta'])

        # Declare the hypothalamic layer
        hypo_out = self._build_layer('hypot', [ins['ba'], lha_out])

        # Declare the thalamic layer
        thal1_out = self._build_layer('thal1', [ins['ext_in'], -ins['ca']])

        # Declare the layer representing the lateral nucleus of the amygdala
        la1_out = self._build_layer('la1', thal1_out)
        la2_out = self._build_layer('la2', la1_out)

        # Declare the layers representing the basal nucleus of the amygdala
        ba1_out = self._build_layer('ba1', [la2_out, hypo_out] +
                                    [lay for lay in chain(ins['pfc'], ins['mc'])])

        outs['ba'] = self._build_layer('ba2', ba1_out)

        # Declare the layer representing the Intercalated nucleus of the
        # amygdala
        itc1_out = self._build_layer('itc1', outs['ba'])
        itc2_out = self._build_layer('itc2', itc1_out)

        # Declare the somato-sensory cortex
        ssc_out = self._build_layer('ssc', thal1_out)

        # Declare the layer representing the central nucleus of the amygdala
        ca1_out = self._build_layer('ca1', [-itc2_out, outs['ba'], hypo_out,
                                            ssc_out])
        outs['ca'] = self._build_layer('ca2', ca1_out, modulation=ins['vta'])

        # Declare the PendunculoPontine Tegmental layer
        ppt_out = self._build_layer('ppt', [lha_out, outs['ca']])

        # Declare both of the Ventral Striatum layers
        vs1_out = self._build_layer('vs1', [outs['ba'], ssc_out] +
                                    [lay for lay in ins['mc']],
                                    modulation=ins['vta'])
        vs2_out = self._build_layer('vs2', [outs['ba'], ssc_out] +
                                    [lay for lay in ins['mc']],
                                    modulation=ins['vta'])

        # Declare the Ventral Tegmental Area, responsible for all of the
        # modulation
        vta1_out = self._build_layer('vta1', [lha_out, ppt_out, outs['ca'],
                                              -vs1_out, -vs2_out,
                                              ins['rew_in']],
                                     activation='softsign', inhibition=False)
        outs['vta'] = self._build_layer('vta2', vta1_out, activation='softsign',
                                        inhibition=False)

        # Declare both layer groups that make up the Dorsal Striatum
        ds1_outs = self._build_layer_group('ds1', [[lay for lay in ins['mc']],
                                                   [lay for lay in ins['pfc']]],
                                           modulation=outs['vta'])

        ds2_outs = self._build_layer_group('ds2', [[lay for lay in ins['mc']],
                                                   [lay for lay in ins['pfc']]],
                                           modulation=-outs['vta'])

        # Declare the Globus Pallidus external layer
        gpe_outs = self._build_layer_group('gpe', [[-out for out in ds2_outs],
                                                   [lay for lay in ins['stn']]])

        # Declare the Sub-Thalamic Nucleus layer
        outs['stn'] = self._build_layer_group('stn', [[-out for out in gpe_outs],
                                                      [lay for lay in ins['pfc']],
                                                      [lay for lay in ins['mc']]])

        # Declare the Substantia Nigra pars Reticulata layer
        snr_outs = self._build_layer_group('snr', [[-out for out in gpe_outs],
                                                   [-out for out in ds1_outs],
                                                   outs['stn']])

        # Declare the second thalamic layer
        thal2_outs = self._build_layer_group('thal2', [[-out for out in snr_outs],
                                                       [lay for lay in ins['pfc']],
                                                       [lay for lay in ins['mc']]])

        # Declare the Pre-Frontal Cortex layer
        outs['pfc'] = self._build_layer_group('pfc', [thal2_outs,
                                                      [outs['ca']] * len(self._action_space),
                                                      [ssc_out] * len(self._action_space)])

        # Declare the Motor Cortex layer
        outs['mc'] = self._build_layer_group('mc', [thal2_outs, outs['pfc']])

        # Declare the main output of the architecture, which is only an
        # average of the MC layer
        outs['main'] = Concatenate()([K.mean(lay, keepdims=True) for lay in
        outs['mc']])

        # Declare the model, providing the input and output layers as parameters
        self._model = Model(inputs=ins,
                            outputs=[outs['ba'], outs['ca'], outs['vta'],
                                     *[lay for lay in outs['stn']],
                                     *[lay for lay in outs['pfc']],
                                     *[lay for lay in outs['mc']],
                                     outs['main']])

    def _build_layer_group(self, name, inputs, activation='softsign',
                           modulation=None, size=None):
        """
        Build a group of layers, each complete with modulation (if necessary),
        lateral inhibition and relu/softsign activation.
        :param name: Str. A string representing the name of the layer.
        :param inputs: List. A list of lists of tensors.
        :param activation: Str. A string representing the activation function
        to use for the layer.
        :param modulation: Tensor. A tensor, whose output/content will be
        used to modulate the layer's output.
        :param size: Int or List. If an integer is provided, all layers
        within the group will have the same defined size. If a list is given,
        then each item in the list is interpreted to be the size of a layer
        within the group.
        :return: List. A list of tensors each representing the output of a
        layer.
        """

        # Provide a default value for the layer size
        if size is None:
            size = [PECtrlCfg.LAYER_SIZES[name]] * len(self._action_space)
        elif isinstance(size, int):
            size = [size] * len(self._action_space)

        # Return a list of layers, each with a unique name
        return [self._build_layer("{}_{}".format(name, idx),
                                  list(lay_ins),
                                  activation=activation,
                                  modulation=modulation,
                                  size=lay_size)
                for idx, (*lay_ins, lay_size) in enumerate(zip(*inputs, size))]

    def _build_layer(self, name, inputs, activation='softsign',
                     modulation=None, inhibition=True, size=None):
        """
        Build a layer, complete with modulation (if necessary), lateral
        inhibition and relu/softsign activation.
        :param name: Str. A string representing the name of the layer.
        :param inputs: List. A list of input tensors.
        :param activation: Str. A string representing the activation function
        to use for the layer.
        :param modulation: Tensor. A tensor, whose output/content will be
        used to modulate the layer's output.
        :param inhibition: Bool. A boolean indicating whether to apply
        inhibition to the layer or not.
        :param size: Int. An integer defining the size of the layer.
        :return: Tensor. A tensor representing the output of the layer
        """

        # Let the user know which layer is being build
        self._log.debug('Building {} layer ...'.format(name))

        # Provide a default value for the size parameter based on the layer's
        # name
        if size is None:
            size = PECtrlCfg.LAYER_SIZES[name]

        # Import some required module from the keras library
        try:
            from tensorflow.keras.layers import Dense, Multiply, Concatenate, \
                Lambda, Activation
            from tensorflow.keras.initializers import constant
        except (ModuleNotFoundError, ImportError):
            from keras.layers import Dense, Multiply, Concatenate, Lambda, \
                Activation
            from keras.initializers import constant

        # Concatenate the list of inputs into a single tensor if necessary
        if isinstance(inputs, list):
            inputs = Concatenate(name='{}_ins'.format(name))(inputs)

        # Build the densely connected layer
        x = Dense(size, use_bias=True,
                  bias_initializer=constant(PECtrlCfg.BIAS),
                  name='{}_dense'.format(name))(inputs)

        # Should we modulate the output?
        if modulation is not None:
            x = Multiply(name='{}_mod'.format(name))([modulation, x])

        # Apply lateral inhibition if necessary/required
        if inhibition:
            x = lat_inhib(x)

        # Finally activation and were are done
        return Activation(activation=activation.lower(), name=name)(x)

    def _activate(self, curr_state, reward=0):
        """
        Loop over the prediction process until reaching stable output
        activity. The number of loop is restricted to a certain number of
        iterations, to avoid having an infinite loop.
        :param curr_state: List. A list containing the agent's observations
        of the current state of the environment.
        :param reward: Float. A float value representing the reward
        corresponding to the previous action.
        :return: Np.ndarray. A numpy array containing the activation values
        of the output layer.
        """

        # Split the current state into two sets of inputs: one corresponding
        # to the agent's internal state and the other to the environment (or
        # external)
        external_in = np.reshape(curr_state[:-2], (1, -1))
        internal_in = np.reshape(curr_state[-2:], (1, -1))

        # Rectify the reward using a set coefficient
        reward_in = np.reshape(max(-1, min(1, reward)), (1, 1))

        # Until we've reached the maximum number of iteration
        old_out = None
        for it in range(PECtrlCfg.MAX_STABLE_ITER):
            # Build the dictionary of inputs
            ins = {'external': external_in,
                   'internal': internal_in,
                   'reward': reward_in,
                   'ca_in': PECtrlCfg.RDR * self._net_outputs['ca'],
                   'ba_in': PECtrlCfg.RDR * self._net_outputs['ba'],
                   'vta_in': PECtrlCfg.RDR * self._net_outputs['vta']}

            ins.update({'stn_{}_in'.format(idx): PECtrlCfg.RDR * self._net_outputs[
                'stn_{}'.format(idx)]
                        for idx in range(len(self._action_space))})
            ins.update({'pfc_{}_in'.format(idx): PECtrlCfg.RDR * self._net_outputs[
                'pfc_{}'.format(idx)]
                        for idx in range(len(self._action_space))})
            ins.update(
                {'mc_{}_in'.format(idx): PECtrlCfg.RDR * self._net_outputs['mc_{}'.format(idx)]
                 for idx in range(len(self._action_space))})

            # Have the model predict the probabilities for each of the available
            # actions
            acts = self._model.predict(ins)

            # Extract the different outputs from the result of the prediction
            # process
            output = acts.pop().flatten()
            for idx in reversed(range(len(self._action_space))):
                self._net_outputs['mc_{}'.format(idx)] = acts.pop()
            for idx in reversed(range(len(self._action_space))):
                self._net_outputs['pfc_{}'.format(idx)] = acts.pop()
            for idx in reversed(range(len(self._action_space))):
                self._net_outputs['stn_{}'.format(idx)] = acts.pop()
            self._net_outputs['vta'] = acts.pop()
            self._net_outputs['ca'] = acts.pop()
            self._net_outputs['ba'] = acts.pop()

            # Check if stability has been reached or not (i.e.: whether the
            # difference between the new and old output is under threshold)
            if old_out is not None and np.max(np.abs(output - old_out)) < PECtrlCfg.STABLE_THRES:
                # We've reached stability, let's break out of this madness
                break

            # Record the output for later computation
            old_out = output

            # Leave a message, to the user, containing the value of the
            # output's activities on each iteration
            self._log.debug('On iteration {} output activity '
                            'was: {}'.format(it, output))

        # Finally return the stable or last computed output activities
        return old_out

    def _load(self):
        """
        Load the PrimEmo architecture from the default location.
        :return: Bool. True if loading was successful, False otherwise.
        """

        # But before anything else, set the session
        self._set_session()

        # Import some required method from the keras library
        try:
            from tensorflow.keras.models import load_model
        except (ModuleNotFoundError, ImportError):
            from keras.models import load_model

        try:
            # Try to load the model from the default location
            self._model = load_model(PECtrlCfg.MODEL_FILE_PATH)
        except OSError as e:
            # Log the error for ease of debugging
            self._log.exception('An error occurred while trying to load the '
                                'model from file: {}'.format(e.strerror))

            # Something went wrong, so return False
            return False
        except:
            # Log the error for ease of debugging
            self._log.exception('An error occurred while trying to load the '
                                'model from file, but we do not know what.')

            # Something went wrong, so return False
            return False
        else:
            # Everything went better than expected
            return True

    def _learn(self, old_state, action, curr_state, reward, done):
        """
        Since the PrimEmo architecture is using a Genetic Algorithm to find
        the optimal weights for this specific task, there is no learning
        happening at the controller level.
        :param old_state: List. A list of extracted features describing the
        previous state of the environment.
        :param action: Tuple. A tuple representing the action performed to go
        from the old to the current state.
        :param curr_state: List. A list of extracted features describing the
        current state of the environment.
        :param reward: Float. A float indicating the how good/bad the action
        undertaken was.
        :param done: Bool. Signal the end of an episode/task.
        """

        pass

    def action(self, reward=0):
        """
        Based on the given current state, chose the next action to perform.
        :param reward: Float. A float value representing the reward
        corresponding to the previous action. This is only required for the
        PrimEmoCtrl controller. Hence, it is initialized to 0, by default.
        :return: Int. An integer representing the action to perform next,
        given the environment's current state.
        """

        # Activate the model until stability is reached over the activation
        # values of the output layer
        output = self._activate(self._old_state, reward)

        # Return the index of an action based on the levels of output activity
        self._action = np.argmax(output)
        return self._action_space[self._action]

    def _monitor(self, curr_env_state, curr_hero, action, reward, done):
        """
        Store all the necessary information within the storage file,
        to monitor the learning process.
        :param curr_env_state: List. A list of the features describing the
        environment's current state.
        :param curr_hero: Hero. An instance of the Hero class, representing
        the hero controlled by the current Controller.
        :param action: Int. An integer representing the action performed to
        reach the current state of the environment and corresponding to the
        reward gathered.
        :param reward: Float. The reward corresponding to performing the
        action in the context of the previous state.
        :param done: Bool. A boolean value indicating whether the
        episode/task has ended or not.
        :return: Nothing.
        """

        # Simply append the current cycle's data to the end of the storage
        self._store = np.vstack((self._store,
                                 [self._cycle, int(done), reward, action,
                                  int(curr_hero.action_is_valid),
                                  *curr_env_state,
                                  *[curr_hero.nb_fights[enemy_type]
                                    for enemy_type in EnemyType],
                                  curr_hero.nb_fights['hero'],
                                  *[curr_hero.nb_items_gathered[item_type]
                                    for item_type in ItemType],
                                  *self._net_outputs['ca'].flatten(),
                                  *self._net_outputs['vta'].flatten()
                                  ]))


if __name__ == "__main__":
    # Import some required configuration
    from settings import EnvCfg, GACfg

    print('Building architecture ...')
    # Simply build the PrimEmo controller from scratch
    ctrl = PrimEmoCtrl(EnvCfg.ACTION_SPACE, 0, 0)
    ctrl._build()
    print('Done.')

    print('Getting weights ...')
    # Get the models weights
    weights = ctrl._model.get_weights()
    print('Done.')

    # Print the name of the layers in the order in which they are declared
    # within the keras model
    print('Model layers:')
    for lay in ctrl._model.layers:
        if lay.get_config().get('activation', None) is not None and '_dense' \
                not in lay.name:
            print(lay.name)

    # Print the shape of the different weights to the console
    print('\nModel weights:')
    for w, w_cfg in zip(weights, GACfg.WEIGHT_SHAPES):
        print('{} == {} is {}'.format(w.shape, w_cfg, w.shape == w_cfg))
