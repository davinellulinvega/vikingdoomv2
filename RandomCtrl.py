#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'davinellulinvega'
from os.path import isfile
from random import choice
from h5py import File
from Controller import Controller
from settings import RndCtrlCfg


class RandomCtrl(Controller):
    """
    Implement a controller that manipulates one of the heroes in the game.
    Since it only chooses actions to perform randomly, its purpose is more
    geared toward debugging and setting a base line for the players.
    """

    def __init__(self, action_space, game_id, hero_id,
                 training=True, monitor=False, misc_config=None):
        """
        Declare some common attributes, such as Queues for communication with
        the game engine, as well as the ID of the corresponding hero. The whole
        hero object is not used here, since it would just mean a redundancy in
        the data.
        Furthermore, each controller is its own process. This is done to
        increase the processing speed, especially, since players do not have to
        be queried sequentially for action.
        :param action_space: List. A list of tuples, representing the
        different actions available to the hero (and therefore the controller).
        :param game_id: An integer representing the ID of the current game.
        :param hero_id: An integer representing the ID of the hero associated
        with the current controller.
        :param training: Bool. A boolean value indicating whether the
        specific controller should be in training or testing mode.
        :param monitor: Bool. A boolean value indicating whether the agent's
        behavior should be monitored or not.
        :param misc_config: Dict. A dictionary containing some more parameters
        specific to each controller.
        """

        # Initialize the parent class
        super(RandomCtrl, self).__init__(action_space, game_id,
                                         hero_id, training=training,
                                         monitor=monitor,
                                         misc_config=misc_config)

        # Overwrite the logging facility
        self._log = RndCtrlCfg.LOG

        # If none of the important files exist
        required_files = [RndCtrlCfg.STORE_FILE_PATH]
        # Try to download the file
        if not isfile(RndCtrlCfg.STORE_FILE_PATH):
            self._download(required_files)

    @classmethod
    def upload(cls, files=None):
        """
        Upload the different monitoring/data/result file to the cloud,
        for backups and ease of retrieval.
        :param files: List. A list of paths to the files to upload to the
        VikingDoom bucket. The paths should be specified as string.
        :return: Nothing.
        """
        # Make sure the files provided as argument are compatible
        if files is None:
            files = [RndCtrlCfg.STORE_FILE_PATH]

        # Provide the correct list of files to the parent's function
        super(RandomCtrl, cls).upload(files)

    def save(self):
        """
        Nothing to save here.
        :return: Nothing.
        """

        # Save the monitoring data for the current game into the dedicated
        # monitoring file, when required
        if self._monitoring:
            storage_file = File(RndCtrlCfg.STORE_FILE_PATH, mode='a',
                                track_order=True)
            storage_file.create_dataset(str(self._game_id), data=self._store,
                                        chunks=True, track_order=True)

            # Close the storage file
            storage_file.close()

    def _learn(self, old_state, action, curr_state, reward, done):
        """
        Silly you this controller knows nothing and learns even less.
        :param old_state: List. A list of extracted features describing the
        previous state of the environment.
        :param action: Tuple. A tuple representing the action performed to go
        from the old to the current state.
        :param curr_state: List. A list of extracted features describing the
        current state of the environment.
        :param reward: Float. A float indicating the how good/bad the action
        undertaken was.
        :param done: Bool. Signal the end of an episode/task.
        :return: Nothing.
        """

        pass

    def _build(self):
        """
        This is getting rather boring. Still nothing to be done here.
        :return: Nothing.
        """

        pass

    def _load(self):
        """
        This is getting rather boring. Still nothing to be done here.
        :return: Nothing.
        """

        # Nothing is being done, so nothing can fail and it makes sure that
        # we do not try to call the _build() method
        return True

    def action(self, reward=0):
        """
        Based on the available actions within the environment, return one at
        random.
        :param reward: Float. A float value representing the reward
        corresponding to the previous action. This is only required for the
        PrimEmoCtrl controller. Hence, it is initialized to 0, by default.
        :return: Tuple. A tuple representing the next action to perform.
        """

        # Randomly choose the index of the next action to perform
        # This is required for monitoring to work correctly
        self._action = choice(range(len(self._action_space)))

        # Return the corresponding action
        return self._action_space[self._action]
