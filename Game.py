#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'davinellulinvega'
from uuid import uuid4
from socketio.exceptions import ConnectionError, SocketIOError
from socketio import Client
import json
from ControllerFactory import ControllerFactory as CtrlFact
from Environment import Environment
from settings import GameCfg, CtrlCfg

# TODO: Configuring Tensorflow's session should be done here,
# if any controller uses the GPU, rather than within each controller.


class Game(object):
    """
    A simple object managing the communication between the environment and
    the different controllers, making decisions for the heroes. The Game is
    in charge of the main game cycle.
    """

    def __init__(self, env_config_file, controllers=None, ctrls_configs=None,
                 training=True, monitor=False, ws_host=None, ws_port=None,
                 render=True):
        """
        Initialize the environment and add some heroes to it, for the players
        to control.
        :param env_config_file: A string representing the absolute path
        to the
        file, from which to load the environment.
        :param controllers: A list of string corresponding to the name of the
        controllers to instantiate and use to decide the next action of the
        corresponding hero.
        :param ctrls_configs: A list of dictionaries, each providing
        additional runtime configuration to the corresponding controllers.
        :param training: Bool. A boolean indicating whether the different
        controllers should be instantiated in their training or testing mode.
        :param monitor: Bool. A boolean value indicating whether the agent's
        behavior should be monitored or not.
        :param ws_host: A string representing the hostname of the websocket
        server to connect to for rendering.
        :param ws_port: An integer representing the port on which the
        websocket server is listening.
        :param render: A boolean value indicating whether the game should
        send data out for rendering and monitoring.
        """

        # Call on the parent class
        super(Game, self).__init__()

        # Give a unique id to the game
        self._id = uuid4().int

        # Initialize and add an environment attribute
        self._env = Environment(env_config_file)

        # Initialize the websocket client and set the render variable
        self._render = render
        self._ws_host = ws_host
        self._ws_port = ws_port
        if render and ws_host and ws_port:
            self._ws_clt = Client(reconnection_attempts=5, logger=GameCfg.LOG,
                                  json=json)
        else:
            self._ws_clt = None

        # Initialize both the list of controllers and the list of
        # corresponding parameters
        self._controllers = {}
        self._ctrls_params = {'types': controllers, 'training': training,
                              'monitor': monitor, 'configs': ctrls_configs}

        # Define a list to store the total reward gathered by each of the controllers
        self._total_rewards = {}

    def play(self):
        """
        This the main method of the Game class. After initializing the
        different controllers, it manages the process of actually playing a
        single game. Finally, whatever the reason, it gracefully shuts down.
        :return: Nothing.
        """

        # This should only be done if controllers were provided
        if self._ctrls_params['types']:
            if self._ctrls_params['configs'] is not None:
                # For each of the controller
                for ctrl, config in zip(self._ctrls_params['types'],
                                        self._ctrls_params['configs']):
                    # Add a player to the game
                    self.add_player(ctrl, self._ctrls_params['training'],
                                    self._ctrls_params['monitor'],
                                    ctrl_config=config)
            else:
                # For each of the controller
                for ctrl in self._ctrls_params['types']:
                    # Add a player to the game
                    self.add_player(ctrl, self._ctrls_params['training'],
                                    self._ctrls_params['monitor'])

        # Try to connect to the rendering server
        if self._render:
            try:
                self._ws_clt.connect('http://{}:{}'.format(self._ws_host,
                                                           self._ws_port))
            except (ConnectionError, SocketIOError):
                GameCfg.LOG.exception('An error occurred while connecting to '
                                      'the display server. Rendering is '
                                      'disabled for this game.')
                self._render = False

        # Initialize the environment
        GameCfg.LOG.info("Resetting the environment ...")
        self._env.reset()
        GameCfg.LOG.info("Done.")

        # Start all the controller processes
        for ctrl in self._controllers.values():
            ctrl.init(self._env)

        # Let the user know that the game is starting
        GameCfg.LOG.info("Starting game: {}".format(self._id))

        # Play the actual game
        try:
            # Until all players are dead or the maximum number of turns has
            # been reached
            while self._controllers and (GameCfg.MAX_TURN is None or
                                         self._env.turn < GameCfg.MAX_TURN):
                GameCfg.LOG.info("Playing turn {}".format(self._env.turn))
                # Emit an event to the Display server to render the provided
                # environment's state
                if self._render:
                    GameCfg.LOG.info("Rendering the environment ...")
                    try:
                        self._ws_clt.emit(GameCfg.RENDER_EVT_NAME,
                                          data=self._env.to_dict())
                    except:
                        GameCfg.LOG.exception('An error occurred while '
                                              'sending data to the display '
                                              'server. Rendering is disabled '
                                              'for this game.')
                        self._render = False
                    GameCfg.LOG.info("Done.")

                # Initialize the list of actions
                actions = {}

                # Wait for the controllers to send their decision back
                GameCfg.LOG.info("Getting player actions ...")

                for hero_id, ctrl in self._controllers.items():
                    try:
                        actions[hero_id] = ctrl.action()
                    except ValueError:
                        self._controllers.pop(hero_id)
                        self._total_rewards[hero_id] = GameCfg.MAX_TURN * \
                                                       CtrlCfg.DEATH_PUNISHMENT

                GameCfg.LOG.info("Done: {}".format(actions))

                # Remove any dead player
                heroes_to_delete = [hero_id for hero_id in self._controllers
                                    if self._env.heroes[hero_id].is_dead]
                if heroes_to_delete:
                    GameCfg.LOG.info("Deleting "
                                     "heroes: {} ...".format(heroes_to_delete))
                    self._delete_player(heroes_to_delete)
                    GameCfg.LOG.info("Done.")

                # Move the environment forward one step at a time
                GameCfg.LOG.info("Move the environment forward ...")
                self._env.step(actions)
                GameCfg.LOG.info("Done.")

                # Move all remaining controllers forward one step at a time
                for ctrl in self._controllers.values():
                    ctrl.step(self._env)

                # Increase the number of turns played
                self._env.turn += 1

        finally:
            # Let the user know that the game ended
            GameCfg.LOG.info("Quitting game: {}".format(self._id))

            # If we were rendering something
            if self._render:
                # Warn the different client that the game has ended
                self._ws_clt.emit('game_over')

    def shutdown(self):
        """
        Ask for all controller to stop their processing and gracefully shut
        down. Disconnect from the Display server and clean after itself.
        :return: List. A list containing the total reward corresponding to
        the different controllers.
        """

        # Delete any remaining controllers
        if self._controllers:
            self._delete_player(list(self._controllers))

        # Disconnect from the display server
        if self._ws_clt:
            self._ws_clt.disconnect()

    def add_player(self, ctrl_type, training, monitor, ctrl_config=None):
        """
        Add a controller, and therefore a hero, to the game. This also
        changes the size of the input queue and add an output queue dedicated to
        the new controller.
        :param ctrl_type: CtrlType. An instance of the CtrlType enumeration,
        indicating the type of controller to instantiate.
        :param training: Bool. A boolean indicating whether the different
        controllers should be instantiated in their training or testing mode.
        :param monitor: Bool. A boolean value indicating whether the agent's
        behavior should be monitored or not.
        :param ctrl_config: Dictionary. A dictionary containing additional
        specific configuration for the controller.
        :return: Nothing.
        """

        # Make sure the game has not reached the maximum number of players yet
        if len(self._controllers) < self._env.max_heroes:
            # Add a hero to the environment
            hero_id = self._env.add_hero()

            # Instantiate the controller, based on its name
            new_ctrl = CtrlFact.get_controller(ctrl_type,
                                               self._env.action_space,
                                               self._id, hero_id, training,
                                               monitor, ctrl_config)

            # Add the controller in the list
            self._controllers[hero_id] = new_ctrl

            # Let the user know that a new player has been added to the game
            GameCfg.LOG.info("Added a new player to the game: "
                             "ID: {}, type: {}".format(hero_id,
                                                       ctrl_type.value))

        else:
            GameCfg.LOG.error("An error occurred while adding a hero to the "
                              "game: The maximum number of {} players has "
                              "already been reached.".format(self._env.max_heroes))

    def _delete_player(self, hero_ids):
        """
        Remove the controller and communication queues managing the hero with
        the given ID.
        :param hero_ids: A list of integers representing the heroes' IDs.
        :return: Nothing.
        """

        # If the given parameter is not a list
        if not isinstance(hero_ids, list):
            # Wrap it into a list
            hero_ids = [hero_ids]

        # For each of the hero IDs
        for hero_id in hero_ids:
            # Remove the controller from the list
            ctrl = self._controllers.pop(hero_id)

            # Call the controller's step() method a final time
            ctrl.step(self._env)

            # Remove the corresponding hero from the environment
            self._env.delete_hero(hero_id)

            # Gather the controller's total reward, for later processing
            self._total_rewards[hero_id] = ctrl.total_reward

            # Save and upload any relevant file
            ctrl.save()
            ctrl.upload()

            # Let the user know that a player has been deleted from the game
            GameCfg.LOG.info("Deleted player: {}".format(hero_id))

    @property
    def id(self):
        return self._id

    @property
    def total_rewards(self):
        return self._total_rewards
