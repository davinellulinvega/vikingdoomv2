#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'davinellulinvega'
from os.path import isfile
from random import random, choice, sample
from h5py import File
import numpy as np
from collections import deque
from Controller import Controller
from settings import DQCtrlCfg, CtrlCfg
from utils import CtrlType


def build_network():
    """
    Build a deep densly connected neural network, used for learning the
    Q-values associated with the different states.
    :return: Model. An instance of the Sequential class, part of the
    Keras framework.
    """

    # Import all required tensorflow modules
    try:
        from tensorflow.keras import Sequential
        from tensorflow.keras.layers import Dense
        from tensorflow.keras.optimizers import Adam
        from tensorflow.keras.initializers import Constant
    except (ModuleNotFoundError, ImportError):
        from keras import Sequential
        from keras.layers import Dense
        from keras.optimizers import Adam
        from keras.initializers import Constant

    # Build the prediction network
    model = Sequential()

    # Add the first layer independently, so that we may specify the
    # dimension of the input
    model.add(Dense(DQCtrlCfg.LAYER_SIZES[0],
                    input_shape=(CtrlCfg.INPUT_SIZE,),
                    activation='relu', use_bias=True,
                    kernel_initializer='he_normal',
                    bias_initializer=Constant(value=0.1)))

    # Add the rest of the layers
    for layer_size in DQCtrlCfg.LAYER_SIZES[1:]:
        model.add(Dense(layer_size, use_bias=True, activation='relu',
                        kernel_initializer='he_normal',
                        bias_initializer=Constant(value=0.1)))

    # Add the output layer, which uses a linear activation
    model.add(Dense(CtrlCfg.OUTPUT_SIZE, use_bias=True,
                    kernel_initializer='he_normal',
                    bias_initializer=Constant(value=0.1)))

    # Compile the prediction network for training
    model.compile(loss='mse',
                  optimizer=Adam(lr=DQCtrlCfg.LEARNING_RATE,
                                 decay=DQCtrlCfg.LR_DECAY_RATE,
                                 amsgrad=DQCtrlCfg.AMSGRAD))

    # Return the initialized model
    return model


class DeepQCtrl(Controller):
    """
    Defines a class that can control a hero using a Deep Q-learning
    algorithm. Deep Q-learning is not much different from the standard
    Q-learning algorithm, aside from being implemented in a deep neural
    network and making use of both a memory queue and target network,
    to speed up and stabilize the learning process.
    """

    def __init__(self, action_space, game_id, hero_id,
                 training=True, monitor=False, misc_config=None):
        """
        Initialize the parent class and declare some of the required attributes.
        :param action_space: List. A list of tuples, representing the
        different actions available to the hero (and therefore the controller).
        :param game_id: An integer representing the ID of the current game.
        :param hero_id: An integer representing the ID of the hero associated
        with the current controller.
        :param training: Bool. A boolean value indicating whether the
        specific controller should be in training or testing mode.
        :param monitor: Bool. A boolean value indicating whether the agent's
        behavior should be monitored or not.
        :param misc_config: Dict. A dictionary containing some more parameters
        specific to each controller.
        """

        # Initialize the parent class providing a default location for
        # storing monitoring data
        super(DeepQCtrl, self).__init__(action_space, game_id,
                                        hero_id, training=training,
                                        monitor=monitor,
                                        misc_config=misc_config)

        # Declare a simple attribute to store the neural network
        # Initialized to None, so that if all else fail an exception is sure
        # to occur early on
        self._predict = None

        # The target and memory attributes are used to speed up and stabilize
        # learning within this controller
        # Again, the target attribute is initialized to None, so that if
        # everything else fail an exception is sure to occur early on
        self._target = None

        # Declare an experience replay queue
        self._memory = deque(maxlen=DQCtrlCfg.MEMORY_MAX_SIZE)

        # Declare an epsilon parameter for the epsilon-greedy policy (
        # choosing the action)
        if training:
            self._epsilon = DQCtrlCfg.EPSILON
        else:
            self._epsilon = DQCtrlCfg.MIN_EPSILON

        # Overwrite the logging facility
        self._log = DQCtrlCfg.LOG

        # If none of the important files exist
        required_files = [DQCtrlCfg.STORE_FILE_PATH,
                          DQCtrlCfg.MODEL_FILE_PATH,
                          DQCtrlCfg.MEMORY_FILE_PATH]
        # Try to download the files
        self._download([file for file in required_files if not isfile(file)])

    def _set_session(self):
        """
        Configure Keras' session so that it does not use all of the GPU
        memory for a single controller.
        :return: Nothing.
        """

        # Import the required tensorflow/keras modules
        from tensorflow import ConfigProto, Session
        try:
            from tensorflow.keras import backend as K
        except (ModuleNotFoundError, ImportError):
            from keras import backend as K

        # Configure keras' default session
        config = ConfigProto()
        config.gpu_options.allow_growth = CtrlCfg.ALLOW_GROWTH
        config.gpu_options.per_process_gpu_memory_fraction = CtrlCfg.GPU_MEMORY_FRACTION

        # Set Keras default session
        K.set_session(Session(config=config))

    def init(self, env):
        """
        For any sub-class, this method should contain the logic related to
        the initialization of the different components. This method is only
        present because Keras, as it stands, does not play nice with
        multi-processing and requires its library to be loaded in particular
        ways. Therefore, this method should be called after start() has been
        triggered and the controller is running in its own process.
        :param env: Environment. An instance of the Environment class,
        containing the initial game's state.
        :return: Nothing
        """

        # Set the session and import all tensorflow related modules
        self._set_session()

        # Let the parent class do its initialization phase
        super(DeepQCtrl, self).init(env)

    def save(self):
        """
        Save the prediction network and the monitoring data using the default
        paths specified within the settings module.
        :return: Nothing.
        """

        # Only save the monitoring data, when required
        if self._monitoring:
            # Save the monitoring data for the current game into the dedicated
            # monitoring file
            storage_file = File(DQCtrlCfg.STORE_FILE_PATH, mode='a',
                                track_order=True)
            storage_file.create_dataset(str(self._game_id), data=self._store,
                                        chunks=True, track_order=True)

            # Close the storage file
            storage_file.close()

        # Save the knowledge gained so far to file, so that subsequent run can
        # benefit
        if self._training:
            memory_storage = File(DQCtrlCfg.MEMORY_FILE_PATH, mode='w',
                                  track_order=True)
            # Transform each column of the memory into a separate dataset
            old_states, actions, rewards, curr_states, dones = [], [], [], [], []
            for o_s, a, r, c_s, d in self._memory:
                old_states.append(o_s)
                actions.append(a)
                rewards.append(r)
                curr_states.append(c_s)
                dones.append(int(d))

            # Create a new data set for each column
            memory_storage.attrs['epsilon'] = self._epsilon
            memory_storage.create_dataset('old_states', data=old_states)
            memory_storage.create_dataset('actions', data=actions)
            memory_storage.create_dataset('rewards', data=rewards)
            memory_storage.create_dataset('curr_states', data=curr_states)
            memory_storage.create_dataset('dones', data=dones)

            # Close the memory storage
            memory_storage.close()

            # Only the prediction network needs to be save, since the target
            # network is initialized  with the same value
            self._predict.save(DQCtrlCfg.MODEL_FILE_PATH)

    def to_dict(self):
        """
        Build a representation of the current controller in the form of a dict.
        :return: Dict. A dictionary containing the main characteristics of the
        current controller.
        """

        # Get the parent's description
        desc = super(DeepQCtrl, self).to_dict()

        # Return the description augmented with the type of controller
        desc['type'] = CtrlType.DEEP_Q.value
        return desc

    @classmethod
    def upload(cls, files=None):
        """
        Upload the different monitoring data and saved model to the cloud,
        for safety and backup purposes.
        :param files: List. A list of paths to the files to upload to the
        VikingDoom bucket. The paths should be specified as string.
        :return: Nothing.
        """

        # A quick message to the user to let it know what is happening
        cls._log.debug("Uploading all the important file to the cloud ...")

        # Make sure the files provided as argument are compatible
        if files is None:
            files = [DQCtrlCfg.STORE_FILE_PATH,
                     DQCtrlCfg.MODEL_FILE_PATH,
                     DQCtrlCfg.MEMORY_FILE_PATH]

        # Provide the correct list of files to the parent's function
        super(DeepQCtrl, cls).upload(files)

    def _build(self):
        """
        Build both the prediction and target network. For the moment the
        architecture is quite simple, since only dense and relu layers are
        used for building both networks.
        :return: Nothing.
        """

        # A quick message to the user to let it know what is happening
        self._log.debug("Building the Prediction and Target models ...")

        # Build the prediction network
        self._predict = build_network()

        # Build the target network
        self._target = build_network()

    def _load(self):
        """
        Load the prediction and target network from file.
        :return: Bool. True if loading was successful, False otherwise.
        """

        # Import all required tensorflow modules
        try:
            from tensorflow.keras.models import load_model
        except (ModuleNotFoundError, ImportError):
            from keras.models import load_model

        # A quick message to the user to let it know what is happening
        self._log.debug("Load the Prediction and Target models ...")

        try:
            # Try to load the models
            self._predict = load_model(DQCtrlCfg.MODEL_FILE_PATH)
            self._target = build_network()
            self._target.set_weights(self._predict.get_weights())
        except OSError as e:
            # Log the error for ease of debugging
            self._log.exception('An error occurred while trying to load the '
                                'models: {}'.format(e.strerror))
            # Something was not quite right, return False
            return False
        except BaseException as e:
            # Log the error for ease of debugging
            self._log.exception('An error occurred while trying to load the '
                                'models: {}'.format(e))
            # Something was not quite right, return False
            return False

        # Try to load the memory queue from file if it exists
        if self._training and isfile(DQCtrlCfg.MEMORY_FILE_PATH):
            # Open the storage
            memory_storage = File(DQCtrlCfg.MEMORY_FILE_PATH, mode='r')
            # Get the different data sets
            old_states = memory_storage['old_states']
            actions = memory_storage['actions']
            rewards = memory_storage['rewards']
            curr_states = memory_storage['curr_states']
            dones = memory_storage['dones']

            # Fill in the memory
            self._memory.extend([(o_s, a, r, c_s, bool(d)) for o_s, a, r, c_s, d in zip(old_states, actions, rewards, curr_states, dones)])

            # Set the value of epsilon
            self._epsilon = memory_storage.attrs.get('epsilon', DQCtrlCfg.EPSILON)

            # Close the storage
            memory_storage.close()

        # Everything went better than expected
        return True

    def action(self, reward=0):
        """
        Based on the given current state, chose the next action to perform.
        :param reward: Float. A float value representing the reward
        corresponding to the previous action. This is only required for the
        PrimEmoCtrl controller. Hence, it is initialized to 0, by default.
        :return: Int. An integer representing the action to perform next,
        given the environment's current state.
        """

        # A quick message to the user to let it know what is happening
        self._log.debug("Choosing the next action to perform ...")

        if random() < self._epsilon:
            # Return a random action
            self._action, res = choice(list(enumerate(self._action_space)))
            return res
        else:
            # Get a prediction from the environment, based on the current state
            act_probs = self._predict.predict(np.reshape(self._old_state,
                                                         (1, -1)),
                                              batch_size=1)

            # Return the action that corresponds to the highest value
            self._action = np.argmax(act_probs)
            return self._action_space[self._action]

    def _learn(self, old_state, action, curr_state, reward, done):
        """
        Following the Q-learning algorithm, update the value of the previous
        state, based on the value of the current one, the reward and the
        action performed to transition in-between.
        :param old_state: List. A list of extracted features describing the
        previous state of the environment.
        :param action: Tuple. A tuple representing the action performed to go
        from the old to the current state.
        :param curr_state: List. A list of extracted features describing the
        current state of the environment.
        :param reward: Float. A float indicating the how good/bad the action
        undertaken was.
        :param done: Bool. Signal the end of an episode/task.
        :return: Nothing.
        """

        # A quick message to the user to let it know what is happening
        self._log.debug("Learning ...")

        # Store the new experience in the memory stack
        self._memory.append((old_state, action, reward, curr_state,
                             done))

        # If the memory stack is not deep enough we cannot really learn
        if self._cycle < DQCtrlCfg.TRAIN_CYCLE_START:
            # So we just bail out
            return

        # Decay the epsilon parameter used within the policy
        if self._epsilon > DQCtrlCfg.MIN_EPSILON:
            self._epsilon *= DQCtrlCfg.EPSILON_DECAY_RATE

        # Check if the target network should have its parameters replaced
        if self._cycle % DQCtrlCfg.TARGET_UPDATE_RATE == 0:
            self._target.set_weights(self._predict.get_weights())

        # Select the batch of memory to learn on
        batch_size = min(len(self._memory), DQCtrlCfg.BATCH_SIZE)
        batch = sample(self._memory, k=batch_size)

        # Build the input and target for training the prediction network
        update_input = np.zeros((batch_size, CtrlCfg.INPUT_SIZE))
        update_target = np.zeros((batch_size, CtrlCfg.INPUT_SIZE))
        action, reward, done = [], [], []
        for idx, item in enumerate(batch):
            update_input[idx] = item[0]
            action.append(item[1])
            reward.append(item[2])
            update_target[idx] = item[3]
            done.append(item[4])

        # Get the prediction and target for the different inputs
        target = self._predict.predict(update_input)
        target_val = self._target.predict(update_target)

        # Finish building the target values
        for i in range(batch_size):
            target[i][action[i]] = reward[i] + ((1. - done[i]) *
                                                DQCtrlCfg.DISCOUNT_FACTOR *
                                                np.amax(target_val[i]))

        # Finally train the prediction network
        hist = self._predict.fit(update_input, target,
                                 batch_size=min(DQCtrlCfg.BATCH_SIZE, 64),
                                 epochs=1, verbose=0)

        # Keep track of the loss, reward and total reward in the logs
        self._log.debug("Loss: {}".format(hist.history['loss']))
