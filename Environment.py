#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'davinellulinvega'
from uuid import uuid4
from random import choice, choices, sample
from collections import namedtuple
try:
    import ujson as json
except (ImportError, ModuleNotFoundError):
    import json
from Vec2D import Vec2D
from Hero import Hero
from Enemy import Enemy
from Item import Item
from settings import EnvCfg, HeroCfg
from utils import ItemType, EnemyType

# Declare to classes, as namedtuple, that are only used by the environment
Area = namedtuple("Area", ['position_min', 'position_max'])


def to_probas(counts):
    """
    A simple method that transforms a list of counters into a list of
    probabilities.
    :param counts: List. A list of counters for different types of objects,
    whose probability of appearing we wish to know.
    :return: List. A list of probability for each object to be chosen.
    """

    # Get the list's sum
    tot_counts = sum(counts)
    return [c / tot_counts for c in counts]


class Environment(object):
    """
    The Environment is a container for all the elements of the game: enemies,
    items, heroes and obstacles. It manages the disappearance and birth of
    anything within the map.
    """

    def __init__(self, file_name):
        """
        Initialize the environment. The basic background and static elements
        are loaded from file. The rest (enemies, heroes and items) are added
        depending on the probabilities and areas defined in the file.
        :param file_name: A string representing the absolute path to the
        file, from which to load the environment.
        """

        # Initialize the parent class
        super(Environment, self).__init__()

        # Initialize an ID attribute for the environment
        self._id = uuid4().int

        # Open and load from the provided configuration file
        with open(file_name, 'r') as conf_file:
            config = json.load(conf_file)

        # Declare some basic parameters of the environment
        self._width = config.get('width', 0)
        self._height = config.get('height', 0)

        # Initialize the set of empty and obstacle tiles
        self._empty_tiles = set([Vec2D(x, y) for x in range(self._width) for y
                                 in range(self._height)])
        self._obstacles = set([Vec2D(x, y) for x, y in config.get(
            'obstacles')])  # obstacles = [(x, y), ..., (x, y)]

        # Remove the obstacle tiles from the list of empty tiles
        self._empty_tiles.difference_update(self._obstacles)

        # Add the walls in the list of obstacles, since they are considered
        # as such
        self._obstacles.update([Vec2D(x, y) for y in range(-1, self._height + 1)
                                for x in [-1, self._width]] +
                               [Vec2D(x, y) for x in range(-1, self._width + 1)
                                for y in [-1, self._height]])

        # Define the different areas
        # areas = [{id: int, position_min: (x, y), position_max: (x, y)}, ...]
        self._areas = {}
        for area in config.get('areas'):
            self._areas[area['id']] = Area(Vec2D(*area.get('position_min')),
                                            Vec2D(*area.get('position_max')))
        # Get the quotas for each areas
        self._quotas = config.get('quotas')

        # A dictionary containing the number of occurrences of objects of
        # each types, as well as the maximum allowed. This is used to choose
        # what item or enemies to add next
        max_counts = {}
        for key, val in config.get('max_enemy_counts').items():
            max_counts[EnemyType(key)] = val
        for key, val in config.get('max_item_counts').items():
            max_counts[ItemType(key)] = val

        self._objects_counts = {'cur': {ItemType.PURSE: 0,
                                         ItemType.CHEST: 0,
                                         ItemType.POTION: 0,
                                         EnemyType.SKELETON: 0,
                                         EnemyType.TROLL: 0,
                                         EnemyType.DRAGON: 0},
                                 'max': max_counts}

        # Initialize some lists and dictionaries of objects to manage
        self._enemies = {}
        self._heroes = {}
        self._items = {}
        # Describe which area heroes, items and enemies belong to. This is used
        # to manage the different quotas
        self._object_to_area = {}
        # Initialize the step meter
        self.turn = 0

    def step(self, actions):
        """
        Move the whole environment forward, one step at a time. Given a
        dictionary of actions to perform for each of the heroes, have them
        collect item, perform their actions and move the enemies as well.
        :param actions: Dictionary. A dictionary, whose keys are the heroes
        IDs and values are the actual actions to perform next, for each of them.
        :return: Nothing.
        """

        # For each hero, the order being chosen randomly
        for hero_id, hero in sample(self._heroes.items(),
                                    k=len(self._heroes)):

            # Make sure the hero is still alive
            if hero.is_dead:
                continue

            # Try to move
            if self._move(hero, actions[hero_id]):
                # Get any item which is on the same tile as the hero
                self._collect_items(hero)
            else:
                # Try to fight neighboring heroes
                if not self._fight_hero(hero):
                    # Try to fight neighboring enemies
                    self._fight_enemy(hero)

        # Have each enemy do its thing
        enemy_to_delete = []
        for enemy_id, enemy in self._enemies.items():
            # If the enemy died before its time
            if enemy.is_dead:
                # Add its ID to the list of enemies to be deleted
                enemy_to_delete.append(enemy_id)

            # Look for a possible fight
            elif not self._fight_hero(enemy) and not enemy.is_static:
                # Otherwise, if you're not static, just move around
                self._move(enemy, enemy.step(self))

        # Remove it from the environment
        self._delete_enemy(enemy_to_delete)

        # Spawn new enemies
        # Get the enemy's type
        e_types = list(EnemyType)
        diffs = [self._objects_counts['max'][e] -
                 self._objects_counts['cur'][e] for e in e_types]
        # This makes sure that we haven't reached full capacity yet
        if any([d > 0 for d in diffs]):
            # Add enemy
            self._add_enemy(choices(e_types, weights=to_probas(diffs), k=1)[0])

        # Spawn new items
        # Get the item's type
        i_types = list(ItemType)
        diffs = [self._objects_counts['max'][i] -
                 self._objects_counts['cur'][i] for i in i_types]

        # A little debug message for the developper
        EnvCfg.LOG.debug("Available item slot(s): {}".format(diffs))

        # This makes sure we haven't reached full capacity
        if any([d > 0 for d in diffs]):
            # Add item
            self._add_item(choices(i_types, weights=to_probas(diffs), k=1)[0])

    def _collect_items(self, hero):
        """
        Collect any items that are in the hero's neighborhood.
        :param hero: Hero. An instance of the Hero class.
        :return: Nothing.
        """

        # For each item currently in the environment
        items_to_delete = []
        for item in self._items.values():
            # Check if the item's position is the same as the hero's
            if item.position == hero.position:
                # Collect the item
                if item.type == ItemType.PURSE or item.type == ItemType.CHEST:
                    hero.gold += item.value
                    # Increase the number of items of this type gathered
                    hero.nb_items_gathered[item.type] += 1
                    # Add it to the list of items to delete and exit the loop
                    items_to_delete.append(item.id)
                    break
                elif item.type == ItemType.POTION:
                    if hero.health < HeroCfg.MAX_HEALTH:
                        hero.health += item.value
                        # Increase the number of items of this type gathered
                        hero.nb_items_gathered[item.type] += 1
                        # Add it to the list of items to delete and exit the loop
                        items_to_delete.append(item.id)
                        break

        # Let the user know which items are collected
        EnvCfg.LOG.info('Collecting item with IDs: {}'.format(items_to_delete))

        # Remove the item from the environment
        self._delete_item(items_to_delete)

    def _move(self, obj, action):
        """
        Try to move the given hero according to its wishes (action).
        :param obj: Hero or Enemy. An instance of either the Hero class or
        the Enemy class.
        :param action: Tuple. A tuple representing the action the hero wishes
        to perform.
        :return: Bool. Return true if the Hero was able to move,
        False otherwise. It is important to note that if the hero chooses to
        remain in the same position, False is returned as well, since idling
        is not considered as an action.
        """

        # Check if the chosen action is idle early on
        if action == (0, 0):
            return False

        # Invalid moves are still considered moves
        if action not in EnvCfg.ACTION_SPACE:
            if isinstance(obj, Hero):
                obj.action_is_valid = False
            return True

        # Try to move the hero
        new_hero_pos = obj.position + action
        if new_hero_pos in self._empty_tiles:
            # The move is valid, so consider the action as done
            obj.position = new_hero_pos
            if isinstance(obj, Hero):
                obj.action_is_valid = True
        elif isinstance(obj, Hero):
            # The move was not valid, let the hero know
            obj.action_is_valid = False

        # Only Idle is not considered to be an action, invalid moves are
        # still actions, just not wise ones
        return True

    def _fight_hero(self, hero):
        """
        Try to have the hero fight with another hero in its neighbor hood.
        :param hero: Hero. An instance of the Hero class.
        :return: Bool. A boolean value set to True if the given hero was able
        to fight, False otherwise.
        """

        # For each hero aside from the current one
        for other_hero in self._heroes.values():
            if hero == other_hero:
                continue

            # Check if both heroes are standing on the same tile
            if other_hero.position == hero.position:
                # Attack the other hero
                Environment._attack(hero, other_hero)
                # Only hero are monitored
                if isinstance(hero, Hero):
                    # Increase the number of heroes attacked by the current hero
                    hero.nb_fights['hero'] += 1
                # Some bludgeoning happened let the world know about it
                return True

        # Move along nothing to see here
        return False

    def _fight_enemy(self, hero):
        """
        Try to have the hero fight with an enemy in its neighbor hood.
        :param hero: Hero. An instance of the Hero class.
        :return: Bool. A boolean value set to True if the given hero was able
        to fight, False otherwise.
        """

        # For each enemy
        for enemy in self._enemies.values():
            # Check if it is in the hero's neighborhood
            if enemy.position - hero.position in EnvCfg.ACTION_SPACE:
                # Attack the enemy
                Environment._attack(hero, enemy)
                # Increase the number of fight the current hero had with this
                # specific type of enemy
                hero.nb_fights[enemy.type] += 1
                # Yes finally we fought
                return True

        # No battle for this poor soul
        return False

    @classmethod
    def _attack(cls, obj1, obj2):
        """
        Have obj1 (an enemy or a hero) attack obj2 (same deal here).
        :param obj1: Enemy or Hero. An instance of either the Enemy or Hero
        class.
        :param obj2: Enemy or Hero. An instance of either the Enemy or Hero
        class.
        :return: Nothing.
        """

        # Make sure only heroes and enemies are attacking each other
        assert (isinstance(obj1, Hero) or isinstance(obj1, Enemy)) and \
               (isinstance(obj2, Hero) or isinstance(obj2, Enemy)), \
            "Only heroes and enemies can attack each other, but received: " \
            "{} and {}".format(type(obj1), type(obj2))

        # Have obj1 attack obj2
        obj2.health -= obj1.strength

        # If the first object is a hero and managed to kill the second object,
        # then give the first object more strength and gold
        if isinstance(obj1, Hero) and isinstance(obj2, Enemy) and obj2.is_dead:
            if obj2.type == EnemyType.SKELETON:
                obj1.gold += EnvCfg.SKELETON_REWARD[0]
                obj1.strength += EnvCfg.SKELETON_REWARD[1]
            elif obj2.type == EnemyType.TROLL:
                obj1.gold += EnvCfg.TROLL_REWARD[0]
                obj1.strength += EnvCfg.TROLL_REWARD[1]
            elif obj2.type == EnemyType.DRAGON:
                obj1.gold += EnvCfg.DRAGON_REWARD[0]
                obj1.strength += EnvCfg.DRAGON_REWARD[1]

    def reset(self):
        """
        Usually called reset (hence its name here), this method's goal is to
        initialize the environment. The heroes should already be there,
        since they are added to the environment by the Game object.
        Therefore, this method adds items and enemies around the map and
        makes sure that the environment is ready for agents to roam around
        and learn.
        :return: Nothing.
        """

        # Remove all remaining enemies and items from the environment if any
        for enemy_id in self._enemies:
            self._delete_enemy(enemy_id)
        for item_id in self._items:
            self._delete_item(item_id)

        # Add new enemies to the environment
        # The skeleton units
        while self._objects_counts['cur'][EnemyType.SKELETON] < self._objects_counts['max'][EnemyType.SKELETON]:
            self._add_enemy(EnemyType.SKELETON)

        # The trolls
        while self._objects_counts['cur'][EnemyType.TROLL] < self._objects_counts['max'][EnemyType.TROLL]:
            self._add_enemy(EnemyType.TROLL)

        # And the dragons
        while self._objects_counts['cur'][EnemyType.DRAGON] < self._objects_counts['max'][EnemyType.DRAGON]:
            self._add_enemy(EnemyType.DRAGON)

        # Add new items to the environment
        # The purses
        while self._objects_counts['cur'][ItemType.PURSE] < self._objects_counts['max'][ItemType.PURSE]:
            self._add_item(ItemType.PURSE)

        # The chests
        while self._objects_counts['cur'][ItemType.CHEST] < self._objects_counts['max'][ItemType.CHEST]:
            self._add_item(ItemType.CHEST)

        # And the potions
        while self._objects_counts['cur'][ItemType.POTION] < self._objects_counts['max'][ItemType.POTION]:
            self._add_item(ItemType.POTION)

        # Reinitialize the step meter
        self.turn = 0

    def add_hero(self):
        """
        Add a hero into the environment respecting the given probabilities.
        To manipulate the spawn point of heroes, you could simply have areas
        of equal probabilities (set probas to 1).
        :return: Int. An integer representing the hero's unitque ID.
        """

        # Get the hero's unique ID
        hero_id = uuid4().int

        # Get the list of valid areas
        area_ids = []
        quotas = []
        for a_id, q in self._quotas.items():
            if q['hero'] >= EnvCfg.HERO_FULFILL_RATE:
                area_ids.append(a_id)
                quotas.append(q['hero'])

        # Choose the area in which the hero will spawn
        area_id = choices(area_ids, weights=to_probas(quotas), k=1)[0]

        # Get the hero's spawn position
        spawn_pos = self._get_rnd_pos_in_area(self._areas[area_id])

        # Get a new hero instance
        hero = Hero(hero_id, spawn_pos)

        # Add the hero to the dictionary
        self._heroes[hero_id] = hero

        # Update the probabilities of a hero spawning in the same area
        new_quota = self._quotas[area_id]['hero'] - EnvCfg.HERO_FULFILL_RATE
        self._quotas[area_id]['hero'] = max(0, new_quota)  # Make sure the
        # quota is positive or null

        # Map the hero's id to the area it begins in
        self._object_to_area[hero_id] = area_id

        # Let the user know that a new hero has been added to the map
        EnvCfg.LOG.info("Added a new hero to the map: ID: {}, "
                        "position: {}".format(hero_id, spawn_pos))

        # Return the hero's ID
        return hero_id

    def delete_hero(self, hero_id):
        """
        Remove a dead hero from the map, freeing its position current position.
        :param hero_id: int. An integer representing the hero's ID.
        :return: Nothing.
        """

        # Get the actual hero
        hero = self._heroes[hero_id]

        # Remove the hero from the dictionary of heroes
        self._heroes.pop(hero_id)

        # Update the quota for the corresponding area
        area_id = self._object_to_area[hero_id]
        self._quotas[area_id]['hero'] += EnvCfg.HERO_FULFILL_RATE

        # Remove the hero from the object to area mapping
        self._object_to_area.pop(hero_id)

        # Let the user know that a hero has been deleted
        EnvCfg.LOG.info("Deleted hero: {}".format(hero_id))

    def to_dict(self):
        """
        Describe the whole state of the environment within a dictionary.
        :return: Dict. A dictionary containing the most basic description of
        the environment's state and of all the objects within it.
        """

        # Return the characteristics of the environment
        return {'width': self._width,
                'height': self._height,
                'turn': self.turn,
                'empty': [pos.tolist() for pos in self._empty_tiles],
                'obstacles': [pos.tolist() for pos in self._obstacles],
                'items': {i_id: i.to_dict()
                          for i_id, i in self._items.items()},
                'enemies': {e_id: e.to_dict()
                            for e_id, e in self._enemies.items()},
                'heroes': {h_id: h.to_dict()
                           for h_id, h in self._heroes.items()}
                }

    def _add_enemy(self, enemy_type):
        """
        Add an enemy, whose type is chosen pseudo-randomly based on the
        number of enemies already present in the environment.
        :param enemy_type: EnemyType. The label corresponding to the type of
        enemy to add to the environment.
        :return: Nothing.
        """

        # Get a new enemy ID
        enemy_id = uuid4().int

        # Get the fulfillment rate and initial parameters based on the chosen
        # type
        if enemy_type == EnemyType.SKELETON:
            fulfill_rate = EnvCfg.SKELETON_FULFILL_RATE
        elif enemy_type == EnemyType.TROLL:
            fulfill_rate = EnvCfg.TROLL_FULFILL_RATE
        else:  # Dragon
            fulfill_rate = EnvCfg.DRAGON_FULFILL_RATE

        # Get the list of valid areas
        area_ids = []
        quotas = []
        for a_id, q in self._quotas.items():
            if q['enemy'] >= fulfill_rate:
                area_ids.append(a_id)
                quotas.append(q['enemy'])

        # Make sure at least one area is available
        if not area_ids or not quotas:
            return

        # Choose the area in which the hero will spawn
        area_id = choices(area_ids, weights=to_probas(quotas), k=1)[0]

        # Get the enemy's initial position
        spawn_pos = self._get_rnd_pos_in_area(self._areas[area_id])

        # Instantiate a new enemy depending on its type
        enemy = Enemy(enemy_id, enemy_type, spawn_pos)

        # Add the enemy to the dictionary of enemies
        self._enemies[enemy_id] = enemy

        # Update the enemy quota for the given area
        new_quota = self._quotas[area_id]['enemy'] - fulfill_rate
        self._quotas[area_id]['enemy'] = max(0, new_quota)

        # Map the enemy ID to the area ID
        self._object_to_area[enemy_id] = area_id

        # Increase the meter for the enemy type
        self._objects_counts['cur'][enemy_type] += 1

        # Let the user know that an enemy has been added
        EnvCfg.LOG.info("Added a new enemy to the map: "
                        "ID: {}, type: {}, position: {}".format(enemy_id,
                                                                enemy_type,
                                                                spawn_pos))

    def _add_item(self, item_type):
        """
        Add an item, whose type is chosen pseudo-randomly based on the number
        of items already present in the environment.
        :param item_type: ItemType. The label corresponding to the type of
        item to add to the environment.
        :return: Nothing.
        """

        # Make sure we have not reached full capacity for this item type yet
        if self._objects_counts['cur'][item_type] >= self._objects_counts['max'][item_type]:
            return

        # Let the user know that we are adding an item of a given type
        EnvCfg.LOG.debug("Adding item of type {}".format(item_type))
        EnvCfg.LOG.debug("Quota(s): {}".format(self._quotas))
        EnvCfg.LOG.debug("Object count(s): {}".format(self._objects_counts))

        # Get a new id for the item
        item_id = uuid4().int

        # Get the fulfillment rate
        if item_type == ItemType.PURSE:
            fulfill_rate = EnvCfg.PURSE_FULFILL_RATE
        elif item_type == ItemType.CHEST:
            fulfill_rate = EnvCfg.CHEST_FULFILL_RATE
        else:
            fulfill_rate = EnvCfg.POTION_FULFILL_RATE

        # Get the list of valid areas
        area_ids = []
        quotas = []
        for a_id, q in self._quotas.items():
            if q['item'] >= fulfill_rate:
                area_ids.append(a_id)
                quotas.append(q['item'])

        # If all areas are already full
        if not area_ids or not quotas:
            # Simply exit, since you cannot add any item
            return

        # Choose the area in which the hero will spawn
        area_id = choices(area_ids, weights=to_probas(quotas), k=1)[0]

        # Get the item's position
        spawn_pos = self._get_rnd_pos_in_area(self._areas[area_id])

        # Instantiate a new item depending on its type
        item = Item(item_id, item_type, spawn_pos)

        # Add the item to the dictionary of items
        self._items[item_id] = item

        # Update the item quota for the given area
        new_quota = self._quotas[area_id]['item'] - fulfill_rate
        self._quotas[area_id]['item'] = max(0, new_quota)

        # Map the item to the area ID
        self._object_to_area[item_id] = area_id

        # Increase the meter for the item type
        self._objects_counts['cur'][item_type] += 1

        # Let the user know that a new item has been added to the map
        EnvCfg.LOG.info("Added a new item to the map: ID: {}, type: {}, "
                        "position: {}".format(item_id, item_type, spawn_pos))

    def _delete_enemy(self, enemy_ids):
        """
        Delete the enemy object corresponding to the given ID.
        :param enemy_ids: List. A list of integers used for identifying specific
        enemies.
        :return: Nothing.
        """

        # If anything else than a list has been provided as parameter
        if not (hasattr(enemy_ids, '__iter__') or hasattr(enemy_ids, '__getitem__')):
            # Wrap it as a list
            enemy_ids = [enemy_ids]

        # For each enemy ID provided
        for enemy_id in enemy_ids:
            # Get the enemy object
            enemy = self._enemies[enemy_id]

            # Make sure the enemy is dead before removing it
            assert enemy.health <= 0, "An error occurred while trying to " \
                                      "remove the enemy with ID {}: It is " \
                                      "not dead.".format(enemy_id)

            # Get the enemy fulfillment rate based on its type
            if enemy.type == 'skeleton':
                fulfill_rate = EnvCfg.SKELETON_FULFILL_RATE
            elif enemy.type == 'troll':
                fulfill_rate = EnvCfg.TROLL_FULFILL_RATE
            else:
                fulfill_rate = EnvCfg.DRAGON_FULFILL_RATE

            # Get the area the enemy belonged to
            area_id = self._object_to_area[enemy_id]

            # Update the enemy quota for the area
            self._quotas[area_id]['enemy'] += fulfill_rate

            # Update the meter for the specific type of enemy
            self._objects_counts['cur'][enemy.type] -= 1

            # Remove the enemy from its area
            self._object_to_area.pop(enemy_id)

            # Remove the enemy from the dictionary of enemies
            self._enemies.pop(enemy_id)

            # Let the user know that an enemy has been deleted
            EnvCfg.LOG.info('Deleted enemy: {}'.format(enemy_id))

    def _delete_item(self, item_ids):
        """
        Delete the Item object corresponding to the given ID.
        :param item_ids: List. A list of integers used for identifying specific
        Items.
        :return: Nothing.
        """

        # If something other than a list of item IDS has been provided,
        # wrap it into a list
        if not (hasattr(item_ids, '__iter__') or hasattr(item_ids, '__getitem__')):
            item_ids = [item_ids]

        # Delete them all
        for item_id in item_ids:
            # Get the item object
            item = self._items[item_id]

            # Get the item fulfillment rate based on its type
            if item.type == ItemType.PURSE:
                fulfill_rate = EnvCfg.PURSE_FULFILL_RATE
            elif item.type == ItemType.CHEST:
                fulfill_rate = EnvCfg.CHEST_FULFILL_RATE
            else:
                fulfill_rate = EnvCfg.POTION_FULFILL_RATE

            # Get the area the item belonged to
            area_id = self._object_to_area[item_id]

            # Update the item quota for the area
            self._quotas[area_id]['item'] += fulfill_rate

            # Update the meter for the specific type of item
            self._objects_counts['cur'][item.type] -= 1

            # Remove the item from its area
            self._object_to_area.pop(item_id)

            # Remove the item from the dictionary of items
            self._items.pop(item_id)

            # Let the user know that an item has been deleted
            EnvCfg.LOG.info("Deleted item: {}".format(item_id))

    def _get_rnd_pos_in_area(self, area):
        """
        Choose and return a random position inside the given area. This method
        also makes sure that the position is available (i.e.: not occupied by
        another object).
        :param area: A namedtuple representing the area as a square on the map.
        :return: Vec2D. The position chosen at random as a vector.
        """

        # From the given area, build the list of possible positions
        poss_pos = set([Vec2D(x, y)
                        for x in range(area.position_min.x,
                                       area.position_max.x + 1)
                        for y in range(area.position_min.y,
                                       area.position_max.y + 1)])

        # Extract the set of actually available positions
        avail_pos = poss_pos.intersection(self._empty_tiles)

        # This makes sure that nothing spawns where a hero is already
        # standing
        avail_pos.difference_update([hero.position
                                     for hero in self._heroes.values()])

        # This makes sure that nothing appears on an enemy's spawn position
        avail_pos.difference_update([enemy.init_position
                                     for enemy in self._enemies.values()])

        # This makes sure that nothing appears on a tile where there is
        # already an item
        avail_pos.difference_update([item.position
                                     for item in self._items.values()])

        # Return a random position chose from within the available position
        return choice(list(avail_pos))

    @property
    def id(self):
        return self._id

    @property
    def width(self):
        return self._width

    @property
    def height(self):
        return self._height

    @property
    def action_space(self):
        return EnvCfg.ACTION_SPACE

    @property
    def empty_positions(self):
        return list(self._empty_tiles)

    @property
    def obstacles(self):
        return list(self._obstacles)

    @property
    def items(self):
        return self._items

    @property
    def enemies(self):
        return self._enemies

    @property
    def heroes(self):
        return self._heroes

    @property
    def max_heroes(self):
        return sum([area_d['hero'] for area_d in self._quotas.values()])

    @property
    def max_enemy(self):
        return sum([self._objects_counts['max'][e_t] for e_t in EnemyType])

    @property
    def max_gold_item(self):
        return self._objects_counts['max'][ItemType.PURSE] + \
               self._objects_counts['max'][ItemType.CHEST]

    @property
    def max_health_item(self):
        return self._objects_counts['max'][ItemType.POTION]


if __name__ == "__main__":
    # Fetch the name of a configuration file from the settings module
    from settings import SimCfg

    # Instantiate an new environment
    env = Environment(SimCfg.AVAILABLE_ENV_CONFIGS[0])

    # Print the maximum allowed counter for each element
    print('Heroes: {}\nEnemy: {}\nGold: {}\nHealth: {}'.format(env.max_heroes, env.max_enemy, env.max_gold_item, env.max_health_item))
