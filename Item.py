#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'davinellulinvega'
from Vec2D import Vec2D
from utils import ItemType
from settings import ItemCfg


class Item(object):
    """
    A container class representing an Item in the environment.
    """

    def __init__(self, i_id, i_type, position):
        """
        Initialize the different attributes of an Item.
        :param i_id: Int. An integer representing the Item's unique ID.
        :param i_type: ItemType. An enumeration specifying the type of Item
        to instantiate.
        :param position: Vec2D. The item's position along the X and Y axis.
        """

        # Initialize the attributes
        self._id = i_id
        assert isinstance(i_type, str) or isinstance(i_type, ItemType), \
            "The item type should be either a string or an instance of the " \
            "ItemType enumeration."
        if isinstance(i_type, str):
            i_type = ItemType(i_type)
        self._type = i_type

        if isinstance(position, tuple):
            position = Vec2D(*position)
        self._position = position

        # Deduce the value of the item based on its type
        if self._type == ItemType.PURSE:
            self._value = ItemCfg.PURSE_VALUE
        elif self._type == ItemType.CHEST:
            self._value = ItemCfg.CHEST_VALUE
        elif self._type == ItemType.POTION:
            self._value = ItemCfg.POTION_VALUE

    def to_dict(self):
        """
        Represent the current Item within a dictionary.
        :return: Dict. A dictionary, whose content describes the current item.
        """

        return {'id': self._id, 'type': self._type.value,
                'value': self._value, 'position': self._position.tolist()}

    @property
    def id(self):
        return self._id

    @property
    def type(self):
        return self._type

    @property
    def value(self):
        return self._value

    @property
    def position(self):
        return self._position
