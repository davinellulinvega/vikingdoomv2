#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'davinellulinvega'
from enum import Enum


class ItemType(Enum):
    PURSE = 'purse'
    CHEST = 'chest'
    POTION = 'potion'


class EnemyType(Enum):
    SKELETON = 'skeleton'
    TROLL = 'troll'
    DRAGON = 'dragon'


class CtrlType(Enum):
    DEEP_Q = 'deep_q'
    ACTOR_CRITIC = 'actor_critic'
    PRIM_EMO = 'prim_emo'
    PRIM_EMO_LES = 'prim_emo_les'
    RANDOM = 'random'
