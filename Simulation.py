#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'davinellulinvega'
from argparse import ArgumentParser
from random import choice
from itertools import cycle
from Game import Game
from settings import SimCfg
from utils import CtrlType


# /!\ The display server is started independently from the simulation. This is
# to give people the choice of having rendering or running headless.
if __name__ == "__main__":
    # Declare some command line options
    parser = ArgumentParser()
    parser.add_argument('--nb_games',
                        type=int, default=None,
                        dest='nb_games',
                        help='The number of game that should be played. By '
                             'default the simulation will go on forever.')
    parser.add_argument('--random_env',
                        action='store_true',
                        dest='rnd_env',
                        help='Indicate whether the environment configuration '
                             'should be chosen at random. Otherwise, '
                             'the configuration for the environment is taken '
                             'from the settings module.')

    parser.add_argument('--render',
                        action='store_true',
                        dest='render',
                        help="Indicate whether the different games should send "
                             "information about the current environment's "
                             "state to the display server and it different "
                             "client.")

    parser.add_argument('--host',
                        type=str,
                        default='localhost',
                        dest='ws_host',
                        help='The host name of the display server the '
                             'different games should try to connect to.')

    parser.add_argument('--port',
                        type=int,
                        default=8080,
                        dest='ws_port',
                        help='The port on which the display server is '
                             'listening.')

    parser.add_argument('--ctrl', dest='controllers', action='append',
                        type=lambda s: CtrlType(s),
                        required=True, help='This parameter can be used '
                                            'multiple time to indicate the '
                                            'type of controller to use for '
                                            'each of the players.',
                        choices=CtrlType)

    parser.add_argument('--test', dest='training', action='store_false',
                        help='Indicates that the controller(s) should not be '
                             'learning anymore, but just set in their testing '
                             'phase.')

    parser.add_argument('--monitor', dest='monitor', action='store_true',
                        help='Indicate whether each controller should monitor, its performance or not.')

    # Parse the different command line arguments
    args = parser.parse_args()

    # Build an cycling iterator over the list of provided environments,
    # if necessary
    if not args.rnd_env:
        env_itr = cycle(SimCfg.ENV_CONFIGS)

    # For however many games have been required
    nb_games = args.nb_games
    while nb_games is None or nb_games > 0:
        # Decrease the number of games left to play
        if nb_games is not None:
            nb_games -= 1

            # Let the user know how many games remain to be played
            SimCfg.LOG.info("Still {} game(s) to go.".format(nb_games))

        # Choose the environment configuration file
        if args.rnd_env:
            # Let the user know that we are choosing a random environment
            SimCfg.LOG.info("Choosing a random environment from the list of "
                            "available ones ...")

            # Do so randomly among the list of all available environment
            # configurations
            env_conf_file = choice(SimCfg.AVAILABLE_ENV_CONFIGS)
        else:
            # Let the user know that we are getting the next environment in
            # the provided list
            SimCfg.LOG.info("Getting the next environment configuration file "
                            "...")

            # Get the next environment in the sequence
            env_conf_file = next(env_itr)

        # Let the user know which environment has been chosen
        SimCfg.LOG.info("Done: {}".format(env_conf_file))

        # Let the user know we are instantiating a new game
        SimCfg.LOG.info("Creating a new game ...")

        # Get a instance of a game
        game = Game(env_conf_file,
                    controllers=args.controllers, training=args.training,
                    monitor=args.monitor, ws_host=args.ws_host,
                    ws_port=args.ws_port, render=args.render)

        # Well this is done, let the user know
        SimCfg.LOG.info("Done. ID: {}".format(game.id))

        # Let the user know that the game is going to begin shortly
        SimCfg.LOG.info("Playing the game ...")

        # And play the game until the end
        game.play()

        # Shut the game down, no matter what happens
        game.shutdown()

        # Well this is done, let the user know
        SimCfg.LOG.info("Done.")
