#!/usr/bin/env python3
from os import path
import math
from h5py import File
import numpy as np
import pandas as pd
import statsmodels.api as sm
from settings import DQCtrlCfg, ACCtrlCfg, PECtrlCfg, RndCtrlCfg, \
    BASE_DIR, FIG_DIR

# Declare the file path to the global data store
GLOBAL_STORE_FILE_PATH = path.join(BASE_DIR, 'Data', 'frames_global.zlib')

# Set pandas' precision to 3 decimal places
pd.set_option('precision', 3)


def compute_stats(data, col_name, title, caption, label, stats=['mean', 'count', 'std'], ci=True):
    """
    """

    # Compute the required stats using panda
    stats = data.loc[:, [col_name, 'ctrl']].groupby('ctrl')[col_name].agg(stats)

    # If required, compute the 95% confidence interval
    if ci:
        stats['ci95_hi'] = stats['mean'] + 1.96 * stats['std'] / np.sqrt(stats['count'])
        stats['ci95_lo'] = stats['mean'] - 1.96 * stats['std'] / np.sqrt(stats['count'])

    # Export the DataFrame as a latex table
    file_path = path.join(BASE_DIR, 'Latex', '{}.tex'.format(title.lower().replace(' ', '_')))
    stats.to_latex(buf=file_path, caption=caption, label=label)

if __name__ == "__main__":
    if not path.isfile(GLOBAL_STORE_FILE_PATH):
        # Let the user know we have to parse the different files
        print("Extracting and processing the results from the controllers' "
              "storage files ...")

        # Declare both a list and a dictionary to contain all the data sets
        d_dsets = {}
        l_dsets = []
        l_tot_rew_dsets = []

        # For each controller
        for f_path, ctrl in zip([DQCtrlCfg.STORE_FILE_PATH,
                                 PECtrlCfg.STORE_FILE_PATH,
                                 ACCtrlCfg.STORE_FILE_PATH,
                                 RndCtrlCfg.STORE_FILE_PATH],
                                ['DQN', 'PrimEmo', 'A2C', 'Random']):

            # Declare a set of lists for storing the different data
            tmp_dsets = []
            tmp_tot_rew = []
            # Open its corresponding data file
            with File(f_path, 'r') as s:
                # Read the first 400 datasets into numpy arrays
                for d in s.values():
                    if len(tmp_dsets) == 400:
                        # We reached the desired length, so just peace out
                        break

                    # That is the reading bit
                    tmp_arr = np.empty(d.shape, dtype=d.dtype)
                    d.read_direct(tmp_arr)
                    if not tmp_arr[-2, 1] == 1:
                        if tmp_arr[-2, 0] != 999:
                            print('Game never ended.')
                            continue
                        else:
                            # Here we 'manipulate' the data to make it easier to
                            # plot the life span of an individual
                            # It is acceptable, since 'done' has no meaning in
                            # our data, but had one for the player
                            tmp_arr[-2, 1] = 1
                    # The last row should be removed, since it is only a repeat
                    tmp_dsets.append(tmp_arr[:-1])

                    # Compute the total reward for the game and add it to the list
                    tmp_tot_rew.append([ctrl, np.sum(tmp_arr[:, 2])])

            print('Processed {} games for controller {}.'.format(len(tmp_dsets),
                                                                 ctrl))

            # Vertically stack the arrays so as to tidy format them
            dset = np.vstack(tmp_dsets)

            # Transform the array into a pandas.DataFrame
            monitored_features = ['cycle', 'done', 'reward', 'action_idx', 'action_valid',
                                  'enemy0_x', 'enemy0_y',
                                  'enemy0_hp', 'enemy0_stg',
                                  'enemy1_x', 'enemy1_y',
                                  'enemy1_hp', 'enemy1_stg',
                                  'gold0_x', 'gold0_y',
                                  'gold1_x', 'gold1_y',
                                  'potion0_x', 'potion0_y',
                                  'potion1_x', 'potion1_y',
                                  'obs0_x', 'obs0_y',
                                  'obs1_x', 'obs1_y',
                                  'obs2_x', 'obs2_y',
                                  'hero_hp', 'hero_stg',
                                  'nb_fights_skeleton',
                                  'nb_fights_troll',
                                  'nb_fights_dragon',
                                  'nb_fights_hero',
                                  'nb_gathered_purse',
                                  'nb_gathered_chest',
                                  'nb_gathered_potion',
                                  'act_ca_0',
                                  'act_ca_1',
                                  'act_ca_2',
                                  'act_ca_3',
                                  'act_ca_4',
                                  'act_ca_5',
                                  'act_ca_6',
                                  'act_ca_7',
                                  'act_ca_8',
                                  'act_ca_9',
                                  'act_ca_10',
                                  'act_ca_11',
                                  'act_ca_12',
                                  'act_ca_13',
                                  'act_ca_14',
                                  'act_vta']

            dframe = pd.DataFrame(data=dset,
                                  columns=monitored_features[0:dset.shape[1]])

            # Transform the list of total reward into a data frame
            tot_rew_dframe = pd.DataFrame(data=tmp_tot_rew, columns=['ctrl',
                                                                     'total_reward'])

            # Add the data from for the total reward to the list of all data frames
            l_tot_rew_dsets.append(tot_rew_dframe)

            # Add the data frame to the dictionary of all data frames
            d_dsets[ctrl] = dframe

            # Add a controller column to make the observations unique and tidy
            s_ctrl = pd.Series(data=[ctrl] * dframe.shape[0], name='ctrl')
            dframe = pd.concat([dframe, s_ctrl], axis=1, sort=False)

            # Add the tidy frame to the list of data frames
            l_dsets.append(dframe)

        # Concatenate the different DataFrames into a single one for ease of use with
        # seaborn
        global_frame_tidy = pd.concat(l_dsets, axis=0, ignore_index=True, sort=False)
        global_frame_wide = pd.concat(d_dsets, sort=False)
        global_frame_tot_rew = pd.concat(l_tot_rew_dsets, axis=0, ignore_index=True,
                                         sort=False)

        # Save both the tidy and wide formats into a hdf5 formatted file, to avoid
        # parsing every time
        global_frame_tidy.to_hdf(GLOBAL_STORE_FILE_PATH, 'tidy', complevel=9)
        global_frame_wide.to_hdf(GLOBAL_STORE_FILE_PATH, 'wide', complevel=9)
        global_frame_tot_rew.to_hdf(GLOBAL_STORE_FILE_PATH, 'tot_rew', complevel=9)
    else:
        # Let the user know we will be using the data previously extracted
        print('Using the existing global storage file ...')

        # Read the main data frames from the hdf5 formatted storage file
        global_frame_tidy = pd.read_hdf(GLOBAL_STORE_FILE_PATH, key='tidy')
        global_frame_wide = pd.read_hdf(GLOBAL_STORE_FILE_PATH, key='wide')
        global_frame_tot_rew = pd.read_hdf(GLOBAL_STORE_FILE_PATH, key='tot_rew')

    # Let the user know the extraction process is done
    print('Done.')

    print('Computing the statistics and generate the corresponding latex tables ...')
    compute_stats(global_frame_tidy.loc[global_frame_tidy['done'] == 1], 'cycle', 'Average Life Span', caption='In this table are detailed the different statistics corresponding to the average life span of each controller.', label='tab:primEmo_avg_life_span')

    compute_stats(global_frame_tidy, 'nb_fights_dragon', 'Average Number of Fights', caption='This table details the statistics describing the number of fights each controller has been involved in during a game.', label='tab:primEmo_avg_nb_fights')

    compute_stats(global_frame_tot_rew, 'total_reward', 'Total Reward', stats=['mean', 'count', 'std', 'min', 'max'], caption='This table details the statistics describing the total reward each controller received during game.', label='tab:primEmo_total_reward')

    compute_stats(global_frame_tidy, 'nb_gathered_potion', 'Average Number of Potions Gathered', caption='This table details the statistics describing the number of potions each controller gathered during a game.', label='tab:primEmo_avg_nb_potions')

    compute_stats(global_frame_tidy, 'nb_gathered_chest', 'Average Number of Chests Gathered', caption='This table details the statistics describing the number of gold items each controller gathered during a game.', label='tab:primEmo_avg_nb_chests')

    print('Done.')


    # Build a data frame containing the amygdala's activities and the distance to
    # the nearest enemies
    df = global_frame_tidy[global_frame_tidy.ctrl == 'PrimEmo']
    s_enemy0 = pd.Series(data=np.abs(df['enemy0_x'] + df['enemy0_y']),
                         name='enemy0_dist')
    s_enemy1 = pd.Series(data=np.abs(df['enemy1_x'] + df['enemy1_y']),
                         name='enemy1_dist')
    s_gold0 = pd.Series(data=np.abs(df['gold0_x'] + df['gold0_y']),
                        name='gold0_dist')
    s_gold1 = pd.Series(data=np.abs(df['gold1_x'] + df['gold1_y']),
                        name='gold1_dist')
    s_potion0 = pd.Series(data=np.abs(df['potion0_x'] + df['potion0_y']),
                          name='potion0_dist')
    s_potion1 = pd.Series(data=np.abs(df['potion1_x'] + df['potion1_y']),
                          name='potion1_dist')

    # Compute the average activity for the CA layer
    avg_ca_act = pd.Series(pd.DataFrame(df.loc[:, 'act_ca_0':'act_ca_14']).mean(axis=1), name='act_ca_avg')

    # Concatenate the computed data into a single tidy formatted data frame
    df = pd.concat([avg_ca_act, df.loc[:, 'act_vta'], df.loc[:, 'hero_hp'], s_enemy0, s_enemy1, s_gold0, s_gold1, s_potion0, s_potion1], axis=1)

    model = sm.GLS(df['act_ca_avg'], df.loc[:, 'enemy0_dist':'potion1_dist'], hasconst=False)

    results = model.fit()
    print(results.summary())

    # Plot the correlation between the computed features and CA/VTA activities
    # gd = sns.PairGrid(df, x_vars=['enemy0_dist', 'enemy1_dist', 'gold0_dist', 'gold1_dist', 'potion0_dist', 'potion1_dist', 'hero_hp'],
    #                   y_vars=['act_ca_avg', 'act_vta'])

    print('Finished')
