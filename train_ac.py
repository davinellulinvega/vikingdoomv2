#!/usr/bin/env python3
# -*- coding: utf-8 -*-
__author__ = 'davinellulinvega'
from argparse import ArgumentParser
from itertools import cycle
from queue import Empty
from os.path import join, isfile
from pathlib import Path
from statistics import mean
import joblib
import boto3
from botocore.exceptions import ClientError
from Game import Game
from settings import SimCfg, ACCtrlCfg, DATA_DIR, AWS, BUCKET_NAME
from ActorCriticCtrl import ActorCriticCtrl
from utils import CtrlType

# Define the path to the file, where the best score is to be saved
BEST_SCORE_PATH = join(DATA_DIR, 'best_score_ac.lz')

# If the script is being executed on an AWS server, try to download the save
# file from the cloud
if AWS:
    # Get a new client instance for the S3 service
    s3 = boto3.client('s3')

    # Extract the object's name
    obj_name = join(*Path(BEST_SCORE_PATH).parts[-2:])

    # Try to download the file
    try:
        s3.download_file(BUCKET_NAME, obj_name, BEST_SCORE_PATH)
    except ClientError as e:
        SimCfg.LOG.exception('An error occurred, while trying to download the '
                             'file: {}.\nError message was: '
                             '{}'.format(BEST_SCORE_PATH, e))

# Initialize a best score variable
if isfile(BEST_SCORE_PATH):
    BEST_SCORE = joblib.load(BEST_SCORE_PATH)
else:
    # -5000 is the worst possible reward, nb_game_cycle * death_punishment
    BEST_SCORE = -5000


def validate():
    global ENV_CONF_FILE

    # Initialize the list of reward and the number of games to be played
    rewards = []
    nb_games = 10
    while nb_games > 0:
        # Get a Game instance
        game = Game(ENV_CONF_FILE, controllers=[CtrlType.ACTOR_CRITIC],
                    training=False, monitor=False, render=False)
        try:
            # Play a game
            game.play()
        except BaseException as e:
            # If any exception is risen, while playing the game, just pretend
            # nothing happened and go on to the next iteration
            SimCfg.LOG.exception("An error occurred, while playing the "
                                 "game: {}".format(e))
            continue

        # Gracefully shut the game down
        game.shutdown()

        # Gather the results
        rewards.extend(game.total_rewards.values())

        # Decrease the number of games left to play
        nb_games -= 1

    # Return the average reward
    return mean(rewards)


if __name__ == "__main__":
    # Declare some command line options
    parser = ArgumentParser()
    parser.add_argument('--nb_games', type=int, default=None, dest='nb_games',
                        help='The number of games that should be played. By '
                             'default the simulation go on forever.')

    # Parse the only option available
    args = parser.parse_args()

    # Build a cycling iterator over the list of provided environments
    env_itr = cycle(SimCfg.ENV_CONFIGS)

    # For however many games have been required
    nb_games = args.nb_games
    while nb_games is None or nb_games > 0:
        # Decrease the number of games left to play
        if nb_games is not None:
            nb_games -= 1

        SimCfg.LOG.debug('NB GAMES {}'.format(nb_games))
        # Get the next environment in the sequence
        ENV_CONF_FILE = next(env_itr)

        # Get an instance of a game
        game = Game(ENV_CONF_FILE, controllers=[CtrlType.ACTOR_CRITIC],
                    training=True, monitor=False, render=False)

        # Train the model for a single game
        game.play()

        # Shut the game down
        game.shutdown()

        # Every 100 games
        if nb_games is not None and (args.nb_games - nb_games) % 50 == 0:
            SimCfg.LOG.debug('validation')
            # Validate the learning done so far
            avg_rewards = validate()

            # Check whether the current model beats the best one
            if avg_rewards > BEST_SCORE:
                # Update the best score
                BEST_SCORE = avg_rewards
                # And save it to file
                joblib.dump(BEST_SCORE, filename=BEST_SCORE_PATH,
                            compress=('lz4', 4))
                # Upload any important file (both models and the best score
                ActorCriticCtrl.upload(files=[(ACCtrlCfg.ACTOR_MODEL_FILE_PATH,
                                               join('Models', 'best_actor.h5')),
                                              (ACCtrlCfg.CRITIC_MODEL_FILE_PATH,
                                               join('Models', 'best_critic.h5')),
                                              BEST_SCORE_PATH])

    # Validate a final time
    avg_rewards = validate()

    # Check whether the current model beats the best one
    if avg_rewards > BEST_SCORE:
        # Update the best score
        BEST_SCORE = avg_rewards
        # And save it to file
        joblib.dump(BEST_SCORE, filename=BEST_SCORE_PATH, compress=('lz4', 4))
        # Upload any important file (both models and the best score
        ActorCriticCtrl.upload(files=[(ACCtrlCfg.ACTOR_MODEL_FILE_PATH,
                                       join('Models', 'best_actor.h5')),
                                      (ACCtrlCfg.CRITIC_MODEL_FILE_PATH,
                                       join('Models', 'best_critic.h5')),
                                      BEST_SCORE_PATH])

    # Display the final best score
    SimCfg.LOG.info('The best AC score is: {}'.format(BEST_SCORE))
