#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'davinellulinvega'
import os
from multiprocessing import cpu_count
from logging import getLogger
from logging.config import dictConfig
from collections import defaultdict
from utils import ItemType, EnemyType

# Declare some basic folder path for storing the different elements of the
# project
BASE_DIR = os.path.abspath(os.path.dirname(__file__))

DATA_DIR = os.path.join(BASE_DIR, 'Data')
# If the Data directory does not exists already, just create it
if not os.path.isdir(DATA_DIR):
    os.mkdir(DATA_DIR, mode=0o766)

MODEL_DIR = os.path.join(BASE_DIR, 'Models')
# If the Models directory does not exists already, just create it
if not os.path.isdir(MODEL_DIR):
    os.mkdir(MODEL_DIR, mode=0o766)

ENVS_DIR = os.path.join(BASE_DIR, 'Envs')
# Make sure the environment folder exist
if not os.path.isdir(ENVS_DIR):
    os.mkdir(ENVS_DIR, mode=0o766)

LOG_DIR = os.path.join(BASE_DIR, 'Logs')
# If the Logs directory does not exists already, just create it
if not os.path.isdir(LOG_DIR):
    os.mkdir(LOG_DIR, mode=0o766)

FIG_DIR = os.path.join(BASE_DIR, 'Figs')
# If the Figs directory does not exists already, just create it
if not os.path.isdir(FIG_DIR):
    os.mkdir(FIG_DIR, mode=0o766)

# Define the name of the bucket in which the different files for the
# project will be stored
BUCKET_NAME = 'vikingdoom'

# Define a switch telling all processes, if the execution is happening on
# an AWS platform or not
AWS = False

# Configure the logging utility
dictConfig({
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'simple': {
            'format': "%(asctime)s %(filename)s - %(levelname)s in "
                      "%(module)s.%(funcName)s at line %(lineno)d: %(message)s"
        },
        'compact': {
            'format': "%(asctime)s - %(levelname)s: %(message)s"
        }
    },
    'handlers': {
        'console': {
            'level': 'ERROR',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
        'simulation': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': os.path.join(LOG_DIR, "simulation.log"),
            'formatter': 'compact'
        },
        'genetic': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': os.path.join(LOG_DIR, "genetic.log"),
            'formatter': 'compact'
        },
        'game': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': os.path.join(LOG_DIR, "game.log"),
            'formatter': 'compact'
        },
        'environment': {
            'level': 'WARN',
            'class': 'logging.FileHandler',
            'filename': os.path.join(LOG_DIR, "environment.log"),
            'formatter': 'compact'
        },
        'controller': {
            'level': 'WARN',
            'class': 'logging.FileHandler',
            'filename': os.path.join(LOG_DIR, "controller.log"),
            'formatter': 'compact'
        },
        'random': {
            'level': 'WARN',
            'class': 'logging.FileHandler',
            'filename': os.path.join(LOG_DIR, "random.log"),
            'formatter': 'compact'
        },
        'deep_q': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': os.path.join(LOG_DIR, "deep_q.log"),
            'formatter': 'compact'
        },
        'actor_critic': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': os.path.join(LOG_DIR, "actor_critic.log"),
            'formatter': 'compact'
        },
        'prim_emo': {
            'level': 'WARN',
            'class': 'logging.FileHandler',
            'filename': os.path.join(LOG_DIR, "prim_emo.log"),
            'formatter': 'compact'
        },
    },
    'loggers': {
        '': {
            'handlers': ['console'],
            'level': 'WARN'
        },
        'simulation': {
            'handlers': ['simulation', 'console'],
            'level': 'INFO',
            'propagate': False,
        },
        'genetic': {
            'handlers': ['genetic', 'console'],
            'level': 'INFO',
            'propagate': False,
        },
        'game': {
            'handlers': ['game', 'console'],
            'level': 'INFO',
            'propagate': False,
        },
        'environment': {
            'handlers': ['environment', 'console'],
            'level': 'WARN',
            'propagate': False,
        },
        'controller': {
            'handlers': ['controller'],
            'level': 'WARN',
            'propagate': False,
        },
        'random': {
            'handlers': ['random'],
            'level': 'WARN',
            'propagate': False,
        },
        'deep_q': {
            'handlers': ['deep_q'],
            'level': 'INFO',
            'propagate': False,
        },
        'actor_critic': {
            'handlers': ['actor_critic'],
            'level': 'INFO',
            'propagate': False,
        },
        'prim_emo': {
            'handlers': ['prim_emo', 'console'],
            'level': 'WARN',
            'propagate': False,
        },
    },
})


class SimCfg(object):
    LOG = getLogger('simulation')
    AVAILABLE_ENV_CONFIGS = [os.path.join(ENVS_DIR, 'small_test.json')]
    ENV_CONFIGS = [os.path.join(ENVS_DIR, 'small_test.json')]


class GameCfg(object):
    MAX_TURN = 1000
    LOG = getLogger('game')  # General logger used for the game engine
    RENDER_EVT_NAME = 'update'


class EnvCfg(object):
    LOG = getLogger('environment')
    # Declare the fulfillment rates for the different objects
    HERO_FULFILL_RATE = 1
    SKELETON_FULFILL_RATE = 1
    TROLL_FULFILL_RATE = 5
    DRAGON_FULFILL_RATE = 25
    PURSE_FULFILL_RATE = 1
    CHEST_FULFILL_RATE = 5
    POTION_FULFILL_RATE = 1

    # Define the reward, (gold, strength) for defeating an enemy
    SKELETON_REWARD = (1, 1)
    TROLL_REWARD = (10, 3)
    DRAGON_REWARD = (50, 10)

    # Declare the action space
    ACTION_SPACE = [(0, -1),  # Up
                    (0, 1),   # Down
                    (-1, 0),  # Left
                    (1, 0),   # Right
                    (0, 0)]   # Idle


class HeroCfg(object):
    MAX_HEALTH = 100
    MAX_STRENGTH = 25


class EnemyCfg(object):
    MAX_HEALTH = 100
    SKELETON_STRENGTH = 1
    TROLL_STRENGTH = 12.5
    DRAGON_STRENGTH = 25
    SKELETON_HEALTH = 10
    TROLL_HEALTH = 25
    DRAGON_HEALTH = 75
    SKELETON_ATTACK_RADIUS = 1
    DRAGON_ATTACK_RADIUS = 2


class ItemCfg(object):
    PURSE_VALUE = 5
    CHEST_VALUE = 25
    POTION_VALUE = 25


class CtrlCfg(object):
    # Declare a generic logging facility for the controller
    LOG = getLogger('controller')

    # Declare the parameters to limit the size of Tensorflow's session on a per
    # controller basis
    ALLOW_GROWTH = True
    # TODO: In case multiple players take part in the game, this parameter
    #   should take the number of players into account
    GPU_MEMORY_FRACTION = 1/3

    # Declare parameters describing the agent's capabilities
    FOW_RADIUS = 50
    DEATH_PUNISHMENT = -5
    INVALID_MOVE_PUNISHMENT = -0.1
    MAX_NB_OBSTACLES = 3

    # Declare some common parameters for the input and output layers of any
    # neural network based controller
    INPUT_SIZE = 18 + 2 * MAX_NB_OBSTACLES
    OUTPUT_SIZE = len(EnvCfg.ACTION_SPACE)

    # Declare the number of features monitored
    # INPUT_SIZE = size of curr_env_state
    # cycle + done + reward + act_idx + action_is_valid + curr_hero.nb_fights[
    # 'hero'] = 6
    NB_MONITOR_FEATURES = INPUT_SIZE + len(ItemType) + len(EnemyType) + 6

    # The following quantities are monitored:
    # Implemented in the controller:
    #   Activity of the amygdala's output layer (CA) -> only relevant for
    #   PrimEmo based controller
    #   Action chosen + current state
    #   Reward
    #   Done -> to know when the game/task ended
    #   Number of cycle survived
    # Implemented in the Hero:
    #   Amount of gold gathered
    #   Remaining health at the end of a game (if still alive otherwise 0)
    #   Number of items gathered, per item type
    #   Number of fights: against heroes, against enemies
    #   Final Level of strength


class RndCtrlCfg(object):
    # Declare a logging facility for the random controller
    LOG = getLogger('random')
    # Declare the absolute path to the file used for storing the monitoring data
    STORE_FILE_PATH = os.path.join(DATA_DIR, 'random.h5')


class DQCtrlCfg(object):
    # Declare a logging facility for the deep Q-learning controller
    LOG = getLogger('deep_q')

    # Declare the absolute path to the file used for storing the monitoring data
    STORE_FILE_PATH = os.path.join(DATA_DIR, 'deep_q.h5')

    # Declare the absolute path to the file used for loading/saving the
    # different networks
    MODEL_FILE_PATH = os.path.join(MODEL_DIR, 'deep_q.h5')

    # Declare the absolute path to save/load the memory from
    MEMORY_FILE_PATH = os.path.join(DATA_DIR, 'dq_memory.h5')

    # Declare the maximum size for the memory queue
    MEMORY_MAX_SIZE = 10000

    # Declare the update rate for updating the target network
    TARGET_UPDATE_RATE = 100

    # Declare the size of the different layers that make up the prediction
    # network
    LAYER_SIZES = [50, 75, 100, 200, 200, 100, 75, 50, 25, 12]

    # Declare an epsilon parameter for the policy
    EPSILON = 0.9
    EPSILON_DECAY_RATE = 0.9995
    MIN_EPSILON = 0.05

    # Declare some network related parameters
    LEARNING_RATE = 1e-4
    # TODO: Decaying the learning rate, might not be really all that useful
    LR_DECAY_RATE = 0
    TRAIN_CYCLE_START = 99
    BATCH_SIZE = 512
    DISCOUNT_FACTOR = 0.9  # 0.99 makes the agent diverge. 0.95 works, but is slower than 0.9 in general.
    AMSGRAD = True


class ACCtrlCfg(object):
    # Define a logging facility for the Actor Critic controller
    LOG = getLogger('actor_critic')

    # Declare the absolute path to the file used for storing the monitoring data
    STORE_FILE_PATH = os.path.join(DATA_DIR, 'actor_critic.h5')

    # Declare the absolute path to the file used for loading/saving the
    # different networks
    ACTOR_MODEL_FILE_PATH = os.path.join(MODEL_DIR, 'actor.h5')
    CRITIC_MODEL_FILE_PATH = os.path.join(MODEL_DIR, 'critic.h5')

    # Declare the size of the different layers that make up the critic
    CRITIC_LAYER_SIZES = [50, 75,150, 137, 125, 112, 100, 87, 75, 62, 50, 37, 25, 12]
    # Declare the size of the different layers that make up the actor
    ACTOR_LAYER_SIZES = [50, 75, 150, 137, 125, 112, 100, 87, 75, 62, 50, 37, 25, 12]

    # Declare some parameters constraining the learning process
    ACTOR_LEARNING_RATE = 1e-5
    CRITIC_LEARNING_RATE = 5e-5
    # TODO: Decaying the learning rate, might not be really all that useful
    LR_DECAY_RATE = 1e-8
    DISCOUNT_FACTOR = 0.99

    # Define the strength of the entropy regularization term
    ENTROPY_RATIO = 1e-3

    # Define the number of steps during which experience is gathered by the
    # actor alone
    # TODO: Set that between 20 and 50
    N_STEPS = 50


class PECtrlCfg(object):
    # Define a logging facility for the Actor Critic controller
    LOG = getLogger('prim_emo')

    # Declare the absolute path to the file used for storing the monitoring data
    STORE_FILE_PATH = os.path.join(DATA_DIR, 'prim_emo.h5')

    # Declare the absolute path to the file used for loading/saving the network
    MODEL_FILE_PATH = os.path.join(MODEL_DIR, 'prim_emo.h5')

    # Define the constant used for the different bias layers
    BIAS = 0.1

    # Define the rate at which recurrent connections decay over time
    RDR = 0.9

    # Define the maximum number of iterations waiting for the output activity
    # to stabilize as well as a threshold to quantify stability
    MAX_STABLE_ITER = 1  # Setting it to 1 at the moment since stability is
    # never reached
    STABLE_THRES = 1e-3

    # Declare the size of the different layers
    LAYER_SIZES = defaultdict(lambda: 10,  # Default layer size
                              ext_in=22,
                              int_in=2,
                              rew_in=1,
                              lha=8,
                              hypot=10,
                              thal1=40,
                              la1=20,
                              la2=10,
                              ba1=50,
                              ba2=25,
                              itc1=12,
                              itc2=7,
                              ssc=20,
                              ca1=30,
                              ca2=15,
                              ppt=10,
                              vs1=20,
                              vs2=20,
                              vta1=30,
                              vta2=1,
                              ds1=20,
                              ds2=20,
                              gpe=15,
                              stn=15,
                              snr=20,
                              thal2=20,
                              pfc=20,
                              mc=20)


class GACfg(object):
    # Define a logger for the GA process
    LOG = getLogger('genetic')
    # Define the environment to be used for the learning phase
    ENV_CONFIGS = os.path.join(ENVS_DIR, 'test.json')
    # Define the number of processes to be used by the GA process
    NB_PROCS = cpu_count() - 1
    # Declare the size of the population, as well as some probabilities for
    # crossover and mutation
    POPULATION_SIZE = 50
    CX_PROB = 0.8
    MU_PROB = 0.05
    IND_PROB = 0.8
    # The extension is set to gz, so that joblib automatically compresses the
    # object to LZ4 format
    CKPT_FILE_PATH = os.path.join(DATA_DIR, 'genetic_ckpt.lz')

    # Declare the size of the different weight and bias matrices
    WEIGHT_SHAPES = [
        # Thal1
        (PECtrlCfg.LAYER_SIZES['ext_in'] + PECtrlCfg.LAYER_SIZES['ca2'],
         PECtrlCfg.LAYER_SIZES['thal1']),
        # Bias
        (PECtrlCfg.LAYER_SIZES['thal1'],),
        # Lha
        (PECtrlCfg.LAYER_SIZES['int_in'] + PECtrlCfg.LAYER_SIZES['ca2'],
         PECtrlCfg.LAYER_SIZES['lha']),
        # Bias
        (PECtrlCfg.LAYER_SIZES['lha'],),
        # La1
        (PECtrlCfg.LAYER_SIZES['thal1'], PECtrlCfg.LAYER_SIZES['la1']),
        # Bias
        (PECtrlCfg.LAYER_SIZES['la1'],),
        # TODO: La2
        (PECtrlCfg.LAYER_SIZES['la1'], PECtrlCfg.LAYER_SIZES['la2']),
        # Bias
        (PECtrlCfg.LAYER_SIZES['la2'],),
        # Hypot
        (PECtrlCfg.LAYER_SIZES['ba2'] + PECtrlCfg.LAYER_SIZES['lha'],
         PECtrlCfg.LAYER_SIZES['hypot']),
        # Bias
        (PECtrlCfg.LAYER_SIZES['hypot'],),
        # Ba1
        (PECtrlCfg.LAYER_SIZES['la2'] + PECtrlCfg.LAYER_SIZES['hypot'] +
         len(EnvCfg.ACTION_SPACE) * PECtrlCfg.LAYER_SIZES['pfc'] +
         len(EnvCfg.ACTION_SPACE) * PECtrlCfg.LAYER_SIZES['mc'],
         PECtrlCfg.LAYER_SIZES['ba1']),
        # Bias
        (PECtrlCfg.LAYER_SIZES['ba1'],),
        # Ba2
        (PECtrlCfg.LAYER_SIZES['ba1'], PECtrlCfg.LAYER_SIZES['ba2']),
        # Bias
        (PECtrlCfg.LAYER_SIZES['ba2'],),
        # Itc1
        (PECtrlCfg.LAYER_SIZES['ba2'], PECtrlCfg.LAYER_SIZES['itc1']),
        # Bias
        (PECtrlCfg.LAYER_SIZES['itc1'],),
        # TODO: Itc2
        (PECtrlCfg.LAYER_SIZES['itc1'], PECtrlCfg.LAYER_SIZES['itc2']),
        # Bias
        (PECtrlCfg.LAYER_SIZES['itc2'],),
        # Ssc
        (PECtrlCfg.LAYER_SIZES['thal1'], PECtrlCfg.LAYER_SIZES['ssc']),
        # Bias
        (PECtrlCfg.LAYER_SIZES['ssc'],),
        # Ca1
        (PECtrlCfg.LAYER_SIZES['itc2'] + PECtrlCfg.LAYER_SIZES['ba2'] +
         PECtrlCfg.LAYER_SIZES['hypot'] + PECtrlCfg.LAYER_SIZES['ssc'],
         PECtrlCfg.LAYER_SIZES['ca1']),
        # Bias
        (PECtrlCfg.LAYER_SIZES['ca1'],),
        # Ca2
        (PECtrlCfg.LAYER_SIZES['ca1'], PECtrlCfg.LAYER_SIZES['ca2']),
        # Bias
        (PECtrlCfg.LAYER_SIZES['ca2'],),
        # Vs1
        (len(EnvCfg.ACTION_SPACE) * PECtrlCfg.LAYER_SIZES['mc'] +
         PECtrlCfg.LAYER_SIZES['ba2'] +
         PECtrlCfg.LAYER_SIZES['ssc'], PECtrlCfg.LAYER_SIZES['vs1']),
        # Bias
        (PECtrlCfg.LAYER_SIZES['vs1'],),
        # Vs2
        (len(EnvCfg.ACTION_SPACE) * PECtrlCfg.LAYER_SIZES['mc'] +
         PECtrlCfg.LAYER_SIZES['ba2'] +
         PECtrlCfg.LAYER_SIZES['ssc'], PECtrlCfg.LAYER_SIZES['vs2']),
        # Bias
        (PECtrlCfg.LAYER_SIZES['vs2'],),
        # Ppt
        (PECtrlCfg.LAYER_SIZES['lha'] + PECtrlCfg.LAYER_SIZES['ca2'],
         PECtrlCfg.LAYER_SIZES['ppt']),
        # Bias
        (PECtrlCfg.LAYER_SIZES['ppt'],),
        # Vta1
        (PECtrlCfg.LAYER_SIZES['lha'] + PECtrlCfg.LAYER_SIZES['ppt'] +
         PECtrlCfg.LAYER_SIZES['ca2'] + PECtrlCfg.LAYER_SIZES['vs1'] +
         PECtrlCfg.LAYER_SIZES['vs2'] + PECtrlCfg.LAYER_SIZES['rew_in'],
         PECtrlCfg.LAYER_SIZES['vta1']),
        (PECtrlCfg.LAYER_SIZES['vta1'],),
        # Vta2
        (PECtrlCfg.LAYER_SIZES['vta1'], PECtrlCfg.LAYER_SIZES['vta2']),
        (PECtrlCfg.LAYER_SIZES['vta2'],)
    ]

    # Weight shapes, from here on out, are concatenated since all layers are
    # in fact groups of layers, so each config is repeated as many times as
    # there are actions available
    # Ds2 + bias
    WEIGHT_SHAPES += [(PECtrlCfg.LAYER_SIZES['mc'] +
                       PECtrlCfg.LAYER_SIZES['pfc'],
                       PECtrlCfg.LAYER_SIZES['ds2']),
                      (PECtrlCfg.LAYER_SIZES['ds2'],)] * len(EnvCfg.ACTION_SPACE)
    # Gpe + bias
    WEIGHT_SHAPES += [(PECtrlCfg.LAYER_SIZES['ds2'] +
                       PECtrlCfg.LAYER_SIZES['stn'],
                       PECtrlCfg.LAYER_SIZES['gpe']),
                      (PECtrlCfg.LAYER_SIZES['gpe'],)] * len(EnvCfg.ACTION_SPACE)
    # Ds1 + bias
    WEIGHT_SHAPES += [(PECtrlCfg.LAYER_SIZES['mc'] +
                       PECtrlCfg.LAYER_SIZES['pfc'],
                       PECtrlCfg.LAYER_SIZES['ds1']),
                      (PECtrlCfg.LAYER_SIZES['ds1'],)] * len(EnvCfg.ACTION_SPACE)
    # Stn + bias
    WEIGHT_SHAPES += [(PECtrlCfg.LAYER_SIZES['mc'] +
                       PECtrlCfg.LAYER_SIZES['gpe'] +
                       PECtrlCfg.LAYER_SIZES['pfc'],
                       PECtrlCfg.LAYER_SIZES['stn']),
                      (PECtrlCfg.LAYER_SIZES['stn'],)] * len(EnvCfg.ACTION_SPACE)
    # Snr + bias
    WEIGHT_SHAPES += [(PECtrlCfg.LAYER_SIZES['ds1'] +
                       PECtrlCfg.LAYER_SIZES['gpe'] +
                       PECtrlCfg.LAYER_SIZES['stn'],
                       PECtrlCfg.LAYER_SIZES['snr']),
                      (PECtrlCfg.LAYER_SIZES['snr'],)] * len(EnvCfg.ACTION_SPACE)
    # Thal2 + bias
    WEIGHT_SHAPES += [(PECtrlCfg.LAYER_SIZES['snr'] +
                       PECtrlCfg.LAYER_SIZES['pfc'] +
                       PECtrlCfg.LAYER_SIZES['mc'],
                       PECtrlCfg.LAYER_SIZES['thal2']),
                      (PECtrlCfg.LAYER_SIZES['thal2'],)] * len(EnvCfg.ACTION_SPACE)
    # Pfc + bias
    WEIGHT_SHAPES += [(PECtrlCfg.LAYER_SIZES['ca2'] +
                       PECtrlCfg.LAYER_SIZES['ssc'] +
                       PECtrlCfg.LAYER_SIZES['thal2'],
                       PECtrlCfg.LAYER_SIZES['pfc']),
                      (PECtrlCfg.LAYER_SIZES['pfc'],)] * len(EnvCfg.ACTION_SPACE)
    # Mc + bias
    WEIGHT_SHAPES += [(PECtrlCfg.LAYER_SIZES['pfc'] +
                       PECtrlCfg.LAYER_SIZES['thal2'],
                       PECtrlCfg.LAYER_SIZES['mc']),
                      (PECtrlCfg.LAYER_SIZES['mc'],)] * len(EnvCfg.ACTION_SPACE)
