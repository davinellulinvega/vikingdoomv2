#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'davinellulinvega'
from pathlib import Path
from os.path import join, isfile
from abc import abstractmethod, ABC
from copy import deepcopy
from itertools import chain
from scipy.spatial.distance import cityblock
import numpy as np
import boto3
from botocore.exceptions import ClientError
from settings import CtrlCfg, HeroCfg, BUCKET_NAME, AWS
from utils import ItemType, EnemyType


class Controller(ABC):
    """
    This is abstract class defines a generic API through which a controller
    interact with its hero and the Game in general.
    """

    # Declare the logging facility as a class attribute
    # This way even class method can use it
    _log = CtrlCfg.LOG

    def __init__(self, action_space, game_id, hero_id, training=True,
                 monitor=False, misc_config=None):
        """
        Declare some common attributes, such as Queues for communication with
        the game engine, as well as the ID of the corresponding hero. The whole
        hero object is not used here, since it would just mean a redundancy in
        the data.
        Furthermore, each controller is its own process. This is done to
        increase the processing speed, especially, since players do not have to
        be queried sequentially for action.
        :param action_space: List. A list of tuples, representing the
        different actions available to the hero (and therefore the controller).
        :param game_id: An integer representing the ID of the current game.
        :param hero_id: An integer representing the ID of the hero associated
        with the current controller.
        :param training: Bool. A boolean value indicating whether the
        specific controller should be in training or testing mode.
        :param monitor: Bool. A boolean value indicating whether the agent's
        behavior should be monitored or not.
        :param misc_config: Dict. A dictionary containing some more parameters
        specific to each controller.
        """

        # Initialize the parent class
        super(Controller, self).__init__()

        # Store the action space for later use
        self._action_space = action_space

        # Record the associated hero's and game's IDs, as well as the
        # action_space for later use
        self._game_id = game_id
        self._hero_id = hero_id

        # Create an empty array to contain the monitoring data for the up
        # coming game
        if misc_config is None:
            self._store = np.empty((0, CtrlCfg.NB_MONITOR_FEATURES),
                                   dtype=np.float32)
        else:
            # Make sure the misc_config is a dictionary and not anything else
            # weird
            assert isinstance(misc_config, dict), "The configuration given " \
                                                  "in parameter should be a " \
                                                  "dictionary of values."
            self._store = np.empty((0, misc_config.get('nb_monitor_features', CtrlCfg.NB_MONITOR_FEATURES)),
                                   dtype=np.float32)

        # Declare some additional attributes required for the learning phase
        # and storing the corresponding hero's state
        self._old_state = None
        self._old_hero = None
        if action_space is not None:
            self._action = action_space[-1]
        else:
            self._action = None

        # Declare some attributes useful for the monitoring
        self._cycle = 0

        # Define an attribute to switch between the training and testing mode
        self._training = training

        # Define an attribute to switch the monitoring feature ON or OFF
        self._monitoring = monitor

        # Declare a variable to store the total amount of reward gathered by
        # the controller
        self._total_reward = 0

    def init(self, env):
        """
        For any sub-class, this method should contain the logic related to
        the initialization of the different components. This method is only
        present because Keras, as it stands, does not play nice with
        multi-processing and requires its library to be loaded in particular
        ways. Therefore, this method should be called after start() has been
        triggered and the controller is running in its own process.
        :param env: Environment. An instance of the Environment class,
        containing the initial game's state.
        :return: Nothing
        """

        # Try to load the controller from its default location if possible
        if not self._load():
            # Otherwise build it from scratch
            self._build()

        if env is not None:
            # Observe the state
            self._old_state = self._observe(env)

            # Initialize the old_hero
            self._old_hero = deepcopy(env.heroes[self._hero_id])

    def to_dict(self):
        """
        Build a representation of the current controller in the form of a dict.
        :return: Dict. A dictionary containing the main characteristics of the
        current controller.
        """

        return {'game_id': self._game_id, 'hero_id': self._hero_id}

    @classmethod
    def upload(cls, files=None):
        """
        Upload the different monitoring/data/result file to the cloud,
        for backups and ease of retrieval.
        :param files: List. A list of paths to the files to upload to the
        VikingDoom bucket. The paths can be specified as either strings,
        representing an absolute path to the file to upload. Or a tuple
        containing the absolute path as a string and the object's name once
        stored in the cloud.
        :return: Nothing.
        """

        # If this is not running on a server, there is no need to upload to
        # the cloud
        if not AWS:
            return

        # Get a new client's instance for the S3 service
        s3 = boto3.client('s3')

        # Make sure there are any files to upload
        if files is not None:
            # For each file in the list
            for file in files:
                if isinstance(file, tuple):
                    file, obj_name = file
                else:
                    # Extract the object's name, which basically is the path
                    # within the bucket where the uploaded file will reside
                    obj_name = join(*Path(file).parts[-2:])

                # Make sure the file exist on disk
                if not isfile(file):
                    # Otherwise skip the rest and move on to the next file
                    continue

                # Upload the file to the bucket
                try:
                    s3.upload_file(file, BUCKET_NAME, obj_name)
                except ClientError as e:
                    cls._log.exception('An error occurred, while trying to '
                                       'upload the files.'
                                       '\nError message was: {}'.format(e))
                    exit(-1)

    @abstractmethod
    def _learn(self, old_state, action, curr_state, reward, done):
        """
        For any sub-class, this method should contain the logic related to the
        learning process.
        :param old_state: List. A list of extracted features describing the
        previous state of the environment.
        :param action: Tuple. A tuple representing the action performed to go
        from the old to the current state.
        :param curr_state: List. A list of extracted features describing the
        current state of the environment.
        :param reward: Float. A float indicating the how good/bad the action
        undertaken was.
        :param done: Bool. Signal the end of an episode/task.
        :return: Nothing.
        """

        pass

    @abstractmethod
    def action(self, reward=0):
        """
        For any sub-class, this method should contain the logic related to the
        activation process, if any, and that of the decision-making process.
        This method should store the chosen action within the _action
        attribute, for use later in the learning process.
        :param reward: Float. A float value representing the reward
        corresponding to the previous action. This is only required for the
        PrimEmoCtrl controller. Hence, it is initialized to 0, by default.
        :return: tuple. A tuple representing the chosen action.
        """

        # Given the main game's loop, the state used for making a decision is
        # the one stored in self._old_state
        pass

    @abstractmethod
    def save(self):
        """
         Save any relevant file to disk. Preferably, most of the operations
         in this method should be atomic, so that data corruption is unlikely
         to occur.
        :return: Nothing.
        """

        pass

    @abstractmethod
    def _build(self):
        """
        Called by the init() method, when needed, to initialize the core of the
        controller.
        :return: Nothing.
        """

        # TODO: Use a default configuration, stored in the settings module.
        pass

    @abstractmethod
    def _load(self):
        """
        Load the controller using the content of the given file.
        :return: Bool. A boolean value indicating whether the controller
        could be loaded or not.
        """

        # TODO: Use a default location for the save/load file, stored in the
        #  settings module.
        pass

    def _observe(self, env):
        """
        Observe the current state of the environment.
        :param env: Environment. An instance of the Environment class,
        representing the current state of the game. It contains and manages
        all the objects that are part of the game (Enemies, Items and Hero).
        :return: List. A list containing the agent's observations of the
        current state of the environment.
        """

        # Get the current state of the corresponding hero
        curr_hero = env.heroes[self._hero_id]

        # Observe the environment
        # Get features related to the closest enemy
        enemies = sorted(env.enemies.values(),
                         key=lambda e: cityblock(e.position, curr_hero.position))
        e_features = []
        for e in enemies[0:2]:
            e_features.extend((e.position - curr_hero.position) / (env.width, env.height))
            e_features.append(e.health / HeroCfg.MAX_HEALTH)
            e_features.append(e.strength / HeroCfg.MAX_STRENGTH)

        # Make sure we have the right number of features to present to the network input
        while len(e_features) < 2 * 4:
            e_features.extend((1, 1, 0, 0))

        # Get features related to the closest gold and health items
        golds = sorted([i for i in env.items.values() if i.type in [ItemType.CHEST, ItemType.PURSE]],
                       key=lambda i: cityblock(i.position, curr_hero.position))
        potions = sorted([i for i in env.items.values() if i.type == ItemType.POTION],
                         key=lambda i: cityblock(i.position, curr_hero.position))
        g_features = []
        hp_features = []
        for g in golds[0:2]:
                g_features.extend((g.position - curr_hero.position) / (env.width, env.height))
        for hp in potions[0:2]:
                hp_features.extend((hp.position - curr_hero.position) / (env.width, env.height))

        # Make sure we have the right number of features to present to the network input
        # For both the gold and health items
        while len(g_features) < 2 * 2:
            g_features.extend((1, 1))

        while len(hp_features) < 2 * 2:
            hp_features.extend((1, 1))

        # Get features related to the closest obstacles
        sorted_obs = sorted(env.obstacles,
                            key=lambda o: cityblock(o, curr_hero.position))[0:CtrlCfg.MAX_NB_OBSTACLES]

        obstacles = list(chain.from_iterable([(obs - curr_hero.position) / (env.width, env.height)
                                              for obs in sorted_obs]))

        # Return the observations, reward and whether the episode is done or not
        return e_features + g_features + hp_features + obstacles + [(HeroCfg.MAX_HEALTH - curr_hero.health) / HeroCfg.MAX_HEALTH, curr_hero.strength / HeroCfg.MAX_STRENGTH]

    def _monitor(self, curr_env_state, curr_hero, action, reward, done):
        """
        Store all the necessary information within the storage file,
        to monitor the learning process.
        :param curr_env_state: List. A list of the features describing the
        environment's current state.
        :param curr_hero: Hero. An instance of the Hero class, representing
        the hero controlled by the current Controller.
        :param action: Int. An integer representing the action performed to
        reach the current state of the environment and corresponding to the
        reward gathered.
        :param reward: Float. The reward corresponding to performing the
        action in the context of the previous state.
        :param done: Bool. A boolean value indicating whether the
        episode/task has ended or not.
        :return: Nothing.
        """

        # Simply append the current cycle's data to the end of the storage
        self._store = np.vstack((self._store,
                                 [self._cycle, int(done), reward, action,
                                  int(curr_hero.action_is_valid),
                                  *curr_env_state,
                                  *[curr_hero.nb_fights[enemy_type]
                                    for enemy_type in EnemyType],
                                  curr_hero.nb_fights['hero'],
                                  *[curr_hero.nb_items_gathered[item_type]
                                    for item_type in ItemType]
                                  ]))

    def _download(self, files=None):
        """
        Download a list of objects from the default S3 bucket.
        :param files: List. A list of string representing the different files to
        be downloaded, as well as the location where to download them locally.
        :return: Nothing.
        """

        # If this is not running on a server, there is no need to download from
        # the cloud
        if not AWS:
            return

        # Make sure there is something to download
        if files is not None:
            # Instantiate an S3 client
            s3 = boto3.client('s3')

            # For each file in the list
            for file in files:
                # Extract the object name
                obj_name = join(*Path(file).parts[-2:])

                # Try to download the file
                try:
                    s3.download_file(BUCKET_NAME, obj_name, file)
                except ClientError as e:
                    self._log.exception('An error occurred, while trying to '
                                        'download the file: {}.\n'
                                        'Error was: {}'.format(file, e))

    def step(self, env):
        """"
        This method should contain the main loop of the controller. Thus,
        it sequentially calls on _observe, _learn and _activate. The loop
        repeats until None is received on the input queue.
        :param env: An instance of the Environment class, containing the
        current state of the game.
        :return:
        """

        # Observe the environment (extract the required features
        # describing the environment's state.)
        curr_state = self._observe(env)

        # Get the current state of the corresponding hero
        curr_hero = deepcopy(env.heroes[self._hero_id])

        # Check if the episode/task has ended (hint the hero dies at the
        # end)
        done = curr_hero.is_dead

        # Compute the reward associated with the action that led to this
        # state
        if done:
            reward = CtrlCfg.DEATH_PUNISHMENT
            self._log.debug('The agent has died ...')
        elif not curr_hero.action_is_valid:
            reward = CtrlCfg.INVALID_MOVE_PUNISHMENT
            # Reset the validity of the action
            curr_hero.action_is_valid = True
        else:
            # In this formula:
            #   + 50 corresponds to the highest amount of gold an agent
            #   can gather on one cycle (reward for defeating a dragon)
            #   To make Chests more attractive to the agent, the
            #   difference in gold is only divided by 25.
            #   + 10 corresponds to the highest amount of strength an
            #   agent can gather on one cycle (reward for defeating a
            #   dragon)
            #   + The difference in health is normalized with the
            #   max_strength, since this represent the highest amount
            #   of life an agent can loose on a single cycle,
            #   which incidentally is also the maximum amount of health
            #   it can gain on a single cycle, by collecting potion
            reward = (curr_hero.gold - self._old_hero.gold) / 25 + \
                     (curr_hero.health - self._old_hero.health) / 25 + \
                     (curr_hero.strength - self._old_hero.strength) / 10 \
                     - 0.001

        # Accumulate the rewards
        self._total_reward += reward

        if self._training:
            # Learn
            self._learn(self._old_state, self._action, curr_state,
                        reward, done)

        # Record the current environment's and hero's states
        self._old_state = curr_state
        self._old_hero = curr_hero

        # Monitor the hero's and environment's state
        if self._monitoring:
            self._monitor(curr_state, curr_hero, self._action, reward,
                          done)

        # Increase the number of cycles
        self._cycle += 1
        self._log.debug('Done with cycle {}'.format(self._cycle))

    @property
    def total_reward(self):
        return self._total_reward
