#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'davinellulinvega'
from utils import CtrlType
from RandomCtrl import RandomCtrl
from DeepQCtrl import DeepQCtrl
from ActorCriticCtrl import ActorCriticCtrl
from PrimEmoCtrl import PrimEmoCtrl


class ControllerFactory(object):
    """
    A simple Factory in charge of instantiating controllers based on the
    given initial parameters.
    """

    @classmethod
    def get_controller(cls, ctrl_type, action_space, game_id,
                       hero_id, training, monitor, misc_config=None):
        """
        Given the type of controller requested, create, configure and return a
        new instance of the Controller sub-class.
        :param ctrl_type: Enum. An instance of the CtrlType enumeration.
        :param action_space: List. A list of tuples, representing the
        different actions available to the hero (and therefore the controller).
        :param game_id: Int. An integer uniquely identifying the game the
        controller is taking part to.
        :param hero_id: Int. An integer uniquely identifying the hero
        corresponding to the current controller.
        :param training: Bool. A boolean value indicating whether the
        controller should be instantiated in its training mode or not.
        :param monitor: Bool. A boolean value indicating whether the agent's
        behavior should be monitored or not.
        :param misc_config: Dict. A dictionary containing some more parameters
        specific to each controller.
        :return: Controller. An instance of a class implementing the
        Controller interface.
        """

        # If a string has been provided for the ctrl_type parameter, try to
        # transform it into an instance of the CtrlType enumeration
        if isinstance(ctrl_type, str):
            ctrl_type = CtrlType(ctrl_type)

        # Make sure the given controller type is indeed an instance of the
        # CtrlType enumeration
        assert isinstance(ctrl_type, CtrlType), "The controller type should " \
                                                "either be an instance of " \
                                                "the CtrlType enumeration or " \
                                                "a string."

        # Based on the given controller type, select the right class to
        # instantiate
        if ctrl_type == CtrlType.DEEP_Q:
            ctrl_cls = DeepQCtrl
        elif ctrl_type == CtrlType.ACTOR_CRITIC:
            ctrl_cls = ActorCriticCtrl
        elif ctrl_type == CtrlType.PRIM_EMO:
            ctrl_cls = PrimEmoCtrl
        elif ctrl_type == CtrlType.PRIM_EMO_LES:
            ctrl_cls = None
        elif ctrl_type == CtrlType.RANDOM:
            ctrl_cls = RandomCtrl
        else:
            raise RuntimeError('Controller type unknown: {}'.format(ctrl_type))

        # Return new instance of the controller class
        return ctrl_cls(action_space, game_id, hero_id, training, monitor,
                        misc_config=misc_config)
