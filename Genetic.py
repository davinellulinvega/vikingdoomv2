#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'davinellulinvega'
from os.path import isfile, join
from argparse import ArgumentParser
from pathlib import Path
from random import choices
from statistics import mean
from multiprocessing import Pool
import numpy as np
from deap import base, tools, algorithms, creator
import joblib
import boto3
from botocore.exceptions import ClientError
from Game import Game
from settings import GACfg, BUCKET_NAME, AWS
from utils import CtrlType

# Declare the Fitness
creator.create('Fitness', base.Fitness, weights=(1.,))

# Declare an Individual, inheriting from Python's list class
creator.create('Individual', list, fitness=creator.Fitness)


def upload():
    """
    Upload the different monitoring/data/result file to the cloud,
    for backups and ease of retrieval.
    :return: Nothing.
    """

    # Get a new client's instance for the S3 service
    s3 = boto3.client('s3')

    # Extract the object's name, which basically is the path
    # within the bucket where the uploaded file will reside
    obj_name = join(*Path(GACfg.CKPT_FILE_PATH).parts[-2:])

    # Upload the file to the bucket
    try:
        s3.upload_file(GACfg.CKPT_FILE_PATH, BUCKET_NAME, obj_name)
    except ClientError as e:
        GACfg.LOG.exception('An error occurred, while trying to '
                            'upload the file: {}.'
                            '\nError was: {}'.format(GACfg.CKPT_FILE_PATH, e))


def download():
    """
    Download a list of objects from the default S3 bucket.
    :return: Nothing.
    """

    # Instantiate an S3 client
    s3 = boto3.client('s3')

    # Extract the object name
    obj_name = join(*Path(GACfg.CKPT_FILE_PATH).parts[-2:])

    # Try to download the file
    try:
        s3.download_file(BUCKET_NAME, obj_name, GACfg.CKPT_FILE_PATH)
    except ClientError as e:
        GACfg.LOG.exception('An error occurred, while trying to '
                            'download the file: {}.\n'
                            'Error was: {}'.format(GACfg.CKPT_FILE_PATH, e))


def swap_content(p1, p2):
    """
    Randomly choose two cut off points. The content, contained between those
    two points, is swapped in both parents.
    :param p1: np.ndarray. A 1-D numpy array, representing the first parent.
    :param p2: np.ndarray. A 1-D numpy array, representing the second parent.
    :return: Nothing. Both parents are modified in-place.
    """

    # Make a copy of both parents
    p1c = np.copy(p1)
    p2c = np.copy(p2)

    # Crossover with only one value is just swapping the parents around
    if len(p1) == 1:
        p1[:] = p2c
        p2[:] = p1c
        return p1, p2

    # Choose the two crossover points randomly within the parent
    # The first point is forced in the first half of the array
    pt1 = np.random.randint(low=0, high=len(p1) // 2)
    # While the second one, has to be in the second half
    pt2 = np.random.randint(low=len(p1) // 2, high=len(p1))

    # Swap the content between the two parents
    p1[pt1:pt2], p2[pt1:pt2] = p2c[pt1:pt2], p1c[pt1:pt2]


def two_points_crossover(p1, p2):
    """
    Simply exchange the content of the two given parents which is located
    between the two randomly chosen crossover points.
    :param p1: List. A list of numpy arrays of dimension 1 or 2, containing the
    weight for the PrimEmo network.
    :param p2: List. A list of numpy arrays of dimension 1 or 2, containing the
    weight for the PrimEmo network.
    :return: Tuple. Both parents are modified in-place and returned by the
    crossover method, as offsprings.
    """

    # Since each individual is a list of numpy arrays, apply the cross-over
    # to the subsequent arrays, not the list as a whole
    for sp1, sp2 in zip(p1, p2):
        # Make sure both parents have the same shape
        assert sp1.shape == sp2.shape, "Both parent should have the same " \
                                       "shape for crossover to happen, but " \
                                       "got {} and {}.".format(sp1.shape,
                                                               sp2.shape)

        if sp1.ndim == 2:
            # Apply the two points crossover to each of the subsequent arrays
            for ssp1, ssp2 in zip(sp1, sp2):
                swap_content(ssp1, ssp2)
        elif sp1.ndim == 1:
            # Apply the two points crossover to the parents directly
            swap_content(sp1, sp2)
        else:
            raise NotImplementedError("Two points crossover only applies to "
                                      "1-D or 2-D arrays, got {} and "
                                      "{}".format(p1.shape, p2.shape))

    # Finally return both modified parents
    return p1, p2


def gaussian_mutation(a, mu, sigma, indpb):
    """
    Randomly mutate the weights within the given array.
    :param a: List. A list of numpy arrays of 1 or 2 dimensions, containing the
    weights or bias of the PrimEmo network.
    :param mu: Float. The mean of the Gaussian/Normal distribution.
    :param sigma: Float. The standard deviation of the Gaussian/Normal
    distribution.
    :param indpb: Float. The independent probability for each attribute to be
    mutated.
    :return: Tuple. A tuple containing the mutated individual.
    """

    # Since an individual is a list of numpy arrays, apply the mutation to
    # each subsequent array
    for arr in a:
        # Build an array of normally distributed random values, with the same
        # shape as arr
        n = np.random.normal(loc=mu, scale=sigma, size=arr.shape)

        # Build an array of random values, used as a mask
        r = np.random.random(size=arr.shape)

        # Compute the mutated version of the array passed in argument
        mutated_arr = np.where(r < indpb, n, np.zeros(arr.shape))

        # This is only here because the mutation operator is supposed to modify
        # the array in place. Otherwise, one could stop at the previous line
        arr += mutated_arr

    # Return the mutated array
    return a,


def checkpoint(gen, pop, hof):
    """
    Save the current state of the GA process, in a compressed file.
    Essentially creating a checkpoint from which the GA process can be resumed.
    :param gen: Int. An integer representing the current generation.
    :param pop: List. A list of individual representing the current
    population.
    :param hof: deap.tools.HallOfFame. A HallOfFame object containing the
    best individual that ever lived.
    :return:
    """

    # Dump a dictionary containing the different objects into the checkpoint
    # file
    joblib.dump({'generation': gen, 'population': pop, 'hall_of_fame': hof},
                filename=GACfg.CKPT_FILE_PATH,
                compress=('lz4', 4))


def rank_selection(pop, k):
    """
    Implement the rank selection algorithm, in which the probability of an
    individual being chosen is proportional to the rank of its first fitness
    value.
    :param pop: List. A list of individuals, representing the population from
    which to select individuals.
    :param k: Int. An integer defining the size the selected population
    should have.
    :return: List. A list containing the selected individuals.
    """

    # Rank the individuals in the initial population, according to their
    # first fitness value
    sorted_pop = sorted(pop, key=lambda i: i.fitness.values[0])

    # Randomly choose the individuals, according to their rank, and return them
    return choices(sorted_pop, weights=range(1, len(pop) + 1), k=k)


# There is a distinct lack of any logging within this function. This is
# mostly due to the fact that multiple evaluation phases will be happening at
# the same time. Since python's logging facility has not been implemented
# with multiprocessing in mind, it is better to not have any in there.
def evaluation(individual):
    """
    Evaluate the individual given in parameter. The evaluation simply consist
    in playing a game until the end.
    :param individual: List. A list of numpy arrays representing the weights and
    biases of the different layers in the PrimEmo architecture.
    :return: tuple. A tuple representing the individual's fitness.
    """

    # Load the environment's configuration
    env_conf_file = GACfg.ENV_CONFIGS

    # Initialize the number of games to play and the list of reward for each
    # individual
    games_left = 10
    rewards = []
    # Build the configuration for the specific individual
    misc_config = {'weights': individual}

    while games_left > 0:
        # Create a new game
        game = Game(env_conf_file, controllers=[CtrlType.PRIM_EMO],
                    training=True, monitor=False, render=False,
                    ctrls_configs=[misc_config])
        try:
            # Actually start the game
            game.play()
        except BaseException as e:
            # If anything happens with the game, just pretend nothing
            # ever happened and go on to the next iteration
            GACfg.LOG.exception("An error occurred, while playing the "
                                "game: {}".format(e))
            continue

        # Shut the game down
        game.shutdown()

        # Gather the game results
        rewards.extend(game.total_rewards.values())

        # Decrease the number of games left to play
        games_left -= 1

    # Compute the average total reward and return it as the individual's fitness
    return mean(rewards)


# Initialize and declare the different statistics
stats = tools.Statistics(key=lambda i: i.fitness.values)
stats.register('avg', np.mean)
stats.register('std', np.std)
stats.register('min', np.min)
stats.register('max', np.max)

# A little message to let the user know that everything went well
GACfg.LOG.debug('Done.')

if __name__ == "__main__":
    # Declare some command line options
    parser = ArgumentParser()
    parser.add_argument('--nb_gens',
                        type=int, required=True,
                        dest='nb_gens',
                        help='The number of generations to use within the '
                             'genetic process.')

    # TODO: This is a awkward, if time permits find another solution for this
    parser.add_argument('--extract_model', dest='extraction', action='store_true',
                        help='A flag indicating that the script should only '
                             'extract and save a model corresponding to the '
                             'PrimEmo controller, and not go through the '
                             'learning process.')

    # Parse the different command line arguments
    args = parser.parse_args()

    # Should we extract and save a model of the PrimEmo architecture
    if args.extraction:
        # Import the required additional modules
        from ControllerFactory import ControllerFactory
        from settings import EnvCfg

        # Download the checkpoint file from the S3 bucket
        download()

        # Load the checkpoint
        try:
            # Load the checkpoint file
            ckpt = joblib.load(GACfg.CKPT_FILE_PATH)

            # Let the user know that we are loading from file
            GACfg.LOG.info('A valid checkpoint file was found.\nLoading '
                           'any available data ...')
        except (FileNotFoundError, EOFError):
            # Let the user know that we are starting from scratch again
            GACfg.LOG.info('Did not find any valid checkpoint file.\nExiting '
                           'the program ...')

            # Exit the program, since without checkpoint nothing can be done
            exit(-1)

        # Extract the custom weights from the hall of fame
        hall_of_fame = ckpt['hall_of_fame']

        # Define the custom configuration, containing the weights
        misc_config = {'weights': hall_of_fame[0]}

        # Build a new controller using the individual in the hall of fame as the
        # weights
        ctrl = ControllerFactory.get_controller(CtrlType.PRIM_EMO,
                                                action_space=EnvCfg.ACTION_SPACE,
                                                game_id=0, hero_id=0,
                                                training=False, monitor=True,
                                                misc_config=misc_config)

        # Complete the initialization process
        ctrl.init(None)

        # Save the controller to its corresponding directory
        ctrl.save()

    else: # Or go through the learning process
        # Try to download the last checkpoint, if none are locally available
        if AWS and not isfile(GACfg.CKPT_FILE_PATH):
            download()

        # Initialize the rest of the objects (generation, population and hall of
        # fame)
        try:
            # Load the checkpoint file
            ckpt = joblib.load(GACfg.CKPT_FILE_PATH)

            # Let the user know that we are loading from file
            GACfg.LOG.info('A valid checkpoint file was found.\nLoading from '
                           'last available data ...')
        except (FileNotFoundError, EOFError):
            # Let the user know that we are starting from scratch again
            GACfg.LOG.info('Did not find any valid checkpoint file.\nStarting '
                           'from scratch ...')

            # Initialize the checkpoint with an empty dictionary
            ckpt = {}

        # Initialize the population
        population = \
            ckpt.get('population',
                     [creator.Individual([np.random.normal(loc=0, scale=1,
                                                           size=shape)
                                          for shape in GACfg.WEIGHT_SHAPES])
                      for _ in range(GACfg.POPULATION_SIZE)])

        # Initialize the generation meter
        generation = ckpt.get('generation', 0)

        # Initialize the Hall of Fame
        hall_of_fame = ckpt.get('hall_of_fame',
                                tools.HallOfFame(maxsize=2, similar=np.array_equal))

        # Register the methods required by the eaSimple algorithm
        toolbox = base.Toolbox()
        toolbox.register('mate', two_points_crossover)
        toolbox.register('mutate', gaussian_mutation, mu=0, sigma=1,
                         indpb=GACfg.IND_PROB)

        # Declare a multi-processing pool to accelerate the computation
        with Pool(processes=GACfg.NB_PROCS, maxtasksperchild=GACfg.POPULATION_SIZE // GACfg.NB_PROCS) as pool:
            # Apply the GA process, until the number of generation is reached
            while generation < args.nb_gens:
                # Evaluate the different individuals
                eval_pop = [i for i in population if not i.fitness.valid]
                nb_evals = len(eval_pop)
                fitnesses = pool.imap(evaluation, eval_pop, chunksize=len(eval_pop) // GACfg.NB_PROCS)

                # Assign the fitness to the corresponding individual
                for fitness, ind in zip(fitnesses, eval_pop):
                    ind.fitness.values = (fitness,)

                # Compile and log the statistics about the current population
                records = stats.compile(population)
                records.update({'evals': nb_evals, 'gen': generation})
                GACfg.LOG.info("Statistics:\ngen\tevals\tavg\tstd\tmin\tmax\n"
                               "{gen}\t{evals}\t{avg}\t{std}\t{min}"
                               "\t{max}".format(**records))

                # Update the Hall of Fame
                hall_of_fame.update(population)

                # Select the parents for the next population
                selected = rank_selection(population, k=len(population))

                # Mate and mutate the selected parents to produce the next
                # population
                population = algorithms.varAnd(selected, toolbox, GACfg.CX_PROB,
                                               GACfg.MU_PROB)

                # Increase the generation meter
                generation += 1

                # Make a new checkpoint
                checkpoint(generation, population, hall_of_fame)

                if AWS:
                    # Upload the checkpoint file
                    upload()
