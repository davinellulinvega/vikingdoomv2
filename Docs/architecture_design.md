# Game
   * Takes the role of the `main` script, initializing all components and managing their different life cycles.
   * It should be noted, that a websocket server/client will be implemented to allow for plug and play display. Should the server be a different script? And only the client is part of the game (on exception, client is set to None and no information is sent to the websocket server)?
   * Each controller, as well as the websocket server will be launched in different processes and communicating via `multiprocessing.Pipe` to optimize for speed, or `multiprocessing.Queue` for convenience (especially `SimpleQueue`, since we only need `put` and `get` methods).
   * The decision to isolate controllers in different processes, results from the fact that Keras (with tensorflow backend) behaves whenever it is:
       a) loaded in the same thread.
       b) imported within method inside different threads.

## Attributes
   * controllers = `[ctrl_proc_1, ctrl_proc_2, ctrl_proc_3, ctrl_proc_4]  # Only contains the processes in which the controllers run`
   * server = `socketio.Server()  # Will also be in separate process, which is the object that will in fact be store here, rather than the server itself`
   * queues_in = `[q_in, ..., q_in]  # A Queue to gather each controller's action`
   * queues_out = `[q_out, ..., q_out]  # A Queue to send each controller the state of the environment`
   * q_server = `{1: (q_in, q_out), 2: (q_in, q_out), 3: (q_in, q_out), 3: (q_in, q_out), 'server': q_out}  # Contains the different queues used for communicating between the different processes`
   Note that `controllers` and `queues` might be implemented as a single dictionary, where the process is the key and the queues, the values.

# Environment
## Attributes
### Private
   * turn
   * enemies = `[enemy_1, enemy_2, ...]  # Each enemy is an object, so just store them in a list for ease of management`
   * items = `[((x, y), type), ...]  # Items are implemented as namedtuples, since they do not change over their lifetime`
   * heroes = `[hero_0, hero_1, hero_2, hero_3]  # Similar to enemies, heroes are objects. Therefore, using a list makes the most sense here.`
   * width
   * height
   * empty_tiles = `set((x, y), (x, y), ...)`
   * obstacles = `set((x, y), (x, y), ...)` (There is no need to differentiate the type of obstacle, just that it is an obstacle and not an enemy or an item).
   * probas = `{'area_1': {'chest': ..., 'purse': ..., 'potion': ..., 'skeleton': ..., 'troll': ..., 'dragon': ...}, 'area_2': ..., 'area_3': ...}`
   * trees = `{'item': cKD_tree(), 'enemy': cKD_tree, 'hero': cKD_tree}  # Or other implementation from scikit-learn. Three trees are required, since only the positions can leave on the tree, no object.`
       Given the low number of items, enemies and heroes in the game, would not the 'naive' method be more convenient, if a little slower? A specially, since you'll need to go through the objects again, to find the rest of the information that should be provided to the heroes anyway?

### Public
	* to_hdf5: Used to store every game state in a HDF5 formatted file, for later analysis?

## Methods
### Private
   * add_hero
   * add_enemy
   * add_item
   * delete_hero
   * delete_enemy
   * delete_item
   * update_trees
### Public
   * `__init__(config_file)`: The configuration of the environment is loaded from a **JSON** or **HDF5** formatted file.
   * step(act_h1, act_h2, act_h3, act_h4): 
       * Returns all observation for each of the four heroes as a tuple. This is to save on processing time, when finding the nearest item/enemy/hero from the current hero. So each observation is fine tuned to each of the heroes. 
       * The observation returned might as well already be hero centric.
       * Should step also return a reward for each hero, thus ensuring that no bias is introduced by having different reward function for each agent.
       * The last returned element should be a variable indicating whether the game is done or not.
       * To summarize, this is to mimic what OpenAI gym is usually doing with their environments.


# Enemy
## Attributes
### Private
   * position X
   * position Y
   * strength
   * health
   * attack_radius
### Public
## Methods
### Private
   * get_features
### Public
   * step


# Item
## Attributes
   * type
   * value
   * position X
   * position Y
## Miscellaneous notes
   * To be implemented as either a `collections.namedtuple` or a class built with the third party `attrs` module.


# Hero
## Attributes
### Private
   * position X
   * position Y
   * strength
   * health
   * gold
   * is_dead
### Public
## Methods
### Private
### Public

# Controller
## Attributes
### Private
	* Hero
	* q_in
	* q_out
	* store
### Public
## Methods
### Private
   * learn
   * activate
   * save
   * build
   * load
   * get_features
### Public
	* run
	* init: Useful for actually building Keras network, which requires import to be done within the 
		process, so outside the `__init__` method.
