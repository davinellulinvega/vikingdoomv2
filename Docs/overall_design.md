# Simulation goals
   * The simulation results should serve to highlight, the survival, emotional
     and decision-making capabilities of the different architectures competing.
   * To this end, the results of interest for the thesis are:
      * Correlation between environmental features/internal state, and the
        activity of the amygdala's output layer.
      * The amount of gold gathered.
      * The number of cycles survived. If the maximum amount of game cycle has
        been reached and heroes are still alive, the amount of remaining life is
        also a measure of great interest. (Or you could simply consider that
        heroes that died before the end have a remaining life of 0).
      * Number of items gathered, per type of item.
      * Number of fights, against other heroes, against enemies. As well as,
        number of fights won and lost.
      * Final level of strength.
      * Seeking behavior, when health or strength are low and the corresponding
        resource is not directly observable/available. This forces the use of a
        fog of war, so that resources are not necessarily directly observable.
        In turn, it allows us to highlight the seeking behavior/exploration
        required to get the needed resource. Hence, highlighting the survival
        capabilities attributed to Prim/ProtoEmo.
      * Acts of sympathy or fear/restraint  when a hero has enough
        strength/health, compared to other. Avoid unnecessary fights.

# Learning
   Learning of the complete simulation should be split into as many steps as
   there are concepts in this game. Introducing a new concept at each step,
   enable the different algorithm to slowly learn the new parameters. More
   importantly though, it allows us to closely monitor the algorithm's evolution
   and detect any problem/complexities as early as possible.

# Testing
   During the testing phase, and because we have chosen a battle royal style of
   playing, maps never seen during the learning phase should be presented in
   either random or sequential order.  It should be noted that choosing the
   sequential order would make your results more reproducible, since it removes
   some of the randomness. Unless the random seed is set to a particular value
   throughout the whole learning and testing phases.

# Game
   The sequence of actions to trigger for each turn of a game is as follow:
   * Observe the state of the environment.
   * To each hero (where the order is chosen at random on each turn):
      * Collect neighboring items.
      * Give said observation.
      * Get next action from hero.
      * Execute next action if possible. The requested action is taken into
          account first, to allow heroes to run away from a fight, if they wish.
          If no action or Idle is chosen and the context fits, try to perform
          any of the automatic actions: fight with another hero, or an enemy
          unit. (yep that's it in this version, no buying, no nothing else).
   * All enemy units that can move, should do so next. Or if the context is
       right, have them fight the closest hero (where close is defined as being
       on one of the neighboring tiles).

   The game ends if any of the following conditions are fulfilled:
      * All heroes are dead.
      * Or the game reaches 1000 turns.

# Environment
   The map corresponding to the environment is made of a simple back ground
   image containing all static elements (rocks, trees, lava, water, houses, and
   everything else that is considered an obstacle). Upon initialization, first
   the heroes are loaded and place on the map, then the enemy units and finally
   the items.

## Features
   * A square tiled map has been adopted as the basic layout. The size of said
      map should be either 25x25 or 50x50. The first size, correspond to smaller
      maps used for training purposes. The bigger maps (50x50) are used for the
      final simulation, when all elements of the game (enemies, items, heroes
      and obstacles) have been introduced and learned.

   * All obstacles are static and should, therefore, be declared in the file
      containing the map's definition. Here obstacles can take the shape of:
      * Trees
      * Rocks
      * Water
      * Lava
      * Houses
      * Grassy highlands
      * Edge of the map
      But not mines, since this new version of the game does not include any
      functional building anymore. This means no more caravan, market or mines.
      Mines are still represented alongside troll units. Tiles holding items
      cannot be stepped on either. However, rather than appearing as obstacles
      or enemies, they are a separate input to the algorithm, denoting a goal or
      opportunity.
      Items, enemies and other heroes, are also considered obstacles in the
      point of view of the current hero, since it cannot step on an already
      occupied tile. However, those objects are not static and therefore,
      cannot be included in the map's definition file.

   * Rather than store the map as an encoded string, and parse it for
       information, it should be a class holding the different entities. 
   * Each group of entities should be stored in a separate dictionary. In
       addition, the map should have and manage (keep up to date) a set of empty
       tiles, which represent the tiles that can be walked upon at each time.

# Building
   To keep this document as brief as possible, this section should only contain
   the strict minimum concerning buildings: There are none in this version.

   The decision has been made to drop both caravans and markets, even though
   they did present the player with some interesting possibilities in terms of
   strategy.  However, this comes at a cost, and in this case the cost is
   complexity.  Introducing the concept of exchanging gold for either health or
   strength, to agents controlled by algorithms, whose sole purpose is to
   maximize the amount of gold gathered during a game, is not easy. So, unless
   times permits it, nothing can be bought in the game for the moment.

   The reason behind the exclusion of mines is similar. Gold, in the point of view
   of the algorithm, is the incentive. Having a building that gives gold at a set
   rate, means that once a mine is acquired, any action afterward will be rewarded,
   regardless of its actual value.
   To avoid or workaround this problem, once again if time permits it, the amount
   of gold gathered by owned mines could be accumulated on a separate meter than
   the one acquired by finding purse/chest items.
   Finally, mines, even if absent from the game, still appear, on the map,
   alongside trolls, to represent the treasure that they guard. However, those mines
   are not considered an object on their own, and therefore, not implemented as
   such. They are only present for representational purposes.

# Enemy
   Enemies, in general, are characterized by a level of health, a level of
   strength, an initial position, a range of motion and an appetite for attacking
   heroes passing by.

## Location
   * In this section, the initial location of enemies is usually defined
       pseudo-randomly. Where pseudo-randomly is taken to mean that for each
       relevant region of the map, each type of enemy unit is assigned a
       probability of appearing. Hence, bottle necks would have a low
       probability of enemies appearing, to avoid blocking the players on one
       side, while the items might be on the other. Open spaces, on the
       contrary, have high probability of enemy appearing, since there should be
       enough space for players to circle around the danger.  Furthermore, the
       probability of a given enemy type to appear in each region also depends
       (dynamically increase/decrease) on the number of enemy units, of any
       type, already present. This is to avoid having too many enemies in the
       same location, which might leave other regions empty, but might also lead
       to a hero being overwhelmed by a too large group of enemies.
   * All this has been implemented via fulfill ratios that are 
       increased/decreased accordingly.

## Type and behavior
   * Skeleton:
      * Skeletons are the weakest of the mobile units, and should
        follow/attack/hunt the nearest hero within a set area. The area within
        which a skeleton is triggered, is centered around its initial location.
        The radius of the area of attack is to be defined. While the skeleton is
        not attacking (no hero is within its sight/area of action), it should
        walk back to its spawn location and wait there for the next hero to
        enter its area of attack.
      * Initially, each skeleton unit is created with a level of strength of 1
          and a level of health of 10 (corresponds to 1/10th of the hero's life).
      * For defeating a skeleton, a hero is rewarded with 1 gold and 1 levels of
          strength (it's the weakest one, what did you expect).
      * After being defeated, a skeleton unit simply disappears from the map. It
          will later be replaced by another enemy units, whose type and location
          will be pseudo-randomly chosen, when the right time comes.

   * Trolls:
      * Seating in the middle between skeleton and dragon, trolls are fairly
        tough opponents. Their strength level is 12.5 and they have 25 health
        points.
      * On the contrary to all the other enemy units, troll units are static,
        since they are each tied to a mine. As mentioned above, mines do not
        serve any purpose, other than a representational one. Mines graphically
        appear alongside the troll and stand as a reward.
      * Defeating a troll unit, is then rewarded with 3 levels of strength and
         10 gold.
      * Once it's been beaten, a troll and its mine disappear, freeing up the
        occupied tile. As with the skeleton, another enemy unit will take its 
        place when the time is right. The type and location of the new enemy
        unit are chosen pseudo-randomly.

   * Dragon:
      * This is the strongest of all enemy units. Similar to the skeleton units,
        dragons move to hunt/follow/attack the nearest hero within their action
        area. The radius of the action area is the same as the one used for the
        skeleton unit. However, because of their size, dragons can only move at
        half the speed of skeletons. (If the skeleton is capable of moving each
        game turn, then the dragon, only moves every two turns).
      * Each new dragon is initialized with a level of life of 75 and a level of
        strength equal to 25 (which correspond to a hero at full strength).
      * As with every other enemy units, defeating a dragon has the following
        consequences:
         * The unit disappears and should be replaced in due time by another
           enemy unit, whose type and location are chosen pseudo-randomly.
         * The vanquishing hero is awarded 10 levels of strength and 50 gold (or
           more?).

   By default when attacking a hero, enemy units deal damage at a rate equal to
   their respective level of strength. Hence, for each turn of the game, a
   skeleton deals 1 damage, a troll 12.5 and a dragon 25.

## Miscellaneous
   * For each enemy type, the maximum number of units present on the map at
      once, is set to:
      * Skeleton: 50-75, since they are the weakest, they can swarm some areas
         without overwhelming the different heroes.
      * Trolls: 20-40, being stronger than skeletons, trolls should be scarce
         enough, so that weak heroes do not encounter them too early in the game.
      * Dragon: 10, same reasoning as above. Dragons are the strongest enemies
         so heroes should only meet them late in the game, when they have had
         time to increase their strength level.

   * Among the different type of enemy units roaming the map, skeletons should
      be the most common type, followed by trolls. Lastly, given their
      overwhelming strength, dragons should be rare. If skeletons have 60%
      chances of appearing, then trolls appear with a probability of 30% and
      dragons with 10%.
      Rather than define such probability of apparition, should we just
      determine trigger points in the game after which trolls and dragon are
      available? This way, heroes have time to grow stronger before encountering
      a troll or a dragon.

   * Following the previous idea, at the beginning of the game, only 3/4th of
       the maximum allowed skeleton units should be initialized on the map.
       Thus, allowing heroes some time to grow in strength and discover the
       environment. Later on, either the probability or trigger points would
       introduce trolls and dragons, when heroes have a fighting chance.

# Hero
   On the map, each hero is represented by a viking of different color. Colors
   are assigned at random.
   The initial positions of the heroes are chosen so as to spread them evenly
   across the map. At the beginning of the game, heroes are initialized with
   their maximum level of health (set to 100 health points), minimum level of
   strength (which is 1 and not zero, otherwise they would not be able to harm
   enemies) and 0 gold.
   Their overall goal is to gather the maximum amount of gold, while staying
   alive for as long as possible.
   Heroes can not only fight against enemy units, to gain strength and gold,
   but also against each others. However, defeating another enemy does not
   trigger any reward (otherwise it would encourage strategies, where a single
   hero defeats all the other, then gathers some gold before dying).

## Feature
   * A hero is characterized by:
      * A level of health
      * A level of strength
      * An amount of gold gathered so far
      * A position (X, Y) on the map
      * An area within which he is capable of detecting objects, also called
          Fog Of War (FOW) hereafter. The FOW is defined as a circle around the
          hero's current position. Its radius should be set to a value slightly
          higher to that of the action area of either skeleton and dragon
          units. This way a hero is able to adapt its current strategy to avoid fighting enemies.
   * Once its amount of health point drops to 0, a hero is considered as dead.
       In this version of Viking Doom, a system of perma-death will be
       implemented (their is no after life). Therefore, once dead, the game
       ends for the corresponding player and he cannot perform any further
       action.
   * The amount of health and strength a given hero can have, during a game, is
       restricted to 100 health point and 25 levels of strength. If a hero
       tries to collect potion (health) item, while its health is already at
       the maximum, will result in pretty much nothing happening. The item
       stays on its tile and the hero does not gain any more health. The same
       is true for a hero, at maximum strength, defeating an enemy unit. The
       hero will be rewarded with gold only, but no more strength.
   * For each turn the order in which each hero's action is considered (and
       executed) is chosen randomly, so as to avoid any algorithms relying on a
       particular sequence to optimize its strategy.

   * For all controllers, gathering gold should be rewarded, depending on the 
       amount of gold collected, while death should be heavily punished. Those 
       two incentives answer the overall goal of the simulation defined above.

## Inputs from the environment
   The following features, describing the state of the environment and that of
   the different heroes, are provided to the different learning algorithms:

   * The location (along the X and Y axis), level of health and strength of the
     closest enemy within the FOW.
   * The location (along the X and Y axis) of the closest health item within the
       FOW.
   * The location (along the X and Y axis) of the closest gold item (regardless
     if it is a purse or chest) within the FOW. As well as the amount of gold to
     be gained, if the item were to be collected. The third value might be
     dropped in the final version, so that heroes have to guess as to the value
     of the item and the reward they are working toward.
   * The location (along the X and Y axis) of the 3 or 5 closest obstacles
       within the FOW. 3 is the minimum, so that the hero has an idea of what
       its surrounding looks and does not get stuck between obstacles or in
       corners. Here are considered as obstacles the following: walls, rocks,
       houses, lava, water, and so one, but not mines, items, enemies or other
       heroes.
   * The location (along the X and Y axis), the level of health, strength and
       gold of the closest hero within the FOW.
   * The level of strength, health and gold of the current hero. Those should
       suffice to describe its internal state.
   It should be noted that all locations mentioned within this section are
   computed to be relative to the hero's current position. This way the hero has
   an egocentric view of it environment. It also avoids having to add the hero's
   current position to the set of inputs for the learning algorithm.

# Item
## Location
   * All items within the game, regardless of the type, are here to support the
       hero in its endeavors (collect gold and survive). They appear
       pseudo-randomly around the map and at random interval from each other.
       Again this is done to prevent algorithms taking advantage of any
       frequency in the apparition.  Furthermore, to force heroes to move to
       collect a given item, items should never appear within 2 tiles of any
       hero.
   * As is the case for the enemy units, the location and type of new items are
       pseudo-random. Where pseudo-random, for the location, is taken to mean
       that each region of the map has a given probability of each item
       appearing. The probability for each item is independent from the number
       of enemy units or heroes already in the region, but should
       increase/decrease depending on the number of item (of any type) already
       present. This avoids having groups of items in large open areas and none
       in bottlenecks.

## Type
   * Purse: Collecting this item rewards the hero with 5 gold units (a value
       between the reward for defeating a skeleton and a troll). Once collected
       the item disappears and will later be replaced by another one, whose type
       and location (along the x and y axis) are chosen pseudo-randomly.
   * Chest: A chest being bigger than a purse, collecting it is rewarded with
       20-25 gold units (a value between the reward for defeating a troll and a
       dragon). Similarly to the previous item, once collected it disappears and
       will be replaced at a later time by another item, whose type and location
       are chosen pseudo-randomly.
   * Potion: This item is the only way for a hero to regain some of its life and
       avoid death. Therefore, collecting a potion increase the hero's health
       level by 25 units.

## Miscellaneous notes
   * At the moment it has not been decided if the item should appear each turn
       or only every other turn of the game. There are no obvious reasons for
       items to only appear every other turn. However, to allow for the
       possibility of testing the difference in strategies, a probability of
       appearing each turn should be used. It can be set to 1 for most of the
       testing phase.
   * Similar to what was suggested for the enemy units, at the beginning of a
       game, not all items should be placed in the environment. Only 3/4th of
       the gold items (purse and chest) should be scattered around, to create an
       incentive for heroes to move. Furthermore, in the initial phases of the
       game, heroes should not have any immediate need for potion to recover
       health. Therefore, only 1/5th of the potions can appear at the beginning,
       and more will come into play later in the game.
   * When the maximum number of items, of each type, has been spread around the
       environment, no new items are added until a hero collects one.

   * For each type of item, the maximum number of instances in the environment
       at once are:
          * Purse: 20 units
          * Chest: 10 units
          * Potion: 20 units

   * Define the maximum number of each items that are allowed per map, in map
     file. Potions should be a scarce resource, while chest and purse should be
     more common. Purses being the most common of items.
