#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'davinellulinvega'
from numpy import ndarray, asarray


class Vec2D(ndarray):
    """
    A simple wrapper around the numpy.ndarray class. This wrapper adds some
    getter/setter properties to a basic array of shape (2,). Since vectors are
    used for positions, it makes it easier to manipulate those
    concepts and perform mathematical operations on them.
    """

    def __new__(cls, x, y):
        """
        Initialize the new instance as a view on the data with the correct
        type (i.e.: Vec2D).
        This is the recommended way of inheriting from numpy's ndarray class.
        :param x: An integer representing the item's position along the X axis.
        :param y: An integer representing the item's position along the Y axis.
        :return: A view of the data with the correct type (i.e.: Vec2D).
        """

        return asarray((x, y)).view(cls)

    def __hash__(self):
        """
        Define a hash based on the two values contained within the instance.
        This hash/pairing function has been implemented following algorithms,
        found here:
            https://stackoverflow.com/questions/919612/mapping-two-integers-to-one-in-a-unique-and-deterministic-way
            http://www.szudzik.com/ElegantPairing.pdf
        :return: int. An integer computed based on the array's content.
        """

        # Make sure we have two non-negative integers
        a = 2 * self[0] if self[0] >= 0 else -2 * self[0] - 1
        b = 2 * self[1] if self[1] >= 0 else -2 * self[1] - 1

        # Compute the pairing
        c = int((a ** 2 + a + b) // 2 if a >= b else (a + b ** 2) // 2)

        # Return the rectifier pairing
        return c if (a < 0 and b < 0) or (a >= 0 and b >= 0) else -c - 1

    def __eq__(self, other):
        if isinstance(other, Vec2D):
            return self.tolist() == other.tolist()
        elif isinstance(other, tuple):
            return tuple(self.tolist()) == other
        elif isinstance(other, list):
            return self.tolist() == other
        else:
            raise NotImplementedError('Comparison between a Vec2D and an '
                                      'instance of type {} has not been '
                                      'implemented'.format(type(other)))

    def __ne__(self, other):
        if isinstance(other, Vec2D):
            return self.tolist() != other.tolist()
        elif isinstance(other, tuple):
            return tuple(self.tolist()) != other
        elif isinstance(other, list):
            return self.tolist() != other
        else:
            raise NotImplementedError('Comparison between a Vec2D and an '
                                      'instance of type {} has not been '
                                      'implemented'.format(type(other)))

    def __lt__(self, other):
        if isinstance(other, Vec2D):
            return self.tolist() < other.tolist()
        elif isinstance(other, tuple):
            return tuple(self.tolist()) < other
        elif isinstance(other, list):
            return self.tolist() < other
        else:
            raise NotImplementedError('Comparison between a Vec2D and an '
                                      'instance of type {} has not been '
                                      'implemented'.format(type(other)))

    def __le__(self, other):
        if isinstance(other, Vec2D):
            return self.tolist() <= other.tolist()
        elif isinstance(other, tuple):
            return tuple(self.tolist()) <= other
        elif isinstance(other, list):
            return self.tolist() <= other
        else:
            raise NotImplementedError('Comparison between a Vec2D and an '
                                      'instance of type {} has not been '
                                      'implemented'.format(type(other)))

    def __gt__(self, other):
        if isinstance(other, Vec2D):
            return self.tolist() > other.tolist()
        elif isinstance(other, tuple):
            return tuple(self.tolist()) > other
        elif isinstance(other, list):
            return self.tolist() > other
        else:
            raise NotImplementedError('Comparison between a Vec2D and an '
                                      'instance of type {} has not been '
                                      'implemented'.format(type(other)))

    def __ge__(self, other):
        if isinstance(other, Vec2D):
            return self.tolist() >= other.tolist()
        elif isinstance(other, tuple):
            return tuple(self.tolist()) >= other
        elif isinstance(other, list):
            return self.tolist() >= other
        else:
            raise NotImplementedError('Comparison between a Vec2D and an '
                                      'instance of type {} has not been '
                                      'implemented'.format(type(other)))

    # The setter and getter below are the only reason this wrapper exist, not
    # ideal, but this is the best I can do until I find a better solution
    # for this
    @property
    def x(self):
        return self[0]

    @x.setter
    def x(self, new_x):
        self[0] = new_x

    @property
    def y(self):
        return self[1]

    @y.setter
    def y(self, new_y):
        self[1] = new_y
