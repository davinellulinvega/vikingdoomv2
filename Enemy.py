#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'davinellulinvega'
from queue import PriorityQueue
from scipy.spatial.distance import cityblock
from Vec2D import Vec2D
from utils import EnemyType
from settings import EnemyCfg


class Enemy(object):
    """
    This class represents an enemy unit in the environment. Enemies are
    characterized by a type, as well as a level of health, a location on the
    map and an ID. The type of enemy dictates its strength, attack radius and
    whether the enemy is static or not (only trolls are static, since they
    are tied to mines). Although, both dragons and skeletons are mobile
    units, dragons are twice as slow as skeletons, this allows Heroes to more
    easily run away from those fearsome creatures.
    """

    def __init__(self, e_id, e_type, position):
        """
        Initialize the different attributes of the enemy unit, given its type.
        :param e_id: Int. A integer representing the Enemy ID.
        :param e_type: EnemyType. An enumeration specifying the type of enemy
        unit to instantiate.
        :param position: Vec2D. The initial position of the Enemy unit along
        the X and Y axis.
        """

        # Instantiate the parent class
        super(Enemy, self).__init__()

        # Make sure the type provided is either a string or an instance of the
        # EnemyType enumeration defined
        assert isinstance(e_type, str) or isinstance(e_type, EnemyType), \
            "The enemy type should either be a valid string or an instance of" \
            " the EnemyType enumeration."

        # Define and set the parameter common to all enemy types
        self._id = e_id
        if isinstance(position, tuple):
            position = Vec2D(*position)
        self._init_position = position
        self._position = position
        if isinstance(e_type, str):
            self._type = EnemyType(e_type)
        else:
            self._type = e_type

        # Define and set the parameters specific to the enemy type
        if self._type == EnemyType.SKELETON:
            self._health = EnemyCfg.SKELETON_HEALTH
            self._strength = EnemyCfg.SKELETON_STRENGTH
            self._attack_radius = EnemyCfg.SKELETON_ATTACK_RADIUS
            self._is_static = False
        elif self._type == EnemyType.TROLL:
            self._health = EnemyCfg.TROLL_HEALTH
            self._strength = EnemyCfg.TROLL_STRENGTH
            self._attack_radius = 0
            self._is_static = True
        elif self._type == EnemyType.DRAGON:
            self._health = EnemyCfg.DRAGON_HEALTH
            self._strength = EnemyCfg.DRAGON_STRENGTH
            self._attack_radius = EnemyCfg.DRAGON_ATTACK_RADIUS
            self._is_static = False
        else:
            raise RuntimeError("Unknown EnemyType: {}".format(self._type))

    def to_dict(self):
        """
        Store the whole Enemy description into a dictionary for ease of
        processing by other objects.
        :return: Dict. A dictionary containing all the attributes that
        describe the current enemy unit.
        """

        return {'id': self._id,
                'position': self._position.tolist(),
                'type': self._type.value,
                'health': self._health,
                'strength': self._strength,
                'is_static': self._is_static}

    def step(self, env):
        """
        Observe the environment's state and decide on the next action to
        perform.
        :param env: Environment. The current environment to be observed.
        :return: Tuple. A tuple representing the translation along the X and
        Y axis, the enemy wishes to perform next. (0, 0) means Idle, which is
        not considered an action.
        """

        # Only non-static enemies can move, the others always return idle
        if self._is_static:
            return 0, 0

        # Dragons only move once every two turns
        if self._type == EnemyType.DRAGON and env.turn % 2 == 0:
            return 0, 0

        # Observe the environment
        goal, dist = self._observe(env.heroes.values())

        # If no hero is within attacking distance
        if goal is None:
            # Check if the enemy unit is already back in its initial position
            if self._position == self._init_position:
                # If so, just wait around and do nothing (maybe drink some mead)
                return 0, 0

            # The goal is to go back to the initial position
            goal = self._init_position

        # Find shortest path to nearest hero
        if dist == 0:
            # The enemy is already in attacking distance of the hero
            return 0, 0
        else:
            # Get the set of empty/walkable tiles from the environment
            empty_tiles = env.empty_positions

            # A* search of the goal from the current position
            start = self._position
            frontier = PriorityQueue()
            frontier.put((0, start))
            came_from = {start: None}
            cost_so_far = {start: 0}

            # While there are still new states to explore
            while not frontier.empty():
                # Get the current position
                current = frontier.get()[1]

                # If the goal has been reached
                if current == goal:
                    # The search is over, so break out of the loop
                    break

                # For each neighbor of the current position
                for neig in set([current + act for act in env.action_space]).intersection(empty_tiles):
                    # Compute the cost of travelling to this neighbor
                    new_cost = cost_so_far[current] + 1

                    # If the neighbor has never been visited before,
                    # or the new cost for reaching it is lower than the
                    # previous one
                    if neig not in cost_so_far or new_cost < cost_so_far[neig]:
                        cost_so_far[neig] = new_cost
                        # Store the neighbor for later exploration
                        priority = new_cost + cityblock(goal, neig)
                        frontier.put((priority, neig))
                        # Store the state we came from to be able to
                        # rebuild the path later
                        came_from[neig] = current

            # Unroll the path from the goal to the starting position,
            # to find out the next move
            current = goal
            prev = came_from[current]
            while prev != start:
                current = prev
                prev = came_from[current]

            # Compute the action to perform next
            return current - start

    def _observe(self, heroes):
        """
        This method checks if a hero is within the attack_radius and return its
        position relative to the current enemy. If multiple heroes are within
        range, only the  relative position of the closest hero is returned.
        :param heroes: List. A list of Hero objects representing all the
        heroes present in the environment.
        :return: Vec2D, Int. The position of the closest hero if any is within
        range, as well as the distance to the enemy unit. Otherwise it simply
        returns None and the attack_radius.
        """

        # Initialize the minimum distance and the position of the
        # corresponding hero
        min_dist = self._attack_radius + 1  # Anything outside the attack
        # radius is irrelevant
        min_pos = None

        # Process each of the heroes
        for hero in heroes:
            # Get the hero's position and compute the distance to the current
            # enemy
            hero_pos = hero.position
            dist = cityblock(hero_pos, self._position)
            if cityblock(hero_pos, self._init_position) <= self._attack_radius + 1 and dist <= min_dist:
                min_dist = dist
                min_pos = hero_pos

        # Return the relative position of the hero
        return min_pos, min_dist

    @property
    def type(self):
        return self._type

    @property
    def position(self):
        return self._position

    @position.setter
    def position(self, new_pos):
        if not self._is_static:
            assert isinstance(new_pos, tuple) or isinstance(new_pos, Vec2D), \
                "Only tuple and Vec2D objects can be used as position for " \
                "the enemy."
            assert len(new_pos) == 2, "A position is defined by a location " \
                                      "along the X and Y axis."
            if isinstance(new_pos, tuple):
                # Unpack the tuple and transform it into a 2D Vector object
                self._position = Vec2D(*new_pos)
            else:
                self._position = new_pos

    @property
    def init_position(self):
        return self._init_position

    @property
    def health(self):
        return self._health

    @health.setter
    def health(self, new_hp):
        self._health = max(0, new_hp)

    @property
    def strength(self):
        return self._strength

    @property
    def id(self):
        return self._id

    @property
    def is_static(self):
        return self._is_static

    @property
    def is_dead(self):
        return self._health <= 0
